package liv_village.liv;

/**
 * Created by Matthew Syrén on 2017/09/29.
 */

public class Schedule {
    private String date;
    private String time;
    private String description;

    //Constructor

    public Schedule(String date, String time, String description) {
        this.date = date;
        this.time = time;
        this.description = description;
    }

    //Getter methods
    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getDescription() {
        return description;
    }
}
