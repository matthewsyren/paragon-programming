package liv_village.liv;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {
    //Declarations
    CompactCalendarView compactCalendar;
    private SimpleDateFormat dateFormatMonth = new SimpleDateFormat("MMMM- yyyy", Locale.getDefault());
    private SimpleDateFormat ymdDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        compactCalendar = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
        compactCalendar.setUseThreeLetterAbbreviation(true);
        date = ymdDateFormat.format(new Date());

        toggleProgressBarVisibility(View.VISIBLE);

        //Posts the details to scheduleJson.php
        APIConnection api = new APIConnection();
        api.execute("http://a15008377.dx.am/employee/scheduleJson.php");

        //Displays month at top of calendar
        TextView txtDate = (TextView) findViewById(R.id.text_date);
        txtDate.setText(dateFormatMonth.format(new Date()));

        compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                date = ymdDateFormat.format(dateClicked);

                //Displays ProgressBar
                toggleProgressBarVisibility(View.VISIBLE);

                //Posts the details to scheduleJson.php
                APIConnection api = new APIConnection();
                api.execute("http://a15008377.dx.am/employee/scheduleJson.php");
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                TextView txtDate = (TextView) findViewById(R.id.text_date);
                txtDate.setText(dateFormatMonth.format(firstDayOfNewMonth));
            }
        });
    }

    //Displays the menu
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);
        return true;
    }

    //Signs the user out when they click on sign out
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                signOut();
                return true;
            default:
                return true;
        }
    }

    //Method signs the user out of the application
    public void signOut(){
        try{
            SharedPreferences preferences = getSharedPreferences("", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("username", null);
            editor.putString("password", null);
            editor.apply();

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
        }
        catch(Exception exc){
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //Method toggles the ProgressBar's visibility
    public void toggleProgressBarVisibility(int visible){
        try{
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
            progressBar.setVisibility(visible);
        }
        catch(Exception exc){
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //Method prevents the user from going back to the previous Activity by clicking the back button
    @Override
    public void onBackPressed() {
    }

    private class APIConnection extends AsyncTask<String, Void, String> {

        //Method posts the user's details to the androidLogin.php page, which will determine whether the entered details are correct
        protected String doInBackground(String... urls) {
            try {
                HttpURLConnection urlConnection = null;
                try {
                    BufferedWriter bufferedWriter = null;
                    StringBuilder stringBuilder = new StringBuilder();
                    BufferedReader bufferedReader = null;

                    //Sets up connection
                    URL url = new URL(urls[0]);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    OutputStream outputStream = urlConnection.getOutputStream();

                    //Fetches the user's email address and key from SharedPreferences
                    SharedPreferences preferences = getSharedPreferences("", Context.MODE_PRIVATE);
                    String username = preferences.getString("username", null);
                    String password = preferences.getString("password", null);

                    //Gets user's details
                    List<AbstractMap.SimpleEntry> lstUserDetails = new ArrayList<>();
                    lstUserDetails.add(new AbstractMap.SimpleEntry("username", username));
                    lstUserDetails.add(new AbstractMap.SimpleEntry("password", password));
                    lstUserDetails.add(new AbstractMap.SimpleEntry("selectedDate", date));

                    //Posts the user's details to androidLogin.php, which will check the details and return a message based on whether they are able to login or not
                    bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                    bufferedWriter.write(getPostString(lstUserDetails));
                    bufferedWriter.flush();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    return stringBuilder.toString();
                }
                finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            }
            catch (Exception e) {
                return null;
            }
        }

        //Method passes the JSON back to the Main thread (to the point from which this class was instantiated)
        protected void onPostExecute(String response) {
            if(response != null){
                if(response.charAt(0) == '['){
                    parseResponse(response);
                }
                else{
                    Toast.makeText(getApplicationContext(),"A problem occurred while fetching your schedule, please try again..." + response , Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection, please try again" , Toast.LENGTH_LONG).show();
            }
        }
    }

    //Method parses the JSON response and displays the user's schedule for the day
    public void parseResponse(String response){
        try{
            ArrayList<Schedule> lstSchedule = new ArrayList<>();
            JSONArray jsonArray = new JSONArray(response);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String date = jsonObject.getString("ScheduleDate");
                String time = jsonObject.getString("ScheduleTime");
                String description = jsonObject.getString("Description");
                Schedule schedule = new Schedule(date, time, description);
                lstSchedule.add(schedule);
            }

            ListView listView = (ListView) findViewById(R.id.list_view_schedule);
            ScheduleListViewAdapter scheduleListViewAdapter = new ScheduleListViewAdapter(this, lstSchedule);
            listView.setAdapter(scheduleListViewAdapter);

            if(lstSchedule.size() == 0){
                Toast.makeText(getApplicationContext(), "There is no schedule set for this date", Toast.LENGTH_LONG).show();
            }

            //Hides ProgresBar
            toggleProgressBarVisibility(View.INVISIBLE);
        }
        catch(JSONException jse){
            Toast.makeText(getApplicationContext(), jse.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //Encodes the user's details to ensure the details aren't misread by the PHP page
    //Learnt from https://stackoverflow.com/questions/9767952/how-to-add-parameters-to-httpurlconnection-using-post
    private String getPostString(List<AbstractMap.SimpleEntry> lstUserDetails) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (AbstractMap.SimpleEntry simpleEntry : lstUserDetails) {
            if (first) {
                first = false;
            }
            else {
                result.append("&");
            }

            //Adds the key-value pair to the String
            result.append(URLEncoder.encode(simpleEntry.getKey().toString(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(simpleEntry.getValue().toString(), "UTF-8"));
        }

        return result.toString();
    }
}
