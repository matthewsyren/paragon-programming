/*
 * Author: Matthew Syrén
 *
 * Date:   29 August 2017
 *
 * Description: Class displays Stock object information in the appropriate ListView
 */

package liv_village.liv;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;

public class ScheduleListViewAdapter extends ArrayAdapter {
    //Declarations
    private Context context;
    private ArrayList<Schedule> lstSchedule;

    //Constructor
    public ScheduleListViewAdapter(Context context, ArrayList<Schedule> lstSchedule) {
        super(context, R.layout.list_view_schedule_row, lstSchedule);
        this.context = context;
        this.lstSchedule = lstSchedule;
    }

    //Method populates the appropriate Views with the appropriate data (stored in the shows ArrayList)
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        //View declarations
        TextView txtDate;
        TextView txtTime;
        TextView txtDescription;
        ImageButton btnDeleteStock;

        //Inflates the list_row view for the ListView
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        convertView = inflater.inflate(R.layout.list_view_schedule_row, parent, false);

        //Component assignments
        txtDate = (TextView) convertView.findViewById(R.id.txvDate);
        txtTime = (TextView) convertView.findViewById(R.id.txvTime);
        txtDescription = (TextView) convertView.findViewById(R.id.txvDesc);

        //Displays the data in the appropriate Views
        Resources resources = context.getResources();
        txtDate.setText(resources.getString(R.string.text_date, lstSchedule.get(position).getDate()));
        txtTime.setText(resources.getString(R.string.text_time, lstSchedule.get(position).getTime()));
        txtDescription.setText(resources.getString(R.string.text_description, lstSchedule.get(position).getDescription()));

        return convertView;
    }
}

