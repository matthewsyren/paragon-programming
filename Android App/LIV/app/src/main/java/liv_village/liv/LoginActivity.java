package liv_village.liv;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    //Declarations
    String username;
    String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Hides ProgressBar
        toggleProgressBarVisibility(View.INVISIBLE);

        checkSignInStatus();
    }

    //Method prevents the user from going back to the previous Activity by clicking the back button
    @Override
    public void onBackPressed() {
    }

    //Method toggles the ProgressBar's visibility
    public void toggleProgressBarVisibility(int visible){
        try{
            ProgressBar progressBar = (ProgressBar) findViewById(R.id.progress_bar);
            progressBar.setVisibility(visible);
        }
        catch(Exception exc){
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //Method takes the user to the MainActivity if they are already signed in
    public void checkSignInStatus(){
        try{
            //Fetches the user's username from SharedPreferences
            SharedPreferences preferences = getSharedPreferences("", Context.MODE_PRIVATE);
            String username = preferences.getString("username", null);
            if(username != null){
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        }
        catch(Exception exc){
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    //Method attempts to sign the user in
    public void loginOnClick(View view){
        try{
            //Fetches user's details
            EditText txtUsername = (EditText) findViewById(R.id.text_username);
            EditText txtPassword = (EditText) findViewById(R.id.text_password);
            username = txtUsername.getText().toString();
            password = txtPassword.getText().toString();

            //Displays ProgressBar
            toggleProgressBarVisibility(View.VISIBLE);

            //Posts the details to androidLogin.php
            APIConnection api = new APIConnection();
            api.execute("http://matthewsyren.dx.am/WIL/androidLogin.php");
        }
        catch(Exception exc){
            Toast.makeText(getApplicationContext(), exc.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    class APIConnection extends AsyncTask<String, Void, String> {

        //Method posts the user's details to the androidLogin.php page, which will determine whether the entered details are correct
        protected String doInBackground(String... urls) {
            try {
                HttpURLConnection urlConnection = null;
                try {
                    BufferedWriter bufferedWriter = null;
                    StringBuilder stringBuilder = new StringBuilder();
                    BufferedReader bufferedReader = null;

                    //Sets up connection
                    URL url = new URL(urls[0]);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    OutputStream outputStream = urlConnection.getOutputStream();

                    //Gets user's details
                    List<AbstractMap.SimpleEntry> lstUserDetails = new ArrayList<>();
                    lstUserDetails.add(new AbstractMap.SimpleEntry("username", username));
                    lstUserDetails.add(new AbstractMap.SimpleEntry("password", password));

                    //Posts the user's details to androidLogin.php, which will check the details and return a message based on whether they are able to login or not
                    bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
                    bufferedWriter.write(getPostString(lstUserDetails));
                    bufferedWriter.flush();
                    bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    return stringBuilder.toString();
                }
                finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }
            }
            catch (Exception e) {
                return null;
            }
        }

        //Method passes the JSON back to the Main thread (to the point from which this class was instantiated)
        protected void onPostExecute(String response) {
            if(response != null){
                if(response.charAt(0) == '1'){
                    SharedPreferences preferences = getSharedPreferences("", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("username", username);
                    editor.putString("password", password);
                    editor.apply();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                }
                else{
                    Toast.makeText(getApplicationContext(),"Incorrect username or password" , Toast.LENGTH_LONG).show();
                }
            }
            else{
                Toast.makeText(getApplicationContext(),"No internet connection, please try again" , Toast.LENGTH_LONG).show();
            }
            toggleProgressBarVisibility(View.INVISIBLE);
        }
    }

    //Encodes the user's details to ensure the details aren't misread by the PHP page
    //Learnt from https://stackoverflow.com/questions/9767952/how-to-add-parameters-to-httpurlconnection-using-post
    private String getPostString(List<AbstractMap.SimpleEntry> lstUserDetails) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (AbstractMap.SimpleEntry simpleEntry : lstUserDetails) {
            if (first) {
                first = false;
            }
            else {
                result.append("&");
            }

            //Adds the key-value pair to the String
            result.append(URLEncoder.encode(simpleEntry.getKey().toString(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(simpleEntry.getValue().toString(), "UTF-8"));
        }

        return result.toString();
    }
}
