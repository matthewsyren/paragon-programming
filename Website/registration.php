<?php
	session_start();
	//Includes appropriate files
    include "styles.css";
    include "sharedFunctions.php";
    include "dbConn.php";
    include "user.php";
        
	//Array used to create a sticky form	
    $arrDisplay = array('username' => '', 'firstName' => '', 'lastName' => '', 'emailAddress' => '', 'skypeAddress' => '', 'password' => '');   
        
    if(isset($_POST["username"]) && isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["emailAddress"]) && isset($_POST["password"]) && isset($_POST["skypeAddress"])){
        //Assigns the values that the user entered to variables (formatInput() ensures that the input is safe to use)
        $username = formatInput($_POST["username"]);
        $firstName = formatInput($_POST["firstName"]);
        $lastName = formatInput($_POST["lastName"]);
        $emailAddress = formatInput($_POST["emailAddress"]);
        $password = formatInput($_POST["password"]);
		$skypeAddress = formatInput($_POST["skypeAddress"]);
                                
        //Sets values to ensure that form input fields don't lose value after submission
        $arrDisplay['username'] = $username;
        $arrDisplay['firstName'] = $firstName;
        $arrDisplay['lastName'] = $lastName;
        $arrDisplay['emailAddress'] = $emailAddress;
        $arrDisplay['password'] = $password;
		$arrDisplay['skypeAddress'] = $skypeAddress;
                              
        //If statements ensure that the entered information is valid. If any information is not valid, $valid is set to false, and the program prompts the user for valid input
        $valid = true;
        if (empty($username)){
            echo "<p class='error'>Please enter your desired username</p>";
            $valid = false;
        }
        if (empty($emailAddress) || checkEmail($emailAddress) == false){
            echo "<p class='error'>Please enter a valid email address</p>";
            $valid = false;
        }
        if (empty($firstName)){
            echo "<p class='error'>Please enter your first name</p>";
            $valid = false;
        }
        if (empty($lastName)){
            echo "<p class='error'>Please enter your last name</p>";
            $valid = false;
        }
        if (empty($password) || checkPassword($password) == false){
            echo "<p class='error'>Please enter a valid password (8 - 12 characters that includes at least 1 uppercase letter and 1 digit)</p>";
            $valid = false;
        }
		if(empty($skypeAddress)){
			echo "<p class='error'>Please enter your Skype address</p>";
            $valid = false;
		}
                
        //Compares the entered information with the information in the database if all entered information is valid
        if($valid == true){
            $dbConnect = new dbConnect();
            $sql = "select * from tblUsers where Username = '$username'";
            $result = $dbConnect->executeQuery($sql);
                        
            //Outputs error message if the user's desired username is taken
            if($result->num_rows > 0){
                echo "<script>alert(' " . $username . " has already been taken, please choose another username');</script>";
            }
            else{
                //Registers user if their information is valid and their email addrress hasn't been used already
                $hashedPassword = hash('sha256', $password);
                $dbConnect = new dbConnect();
                $user = new User($username, $firstName, $lastName, $emailAddress, $hashedPassword, $skypeAddress);
                $sql = "insert into tblUsers (Username, FirstName, LastName, EmailAddress, Password, ResetPasswordCode, SkypeAddress) values ('$user->username', '$user->firstName', '$user->lastName', '$user->emailAddress', '$user->password', NULL, '$user->skypeAddress');";
                $success = $dbConnect->executeQuery($sql);
                if($success == true){
					$_SESSION["Username"] = $username;
                    echo "<script>alert('Your account has been created successfully!');</script>";
                    redirectPage("index.php");
                }
                else{
                    echo "<script>alert('There was an error while creating your account, please try again');</script>";
                }
            }                    
        }
    }
?>

<html>
	<body>
		<form name = "registration"  method = "POST">
		<h1>Register</h1>
        <p><label>Username:</label> <input maxlength="30" type = "text" name = "username" value="<?php echo $arrDisplay['username']; ?>"/> </p>
		<p><label>First Name:</label> <input maxlength="55" type = "text" name = "firstName" value="<?php echo $arrDisplay['firstName']; ?>"/> </p>
		<p><label>Last Name:</label> <input maxlength="55" type = "text" name = "lastName" value="<?php echo $arrDisplay['lastName']; ?>"/> </p>
		<p><label>Email Address:</label> <input maxlength="254" type = "text" name = "emailAddress" value="<?php echo $arrDisplay['emailAddress']; ?>"/> </p>
		<p><label>Skype Address:</label> <input maxlength="256" type = "text" name = "skypeAddress" value="<?php echo $arrDisplay['skypeAddress']; ?>"/> </p>
		<p><label>Password:</label> <input type = "password" name = "password" value="<?php echo $arrDisplay['password']; ?>"/> </p>
		<button type="submit" name = "registration"><strong>Sign up</strong></button> 
		</form>
	</body>
</html>