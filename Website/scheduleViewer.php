<?php 
	session_start();

	include "dbConn.php";
   
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		//Calls the dateClicked function is the data for that function has been posted
		if(isset($_POST["dateClicked"])){
			getSchedule($_POST["dateClicked"]);
			unset($_POST["dateClicked"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
	//Select staff from tblSchedule where username = $variable
	function getSchedule($date){
		$username = $_SESSION["Username"];
		$dbConnect = new dbConnect();
		$date = date("Y-m-d", strtotime($date));
		$sql = "SELECT * FROM tblSchedule WHERE Username ='$username' AND ScheduleDate = '$date'";
    
		//Echoes the user's schedule into a table
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
            echo "<tr><th>Time</th><th>Description</th></td>";
			while($rows = $result->fetch_assoc()){
				echo "<tr>";
                echo "<td>" . $rows["ScheduleTime"] . "</td>";
                echo "<td>" . $rows["Description"] . "</td>";
                echo "</tr>";
           }
		}
		else{
			echo "<center>You have nothing scheduled for this date</center>";
		}
   }
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "loaderStyle.php";
			echo "<br>";
		}
		if(!isset($_POST["ajaxResponse"])){
   ?>
   <h1>Schedule</h1>
   <?php 
       include "calendar.php";
    ?>
<p><label>Date</label><br/><input type="date" id="cmbDate" name="cmbDate" value="<?php echo date('Y-m-d'); ?>"/> </p>

<script>
	//Calls setListeners function when page loads
	window.onload = setListeners;
	
	//Function sets listeners to perform specific actions when an event occurs
	function setListeners(){
		displayLoader();
		
        var cmbDate = document.getElementById("cmbDate");

		//Fetches the user's schedule whenever the user selects a new date
        cmbDate.addEventListener("change", function() {
			displayLoader();

			var firstDate = $(cmbDate).val();
			fetchSchedule(firstDate);
		});   

		//Displays the current date
        var firstDate = $(cmbDate).val();
        fetchSchedule(firstDate);
		hideLoader();
	}
	
	//Function fetches the user's schedule
	function fetchSchedule(firstDate){
		var tblSchedule = document.getElementById("tblSchedule");
		displaySelectedDate(firstDate);
			
        $.ajax({
			url: window.location.pathname,
			type: "post",
			data: {"dateClicked" : firstDate, "ajaxResponse": "1"},
			success: function(response){ 
				tblSchedule.innerHTML = response;
				hideLoader();
			}
		});
	}
	
	//Function displays the selected date and fetches the user's schedule whenever they select a new date
    function handleDateClick(date){
		displayLoader();
        var formattedDate = new Date(date);
        var tblSchedule = document.getElementById("tblSchedule");
        var cmbDate = document.getElementById("cmbDate");
        formattedDate = formatDate(formattedDate);
        $(cmbDate).val(formattedDate);
        displaySelectedDate(formattedDate);
        
		//Fetches the user's schedule
        $.ajax({
            url: window.location.pathname,
            type: "post",
            data: {"dateClicked" : date, "ajaxResponse": "1"},
            success: function(response){ 
                tblSchedule.innerHTML = response;
				hideLoader();
            }
        });
    }  
    
	//Function displays the selected date on the calendar
    function displaySelectedDate(firstDate){  
        var table = document.getElementById("table");

		for (var i = 2, row; row = table.rows[i]; i++) {
			for (var j = 0, col; col = row.cells[j]; j++) {
				currentMonth = table.rows[0].cells[1].innerHTML;
				var colValue = new Date(col.innerHTML + ' ' + currentMonth);
				colValue = formatDate(colValue);
				if(colValue == firstDate && col.innerHTML != ""){
					$(col).addClass('selectedDate');
					$(col).removeClass('unselectedDate');
				}
                else{
                    $(col).addClass('unselectedDate');
					$(col).removeClass('selectedDate');
                }
			} 
		}
	}  
    
    //Function formats a date into a format suitable for a datepicker
	//https://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
    function formatDate(date) {
		var month = '' + (date.getMonth() + 1),
		day = '' + date.getDate(),
		year = date.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	}

</script>
<html>
    <br>
    <?php
        if(!isset($_POST["ajaxResponse"])){ ?>
		<h2>Time slots</h2>
    <?php
		}
    ?>
    <br>
    <table id="tblSchedule" class="report"></table>
</html>

<?php
		}
	}
?>