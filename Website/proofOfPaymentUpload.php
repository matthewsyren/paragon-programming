<?php
	session_start();
	include "dbConn.php";
	
	//Ensures that a file has been successfully uploaded
    if ( 0 < $_FILES['file']['error'] ) {
        echo "An error occured while uploading your file, please try again later...";
    }
    else {
		//Assigns file details to variables
        $file_name = $_FILES["file"]["name"];
        $temp_name = $_FILES["file"]["tmp_name"];
        $file_type = $_FILES["file"]["type"];
			
		$username = $_SESSION["Username"];
		$sql = "select FirstName, LastName, EmailAddress from tblUsers where Username = '$username'";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$fullName = $row["FirstName"] . ' ' . $row["LastName"];
			$userEmailAddress = $row["EmailAddress"];
			$firstName = $row["FirstName"];
			$eol = PHP_EOL;
			$bookingID = $_POST["bookingID"];
			
			//Mails the user to confirm that their proof of payment has been received
			mail($userEmailAddress, "Your proof of payment", "Hi, $firstName. $eol $eol" . "Your proof of payment for Booking ID: $bookingID has been received. You will receive an email once a LIV employee has viewed it. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
			
			//Fetches LIV employee information
			$sql = "select * from tblLivEmployees where JobTitle = 'Active'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				while($row = $result->fetch_assoc()){
					//Sets up required values for email
					$from = "noreply@a15008377.dx.am";
					$to = $row["EmailAddress"];
					$livName = $row["FirstName"];
					$subject = "$fullName proof of payment";
					$eol = PHP_EOL;
					$message = "Hi, $livName. $eol $eol" . "$fullName has submitted their proof of payment for Booking ID: $bookingID. $eol $eol" . "Kind regards, $eol" . "LIV Portal";                               
										 
					$file = $temp_name;
					$content = chunk_split(base64_encode(file_get_contents($file)));
					$uid = md5(uniqid(time()));
					$eol = PHP_EOL;
						  
					//Adds message to header
					$header = "From: " . $from . $eol;
					$header .= "Reply-To: " . $replyto . $eol;
					$header .= "MIME-Version: 1.0".$eol;                                
					$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"" . $eol;
					$header .= "This is a multipart message in MIME format. " . $eol;
										 
					//Content
					$header .= "--" . $uid . $eol;
					$header .= "Content-type:text/plain; charset=iso-8859-1".$eol;
					$header .= "Content-Transfer-Encoding: 7bit".$eol;
					$header .= $message . $eol;
								
					//Attachment code learnt and adapted from https://www.youtube.com/watch?v=zYocypr0Xig								
					//Attachment
					$header .= "--" . $uid . $eol;
					$header .= "Content-type: " .$file_type . "; name=\"" . $file_name . "\"". $eol;
					$header .= "Content-Transfer-Encoding: base64" . $eol;
					$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"" .$eol;
					$header .= $content . $eol;
							   
					//Sends email to LIV employee
					mail($to, $subject, "", $header);                      
				}
				echo "Your proof of payment has been submitted, please wait for a LIV employee to contact you";
			}
		}                   
    }
?>