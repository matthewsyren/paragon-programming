﻿<?php
        session_start();
?>
<?php
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		include "styles.css";
		include "navbar.php";
		include "volunteer.php";
        
?>
<!DOCTYPE html>
<html>
<head>
<Style>
	#div1{
		background-color: #f99f18;
		cursor:pointer;
		height:200px;
		width: 33%;
		display: inline-block
	}
	
	table{
		border: 0px solid black;
	}
	th{
		text-align: left;
	}
	#txtAddress
	{
	text-align:left
	}
	#div2{
		background-color: #0fb5fc;
		cursor:pointer;
		height:200px;
		width: 33%;
		display: inline-block
	}
	
	#div3{
		background-color: #8cdd3b;
		cursor:pointer;
		height:200px;
		width: 33%;
		display: inline-block
	}
	#lblGender
	{
		display:block
	}	
	h1{
		text-align: center;
	}
	img{
		float: left;
	}
	        
    form{
		margin:auto;
		display:block;
		width:100%;
		text-align: center;
	}
	h2
	{
		color: #FFFFFF;
		font-size: large;
		background-color: #000000;
	}
	h1{
		text-align:center;
	}
	
	p{
		text-align:center;
	}
		
	button{
		background: #f99f18;
		border: 1px solid #f99f18;
		color:#ffffff;
		margin:auto;
		display:block;
		padding: 5px;
		font-size:25px;
		cursor:pointer
	}
		
	h3{
		text-align: center;		
	}
	
	p{
		display: block;
	}
	#frm1{
		table-layout:inherit
	}
	#logo
	{
	}

	.text{
		width: 250px;
	}

	textarea{
		width: 150px;
	}
</Style>
</head>
	<body>
	<?php
        include("dbConn.php");
        include "reference.php";
        include "workExperience.php";
        include "education.php";
        include "sharedFunctions.php";
		
		//Declare Variables
		$subject = "Volunteer";
		$eol = PHP_EOL;
  		$msg = stripslashes('Title- '. $_POST['Select1'] . "$eol". 'Surname- '. $_POST['txtSurname'] . "$eol". 'First Names- '. $_POST['fNnames'] . "$eol". 'Address- '.$_POST['txtAddress'] . "$eol". 'Email- '.$_POST['email'] . "$eol". 'Home Telephone Number- '. $_POST['telHome'] . "$eol". 'Business Telephone Number- '.$_POST['telBusiness'] . "$eol". 'Cell Phone Number- '.$_POST['Phnone'] . "$eol". 'Date of Birth- '.$_POST['dateofbirth'] . "$eol". 'Age- '.$_POST['age'] . "$eol". 'Gender- '.$_POST['gender'] . "$eol". 'Nationality- '.$_POST['nationality'] . "$eol". 'Passport Number- '.$_POST['passport'] . "$eol". 'Date Issued- '.$_POST['expirtiondate'] . "$eol". 'Valid Until- '.$_POST['expirationdate'] . "$eol". 'Marital Status- '.$_POST['marital'] . "$eol". 'Residential Permit- '.$_POST['nonRSAPerm'] . "$eol". 'Religion- '.$_POST['religion'] . "$eol". 'Church- '.$_POST['church'] . "$eol". 'Current Occupation- '. $_POST['occupation'] . "$eol". 'Area To Volunteer- '. $_POST['vol'] . "$eol". 'Emergency Contact Name- '.$_POST['name'] . "$eol". 'Emergency Contact Telephone- '.$_POST['tel'] . "$eol". 'Emergency Contact Address- '.$_POST['addresss2'] . "$eol". 'Emergency Contact Relationship- '.$_POST['relationship'] . "$eol". 'Medical Problems- '.$_POST['illness'] . "$eol". 'School 1- '.$_POST['institution1'] . "$eol". 'Start Date 1- '.$_POST['startDates1'] . "$eol". 'End Date 1- '.$_POST['endDates1'] . "$eol". 'School 2- '.$_POST['institution2'] . "$eol". 'Start Date 2- '.$_POST['startDates2'] . "$eol". 'End Date 2- '.$_POST['endDates2'] . "$eol". 'School 3- '.$_POST['institution3'] . "$eol". 'Start Date 3- '.$_POST['startDates3'] . "$eol". 'End Date 3- '.$_POST['endDates3'] . "$eol". 'Languages Spoken- '.$_POST['lang'] . "$eol". 'Intercultural Expericence- '.$_POST['cult'] . "$eol". 'Postion 1- '.$_POST['pos1'] . "$eol". 'Organisation 1- '.$_POST['org1'] . "$eol". 'Start Date 1- '.$_POST['dateStart1'] . "$eol". 'End Date 1- '.$_POST['dateEnd1'] . "$eol". 'Leave 1- '. $_POST['leave1'] . "$eol". 'Postion 2- '.$_POST['pos2'] . "$eol". 'Organisation 2- '.$_POST['org2'] . "$eol". 'Start Date 2- '.$_POST['dateStart2'] . "$eol". 'End Date 2- '.$_POST['dateEnd2'] . "$eol".  "Leave 2- " . $_POST['leave2'] . "$eol".  "Position 3- " . $_POST['pos3'] . "$eol". 'Organisation 3- '.$_POST['org3'] . "$eol". 'Start Date 3- '.$_POST['dateStart3'] . "$eol". 'End Date 3- '.$_POST['dateEnd2'] . "$eol". 'Leave 3- '.$_POST['leave3'] . "$eol". 'Position 4- ' . $_POST['pos4'] . "$eol". 'Organisation 4- '.$_POST['org4'] . "$eol". 'Start Date 4- '.$_POST['dateStart4'] . "$eol". 'End Date 4- '.$_POST['dateEnd4'] . "$eol". 'Leave 4- '.$_POST['leave4'] . "$eol". 'Position 5- '.$_POST['pos5'] . "$eol". 'Organisation 5- '.$_POST['org5'] . "$eol". 'Start Date 5- '.$_POST['dateStart5'] . "$eol". 'End Date 5- '.$_POST['dateEnd5'] . "$eol". 'Leave 5- '.$_POST['leave5'] . "$eol". 'Other Skills Or Work Experience- '.$_POST['skill'] . "$eol". 'Hobbies And Interests- '.$_POST['hobby'] . "$eol". 'Worked With NGO Before- '.$_POST['priorHist'] . "$eol". 'How LIV Was Discovered- '.$_POST['liv'] . "$eol". 'Previus Expericence With LIV- '.$_POST['txtlivInvolvement'] . "$eol". 'Reasons For Volunteering- '.$_POST['txtwhyLIV'] . "$eol". 'Expectations As A Volunteer- ' . $_POST['txtexpectaions'] . "$eol". 'Substance Abuse History- ' . $_POST['txtdrugHist'] . "$eol". 'Smoker- ' . $_POST['txtSmoke'] . "$eol". 'Inebriation Frequency- ' . $_POST['txtInebriated'] . "$eol". 'Past Psychological Treatment- ' . $_POST['txtPsychTreatment'] . "$eol". 'Life Circumstances And Challenges- '.$_POST['txtChilhoodNote'] . "$eol". 'Family Opinion Of LIV- '.$_POST['txtFamilyViews'] . "$eol". 'Relationships With Others- '.$_POST['txtLeadershipRelations'] . "$eol". 'Criminal Convictions- '.$_POST['con'] . "$eol". 'Reference Name 1- '.$_POST['txtRefName1'] . "$eol". 'Reference Organisation 1- '. $_POST['txtRefOrg1'] . "$eol".'Reference Position 1- '.$_POST['txtRefPosition1'] . "$eol". 'Reference Email 1- '.$_POST['txtRefMail1'] . "$eol". 'Reference Telephone Number 1- '.$_POST['txtRefTel1'] . "$eol". 'Reference Name 2- '.$_POST['txtRefName2'] . "$eol". 'Reference Organisation 2- '.$_POST['txtRefOrg2'] . "$eol". 'Reference Position 2- '. $_POST['txtRefPosition2'] . "$eol". 'Reference Email 2- '.$_POST['txtRefMail2'] . "$eol". 'Reference Telephone Number 2- '.$_POST['txtRefTel2'] . "$eol". 'Reference Name 3- '.$_POST['txtRefName3'] . "$eol". 'Reference Organisation 3- ' . $_POST['txtRefOrg3'] . "$eol". 'Reference Position 3- '.$_POST['txtRefPosition3'] . "$eol". 'Reference Email 3- '.$_POST['txtRefMail3'] . "$eol". 'Reference Telephone Number 3- '.$_POST['txtRefTel3'] . "$eol". 'Reference Name 4- '.$_POST['txtRefName4'] . "$eol". 'Reference Organisation 4- '.$_POST['txtRefOrg4'] . "$eol".'Reference Position 4- '.$_POST['txtRefPosition4'] . "$eol". 'Reference Email 4- '. $_POST['txtRefMail4'] . "$eol". 'Reference Telephone Number 4- '. $_POST['txtRefTel4'] . "$eol". 'Reference Name 5- '. $_POST['txtRefName5'] . "$eol". 'Reference Organisation 5- '. $_POST['txtRefOrg5'] . "$eol". 'Reference Position 5- '. $_POST['txtRefPosition5'] . "$eol". 'Reference Email 5- '.$_POST['txtRefMail5'] . "$eol". 'Reference Telephone Number 5- '.$_POST['txtRefTel5'] . "$eol". 'Additional Information- '.$_POST['txtMoreInfo'] . "$eol");

        $username = $_SESSION["Username"];
		$title = "";
		$surname = "";
		$firstName = "";
		$address = "";
		$email = "";
		$homeTelephoneNumber = "";
		$businessTelephoneNumber = "";
		$cellNumber = "";
		$dateOfBirth = "";
		$age = "";
		$gender = "";
		$nationality = "";
		$passportNumber = "";
		$dateIssued = "";
		$validUntil = "";
		$maritalStatus = "";
		$residentialPermit = "";
		$religion = "";
		$church = "";
		$currentOccupation = "";
		$areaToVolunteer = "";
		$emergencyContactName = "";
		$emergencyContactTelephone = "";
		$emergencyContactAddress = "";
		$relationship = "";
		$medicalProblems = "";
		$school = "";
		$dateStart = "";
		$dateEnd = "";
		$languagesSpoken = "";
		$interculturalExperience = "";
		$position = "";
		$organisation = "";
		$dateStarted = "";
        $dateEnded = "";
		$reasonsForLeaving = "";
		$otherSkillsOrWorkExperience = "";
		$hobbiesAndInterests = "";
		$workedWithNGOBefore = "";
		$howLIVWasDiscovered = "";
		$previousExperienceWithLIV = "";
		$reasonsForVolunteering = "";
		$expectationsAsAVolunteer = "";
		$substanceAbuseHistory = "";
		$smoker = "";
		$inebriationFrequency = "";
		$pastPsychologicalTreatment = "";
		$lifeCircumstancesAndChallenges = "";
		$familyOpinionOfLIV = "";
		$relationshipWithOthers = "";
		$criminalConvictions = "";
		$referenceName = "";
		$referenceOrganisation = "";
		$referencePosition = "";
		$referenceEmail = "";
		$referenceTelephone = "";
		$additionalInformation = "";
		
		//Saves posted values to variables
        if(isset($_POST['submit'])){
			//Saves values
			$title = formatInput($_POST['Select1']);
			$surname = formatInput($_POST['txtSurname']);
			$firstName = formatInput($_POST['fNnames']);
			$address = formatInput($_POST['txtAddress']);
			$email = formatInput($_POST['email']);
			$homeTelephoneNumber = formatInput($_POST['telHome']);
			$businessTelephoneNumber = formatInput($_POST['telBusiness']);
			$cellNumber = formatInput($_POST['Phnone']);
			$dateOfBirth = formatInput($_POST['dateofbirth']);
			$age = formatInput($_POST['age']);
			$gender = formatInput($_POST['gender']);
			$nationality = formatInput($_POST['nationality']);
			$passportNumber = formatInput($_POST['passport']);
			$dateIssued = formatInput($_POST['expirtiondate']);
			$validUntil = formatInput($_POST['expirationdate']);
			$maritalStatus = formatInput($_POST['marital']);
			$residentialPermit = formatInput($_POST['nonRSAPerm']);
			$religion = formatInput($_POST['religion']);
			$church = formatInput($_POST['church']);
			$currentOccupation = formatInput($_POST['occupation']);
			$areaToVolunteer = formatInput($_POST['vol']);
			$emergencyContactName = formatInput($_POST['name']);
			$emergencyContactTelephone = formatInput($_POST['tel']);
			$emergencyContactAddress = formatInput($_POST['addresss2']);
			$relationship = formatInput($_POST['relationship']);
			$medicalProblems = formatInput($_POST['illness']);	
			$school1 = formatInput($_POST['institution1']);
			$dateStart1 = formatInput($_POST['startDates1']);
			$dateEnd1 = formatInput($_POST['endDates1']);
            $school2 = formatInput($_POST['institution2']);
			$dateStart2 = formatInput($_POST['startDates2']);
			$dateEnd2 = formatInput($_POST['endDates2']);
            $school3 = formatInput($_POST['institution3']);
			$dateStart3 = formatInput($_POST['startDates3']);
			$dateEnd3 = formatInput($_POST['endDates3']);
               
            $education1 = new education($school1, $dateStart1, $dateEnd1);
            $arrEducation = array($education1);
            
			//Adds education objects to an array if they have all required fielsa
            if(!empty($school2) && !empty($dateStart2) && !empty($dateEnd2)){
                $education2 = new education($school2, $dateStart2, $dateEnd2);
                array_push($arrEducation, $education2);
            }
            if(!empty($school3) && !empty($dateStart3) && !empty($dateEnd3)){
                $education3 = new education($school3, $dateStart3, $dateEnd3);
                array_push($arrEducation, $education3);
            }
            if(!empty($school4) && !empty($dateStart4) && !empty($dateEnd4)){
				$education4 = new education($school4, $dateStart4, $dateEnd4);
                array_push($arrEducation, $education4);
            }
            if(!empty($school5) && !empty($dateStart5) && !empty($dateEnd5)){
				$education5 = new education($school5, $dateStart5, $dateEnd5);
                array_push($arrEducation, $education5);
            }
                 
			$languagesSpoken = formatInput($_POST['lang']);
			$interculturalExperience = formatInput($_POST['cult']);
            $position1 = formatInput($_POST['pos1']);
			$organisation1 = formatInput($_POST['org1']);
			$dateStarted1 = formatInput($_POST['dateStart1']);
			$dateEnded1 = formatInput($_POST['dateEnd1']);
			$reasonsForLeaving1 = formatInput($_POST['leave1']);
            $position2 = formatInput($_POST['pos2']);
			$organisation2 = formatInput($_POST['org2']);
			$dateStarted2 = formatInput($_POST['dateStart2']);
			$dateEnded2 = formatInput($_POST['dateEnd2']);
			$reasonsForLeaving2 = formatInput($_POST['leave2']);
            $position3 = formatInput($_POST['pos3']);
			$organisation3 = formatInput($_POST['org3']);
			$dateStarted3 = formatInput($_POST['dateStart3']);
			$dateEnded3 = formatInput($_POST['dateEnd3']);
			$reasonsForLeaving3 = formatInput($_POST['leave3']);
            $position4 = formatInput($_POST['pos4']);
			$organisation4 = formatInput($_POST['org4']);
			$dateStarted4 = formatInput($_POST['dateStart4']);
			$dateEnded4 = formatInput($_POST['dateEnd4']);
			$reasonsForLeaving4 = formatInput($_POST['leave4']);
            $position5 = formatInput($_POST['pos5']);
			$organisation5 = formatInput($_POST['org5']);
			$dateStarted5 = formatInput($_POST['dateStart5']);
			$dateEnded5 = formatInput($_POST['dateEnd5']);
			$reasonsForLeaving5 = formatInput($_POST['leave5']);
                
            $workExperience1 = new workExperience($position1, $organisation1, $dateStarted1, $dateEnded1, $reasonsForLeaving1);
            $arrWorkExperience = array($workExperience1);
            
            if(!empty($position2) && !empty($organisation2) && !empty($dateStarted2) && !empty($dateEnded2) && !empty($reasonsForLeaving2)){
                $workExperience2 = new workExperience($position2, $organisation2, $dateStarted2, $dateEnded2, $reasonsForLeaving2);
                array_push($arrWorkExperience, $workExperience2);
            }
            if(!empty($position3) && !empty($organisation3) && !empty($dateStarted3) && !empty($dateEnded3) && !empty($reasonsForLeaving3)){
                $workExperience3 = new workExperience($position3, $organisation3, $dateStarted3, $dateEnded3, $reasonsForLeaving3);
                array_push($arrWorkExperience, $workExperience3);
            }
            if(!empty($position4) && !empty($organisation4) && !empty($dateStarted4) && !empty($dateEnded4) && !empty($reasonsForLeaving4)){
                $workExperience4 = new workExperience($position4, $organisation4, $dateStarted4, $dateEnded4, $reasonsForLeaving4);
                array_push($arrWorkExperience, $workExperience4);
            }
            if(!empty($position5) && !empty($organisation5) && !empty($dateStarted5) && !empty($dateEnded5) && !empty($reasonsForLeaving5)){
				$workExperience5 = new workExperience($position5, $organisation5, $dateStarted5, $dateEnded5, $reasonsForLeaving5);
                array_push($arrWorkExperience, $workExperience5);
            }
		
			$otherSkillsOrWorkExperience = formatInput($_POST['skill']);
			$hobbiesAndInterests = formatInput($_POST['hobby']);
			$workedWithNGOBefore = formatInput($_POST['priorHist']);
			$howLIVWasDiscovered = formatInput($_POST['liv']);
			$previousExperienceWithLIV = formatInput($_POST['txtlivInvolvement']);
			$reasonsForVolunteering = formatInput($_POST['txtwhyLIV']);
			$expectationsAsAVolunteer = formatInput($_POST['txtexpectaions']);
			$substanceAbuseHistory = formatInput($_POST['txtdrugHist']);
			$smoker = formatInput($_POST['txtSmoke']);
			$inebriationFrequency = formatInput($_POST['txtInebriated']);
			$pastPsychologicalTreatment = formatInput($_POST['txtPsychTreatment']);
			$lifeCircumstancesAndChallenges = formatInput($_POST['txtChilhoodNote']);
			$familyOpinionOfLIV = formatInput($_POST['txtFamilyViews']);
			$relationshipWithOthers = formatInput($_POST['txtLeadershipRelations']);
			$criminalConvictions = formatInput($_POST['con']);
                
			$referenceName1 = formatInput($_POST['txtRefName1']);
			$referenceOrganisation1 = formatInput($_POST['txtRefOrg1']);
			$referencePosition1 = formatInput($_POST['txtRefPosition1']);
			$referenceEmail1 = formatInput($_POST['txtRefMail1']);
			$referenceTelephone1 = formatInput($_POST['txtRefTel1']);
            $referenceName2 = formatInput($_POST['txtRefName2']);
			$referenceOrganisation2 = formatInput($_POST['txtRefOrg2']);
			$referencePosition2 = formatInput($_POST['txtRefPosition2']);
			$referenceEmail2 = formatInput($_POST['txtRefMail2']);
			$referenceTelephone2 = formatInput($_POST['txtRefTel2']);
            $referenceName3 = formatInput($_POST['txtRefName3']);
			$referenceOrganisation3 = formatInput($_POST['txtRefOrg3']);
			$referencePosition3 = formatInput($_POST['txtRefPosition3']);
			$referenceEmail3 = formatInput($_POST['txtRefMail3']);
			$referenceTelephone3 = formatInput($_POST['txtRefTel3']);
            $referenceName4 = formatInput($_POST['txtRefName4']);
			$referenceOrganisation4 = formatInput($_POST['txtRefOrg4']);
			$referencePosition4 = formatInput($_POST['txtRefPosition4']);
			$referenceEmail4 = formatInput($_POST['txtRefMail4']);
			$referenceTelephone4 = formatInput($_POST['txtRefTel4']);
            $referenceName5 = formatInput($_POST['txtRefName5']);
			$referenceOrganisation5 = formatInput($_POST['txtRefOrg5']);
			$referencePosition5= formatInput($_POST['txtRefPosition5']);
			$referenceEmail5 = formatInput($_POST['txtRefMail5']);
			$referenceTelephone5 = formatInput($_POST['txtRefTel5']);
                
			$additionalInformation = formatInput($_POST['txtMoreInfo']);
			$countErrors=0;
                
            $reference1 = new reference($referenceName1, $referenceOrganisation1, $referencePosition1, $referenceTelephone1, $referenceEmail1);
            $arrReferences = array($reference1);
            
			//Stores references in an array
            if(!empty($referenceName2) && !empty($referenceOrganisation2) && !empty($referencePosition2) && !empty($referenceEmail2) && !empty($referenceTelephone2)){
                $reference2 = new reference($referenceName2, $referenceOrganisation2, $referencePosition2, $referenceTelephone2, $referenceEmail2);
                array_push($arrReferences, $reference2);
            }
            if(!empty($referenceName3) && !empty($referenceOrganisation3) && !empty($referencePosition3) && !empty($referenceEmail3) && !empty($referenceTelephone3)){
                $reference3 = new reference($referenceName3, $referenceOrganisation3, $referencePosition3, $referenceTelephone3, $referenceEmail3);
                array_push($arrReferences, $reference3);
            }
            if(!empty($referenceName4) && !empty($referenceOrganisation4) && !empty($referencePosition4) && !empty($referenceEmail4) && !empty($referenceTelephone4)){
                $reference4 = new reference($referenceName4, $referenceOrganisation4, $referencePosition4, $referenceTelephone4, $referenceEmail4);
                array_push($arrReferences, $reference4);
            }
            if(!empty($referenceName5) && !empty($referenceOrganisation5) && !empty($referencePosition5) && !empty($referenceEmail5) && !empty($referenceTelephone5)){
                $reference5 = new reference($referenceName5, $referenceOrganisation5, $referencePosition5, $referenceTelephone5, $referenceEmail5);
                array_push($arrReferences, $reference5);
            }
	
			//Deletes old data and inserts new data to the database
            $sql = "delete from tblVolunteers where Username = '$username';";
            $dbConnect = new dbConnect();
            $dbConnect->executeQuery($sql);
			$sql = "delete from tblApplications where Username = '$username';";
            $dbConnect = new dbConnect();
            $dbConnect->executeQuery($sql);
							
			$sql = "delete from tblVolunteerReferences where ApplicantUsername = '$username';";
            $dbConnect = new dbConnect();
            $dbConnect->executeQuery($sql);
							
			$sql = "delete from tblApplicantWorkExperience where Username = '$username';";
            $dbConnect = new dbConnect();
            $dbConnect->executeQuery($sql);
								
			$sql = "delete from tblApplicantEducation where Username = '$username';";
            $dbConnect = new dbConnect();
            $dbConnect->executeQuery($sql);
                                   
            $date = date('Y/m/d');
            $sql = "insert into tblApplications values ('$username', '$date', 0, 0, 0, 'Volunteer');";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
			
            if($result == 1){
				$volunteer = new Volunteer($username, $title,$address, $homeTelephoneNumber, $businessTelephoneNumber, $cellNumber, $dateOfBirth, $age, $gender, $nationality, $passportNumber, $dateIssued, $validUntil, $maritalStatus, $residentialPermit, $religion, $church, $currentOccupation, $areaToVolunteer, $emergencyContactName, $emergencyContactTelephone, $emergencyContactAddress, $relationship, $medicalProblems, $languagesSpoken, $interculturalExperience, $otherSkillsOrWorkExperience, $hobbiesAndInterests, $workedWithNGOBefore, $howLIVWasDiscovered, $previousExperienceWithLIV, $reasonsForVolunteering, $expectationsAsAVolunteer, $substanceAbuseHistory, $smoker, $inebriationFrequency, $pastPsychologicalTreatment, $lifeCircumstancesAndChallenges, $familyOpinionOfLIV, $relationshipWithOthers, $criminalConvictions, $additionalInformation);

				$sqlRes = "INSERT INTO tblVolunteers (Username, Title,Address,HomeTelephoneNumber,BusinessTelephoneNumber,CellNumber,DateOfBirth,Age,
				Gender,Nationality,PassportNumber,DateIssued,ValidUntil,MaritalStatus,ResidentialPermit,Religion,Church,CurrentOccupation,AreaToVolunteer,EmergencyContactName,
				EmergencyContactTelephone,EmergencyContactAddress,EmergencyContactRelationship,MedicalProblems,LanguagesSpoken,InterculturalExperiences,
				OtherSkillsOrWorkExperience,HobbiesAndInterests,WorkedWithNGOBefore,HowLivWasDiscovered,PreviousExperienceWithLIV,ReasonsForVolunteering,
				ExpectationsAsAVolunteer,SubstanceAbuseHistory,Smoker,InebriationFrequency,PastPsychologicalTreatment,LifeCircumstancesAndChallenges,FamilyOpinionOfLIV,
				RelationshipsWithOthers,CriminalConvictions,AdditionalInformation) VALUES('$volunteer->username', '$volunteer->title','$volunteer->address', 
				'$volunteer->homeTelephoneNumber', '$volunteer->businessTelephoneNumber', '$volunteer->cellNumber', '$volunteer->dateOfBirth', '$volunteer->age', '$volunteer->gender', '$volunteer->nationality', '$volunteer->passportNumber', 
				'$volunteer->dateIssued', '$volunteer->validUntil', '$volunteer->maritalStatus', '$volunteer->residentialPermit', '$volunteer->religion', '$volunteer->church', '$volunteer->currentOccupation', '$volunteer->areaToVolunteer', '$volunteer->emergencyContactName', 
				'$volunteer->emergencyContactTelephone', '$volunteer->emergencyContactAddress', '$volunteer->relationship', '$volunteer->medicalProblems', '$volunteer->languagesSpoken', '$volunteer->interculturalExperience', 
				'$volunteer->otherSkillsOrWorkExperience', '$volunteer->hobbiesAndInterests', '$volunteer->workedWithNGOBefore', '$volunteer->howLIVWasDiscovered', '$volunteer->previousExperienceWithLIV', '$volunteer->reasonsForVolunteering', 
				'$volunteer->expectationsAsAVolunteer', '$volunteer->substanceAbuseHistory', '$volunteer->smoker', '$volunteer->inebriationFrequency', '$volunteer->pastPsychologicalTreatment', '$volunteer->lifeCircumstancesAndChallenges', 
				'$volunteer->familyOpinionOfLIV', '$volunteer->relationshipWithOthers', '$volunteer->criminalConvictions', '$volunteer->additionalInformation');";
				$dbConnect = new dbConnect();        
				$result = $dbConnect->executeQuery($sqlRes);
			
				//Inserts all references to the database
                foreach($arrReferences as $reference){
                    $sqlRes = "INSERT INTO tblVolunteerReferences (FullName, Organisation, PositionHeld, EmailAddress, TelephoneNumber, ApplicantUsername) VALUES('$reference->name', '$reference->organisation', 
								'$reference->position', '$reference->email', '$reference->telephone', '$username');";
					$dbConnect = new dbConnect();        
					$result = $dbConnect->executeQuery($sqlRes); 
                }
				
				if ($result === FALSE)
					echo "<script>alert('An error occurred, please try again');</script>";
				else
					echo "<script>alert('Your application has been submitted, please wait for a LIV employee to contact you. ');</script>";
                     
				//Inserts all work experience to the database
                foreach($arrWorkExperience as $workExperience){
					$sqlRes = "INSERT INTO tblApplicantWorkExperience (Username, Organisation, PositionHeld, DateStarted, DateEnded, ReasonForLeaving) VALUES('$username', '$workExperience->organisation', '$workExperience->position', 
								'$workExperience->startDate', '$workExperience->endDate', '$workExperience->reasonForLeaving');";
					$dbConnect = new dbConnect();        
					$result = $dbConnect->executeQuery($sqlRes);
                }

				//Inserts all education information to the database
				foreach($arrEducation as $education){
                    $sqlRes = "INSERT INTO tblApplicantEducation (Username, NameOfSchool, DateStarted, DateEnded) VALUES('$username', '$education->institute', '$education->startDate', '$education->endDate');";
					$dbConnect = new dbConnect();        
					$result = $dbConnect->executeQuery($sqlRes);
				}

				$from = "noreply@a15008377.dx.am";
                $eol = PHP_EOL;

                $file_name = $_FILES["idOrPassport"]["name"];
                $temp_name = $_FILES["idOrPassport"]["tmp_name"];
                $file_type = $_FILES["idOrPassport"]["type"];
                        
                $file_name1 = $_FILES["policeCheck"]["name"];
                $temp_name1 = $_FILES["policeCheck"]["tmp_name"];
                $file_type1 = $_FILES["policeCheck"]["type"];

				$sql = "select * from tblLivEmployees where JobTitle = 'Active'";
				$dbConnect = new dbConnect();
				$result = $dbConnect->executeQuery($sql);
				if($result->num_rows > 0){
					$livEmployees = "";
					//Sends an email to all LIV employees with the application information
					while($row = $result->fetch_assoc()){
						//Attachment code learnt and adapted from https://www.youtube.com/watch?v=zYocypr0Xig
						$from = "noreply@a15008377.dx.am";
                        $to = $row["EmailAddress"];
						$msg = stripslashes($msg);
						$eol = PHP_EOL;
						$name = $row["FirstName"];
						$fullName = $firstName . ' ' . $surname;
                        $message = stripslashes("Hi, $name. $eol $eol" . "$fullName  has applied to be a Volunteer. Here are their details: $eol" . "$msg " . " $eol $eol" . "Kind regards, LIV Portal");                         
                        $livEmployees .= $row["FirstName"] . '- ' . $row["EmailAddress"] . "$eol";
                        $file = $temp_name;
                        $content = chunk_split(base64_encode(file_get_contents($file)));
                        $uid = md5(uniqid(time()));
                                 
                        //Adds information to header     
                        $header = "From: " . $from . $eol;
                        $header .= "Reply-To: " . $replyto . $eol;
                        $header .= "MIME-Version: 1.0".$eol;   
                        $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"" . $eol;
                        $header .= "This is a multipart message in MIME format. " . $eol;
                                 
                        //Content
                        $header .= "--" . $uid . $eol;
                        $header .= "Content-type:text/plain; charset=iso-8859-1".$eol;
                        $header .= "Content-Transfer-Encoding: 7bit".$eol;
                        $header .= $message . $eol;
                                 
                        //Attachment
                        $header .= "--" . $uid . $eol;
                        $header .= "Content-type: " .$file_type . "; name=\"" . $file_name . "\"". $eol;
                        $header .= "Content-Transfer-Encoding: base64" . $eol;
						$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"" .$eol;
                        $header .= $content;
                                 
                        $file = $temp_name1;
                        $content = chunk_split(base64_encode(file_get_contents($file)));
                                 
                        //Attachment
                        $header .= "--" . $uid . $eol;
                        $header .= "Content-type: " .$file_type1 . "; name=\"" . $file_name1 . "\"". $eol;
                        $header .= "Content-Transfer-Encoding: base64" . $eol;
                        $header .= "Content-Disposition: attachment; filename=\"".$file_name1."\"" .$eol;
                        $header .= $content . $eol;

						//Sends email to LIV employees
                        mail($to, $fullName .  " volunteer application", "", $header);   	
                    }
					//Sends email to references
					mailReferences($arrReferences, $fullName, $livEmployees);
						
					//Sends email to applicant
					$eol = PHP_EOL;
					mail($email, "Your volunteer application", "Hi, $firstName" . ". $eol $eol" . "Your application to become a volunteer at LIV has been received, please wait for a LIV employee to contact you. $eol $eol". "Kind regards, $eol" . "LIV Portal");
				}                 
            }
            else{
                echo "not allowed";
            }
        }
		else{
			echo "variable not set";
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
		
	//Function emails the references asking for a reference letter	
	function mailReferences($arrReferences, $fullName, $livEmployees){
		foreach($arrReferences as $reference){
			$emailAddress = $reference->email;
			$referenceName = $reference->name;
			$eol = PHP_EOL;
			$message = "Hi, " . $referenceName . ". $eol $eol" . "We are contacting you to ask you to write a letter of recommendation for $fullName for their time spent with you. They are currently seeking a volunteer position with LIV Village, an orphanage in Durban, South Africa where we work with vulnerable children. $eol $eol" . "Before we can accept them as a volunteer we need to do in-depth background checks on them, whether they would be suitable to work with vulnerable children for however long the volunteer period would be. We kindly ask you to please provide us with a letter of reference from someone who knows $fullName on a professional level and who can speak about their abilities to perform in the role for which they are being considered. Both their character and professional capacity would need to fit into our setup for them to be considered for the position. $eol $eol" . "LIV exists to raise the next generation of leaders in our nation. It is our mission, our purpose and our passion. We place vulnerable, parentless children into a family environment where they receive unconditional love, spiritual discipleship, care and nurturing and where all their physical needs are met. $eol $eol" . "Children are tomorrow’s future, so the manner in which they are raised will influence who they become. HIV/AIDS and poverty have resulted in many children being abandoned and orphaned. We want these children to grow up to be a generation that will influence positive change within South Africa, our continent and indeed the world. If these children are equipped with essential moral values and life skills, they truly can live lives that will influence and inspire others. $eol $eol" . "If you are able to, please provide a letter of recommendation for the person in question to the following people- $eol" . $livEmployees . "$eol $eol" . "We will be happy to answer any questions or provide additional information. $eol $eol" . "Sincerely, $eol" . "LIV Portal";
			mail($emailAddress, $fullName . " volunteer reference letter", $message);
		}
	}
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
?>
	<br>	
<form id="frm1" draggable="true" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method = "POST" enctype="multipart/form-data">	
 &nbsp;&nbsp;&nbsp; <br>
<h1>ON SITE VOLUNTEER APPLICATION</h1>
	<br>
<h2>
Personal Details</h2>
</div>

		<label id="Label1" for="Title"> Title
		<select name="Select1">
				<option value="Ms">Ms</option>
				<option selected="" value="Mr">Mr</option>
			</select>
			<br>
			<br>
		</label>
		<label for="surname">Surname</label><br/>
		<input type="text" class="text" name ="txtSurname" id="txtSurname" maxlength="30" value="" required="true">
		<br>
		<br>
		<label for="firstnames">First Names</label><br/>
		<input type="text" class="text" name ="fNnames" id="fNnames" maxlength="30" value="" required="true">
		<br>
		<br>
		<label for="address">Address</label><br/>
		<br>
		<textarea name ="txtAddress" id="txtAddress" maxlength="150" style="height: 106px; width: 265px" required="true"></textarea>
		<br>
		<br>
		<label for="email">Email</label><br/>
		<input type="email" class="text" name ="email" id="email" maxlength="30" value="" required="true">
		<br>
		<br>
		<label for="telHome">Tel(Home)</label><br/>
		<input type="text" class="text" name ="telHome" id="telHome" maxlength="20" value="" required="true">
		<br>
		<br>
		<label for="telBusiness">Tel(Business)</label><br/>
		<input type="text" class="text" name ="telBusiness" id="telBusiness" maxlength="20" value="" required="true">
		<br>
		<br>
		<label for="Phone">Phone</label><br/>
		<input type="text" class="text" name ="Phnone" id="Phnone" maxlength="20" value="" required="true">
		<br>
		<br>
		<label for="dateofbirth">Date of Birth</label><br/>
		<input type="date" class="text" name ="dateofbirth" id="dateofbirth" value="" required="true">
		<br>
		<br>

		<label for="age">Age</label><br/>
		<input type="number" class="text" name ="age" id="age" value="" required="true">
		<br>
		<br>
		<label for="gender">Gender</label><br/>
		<br>
		<label for="male">Male</label>
			<input type="radio" name="gender" id="male" value="Male" required="true">
		<label for="female">Female</label>
		<input type="radio" name="gender" id="female" value="Female" required="true">
		<br>
		<br>
		<label for="nationality">Nationality</label><br/>
		<input type="text" class="text" name ="nationality" id="nationality" maxlength="40" value="" required="true">
		<br>
		<br>

		<label for="passport">Passport Number</label><br/>
		<input type="text" class="text" name ="passport" id="passport" maxlength="20" value="" required="true">
		<br>
		<br>
		
		<label for="dateissued">Date Issued</label><br/>
		<input type="date" class="text" name ="expirtiondate" id="expirtiondate" value="" required="true">	
		<br>
		<br>
	
		<label for="expirationdate">Expiration Date</label><br/>
		<input type="date" class="text" name ="expirationdate" id="expirationdate" value="" required="true">
		<br>
		<br>
		<b>Marital Status:</b>
		<br>
		<label for="single">Single</label>
		<input type="radio" name="marital" id="single" value="Single" required="true">
  
		<label for="married">Married</label>
		<input type="radio" name="marital" id="married" value="Married" required="true">
  
		<label for="">Widowed</label>
		<input type="radio" name="marital" id="widowed" value="Widowed" required="true">
 
		<label for="remarried">Remarried</label>
		<input type="radio" name="marital" id="remarried" value="Remarried" required="true">
  
		<label for="engaged">Engaged</label>
		<input type="radio" name="marital" id="engaged" value="Engaged" required="true">
		<br>
		<br>

		<label for="nonRSAPerm">If not South African, do you have a residential permit?</label><br/>
		<input type="text" class="text" name ="nonRSAPerm" id="nonRSAPerm" value="" >
		<br>
		<br>
		
		<label for="religion">Religion</label><br/>
		<input type="text" class="text" name ="religion" id="religion" maxlength="30" value="" required="true">
		<br>
		<br>

		<label for="chruch">Church</label><br/>
		<input type="text" class="text" name ="church" id="church" maxlength="30" value="" required="true">
		<br>
		<br>
		<label for="occupation">Current Occupation</label><br/>
		<input type="text" class="text" name ="occupation" id="occupation" maxlength="30" value="" required="true">
		<br>
		<br>

		<label for="vol">What area would you like to volunteer</label><br/>
		<input type="text" class="text" name ="vol" id="vol" maxlength="100" value="" required="true">
		<br>
		<br>

		<b>In case of emergency, your contact person:</b>
		
		<br>
		
		<label for="name">Name</label><br/>
		<input type="text" class="text" name ="name" id="name" maxlength="100" value="" required="true">
		<br>
		<br>

		
		<label for="tel">Tel</label><br/>
		<input type="text" class="text" name ="tel" id="tel" maxlength="20" value="" required="true">
		<br>
		<br>
		
		<label for="Address2">Address</label><br/>
		<input type="text" class="text" name ="addresss2" id="addresss2" maxlength="150" value="" required="true">
		<br>
		<br>
		
		<label for="relationship">Relationship</label><br/>
		<input type="text" class="text" name ="relationship" id="relationship" maxlength="30" value="" required="true">
		<br>
		<br>
		
		<label for="illness">Do you have any medical problems/illnesses?</label><br/>
		<input type="text" class="text" name ="illness" id="illness" maxlength="50" value="" required="true">
		
		<div>
		<h2>Education/Work Experience Information</h2>
            <table>
                <tr>
                    <th>Education Name of School/<br/>Institute/Certificate/<br/>Degree/Name of School/Institute</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
                <tr>
                    <td><input type="text" class="text" name ="institution1" id="institution1" maxlength="50" value="" required="true"></td>
					<td><input type="date" class="text" name ="startDates1" id="startDates1" value="" required="true"></td>
                    <td><input type="date" class="text" name ="endDates1" id="endDates1" value="" required="true"></td>
                </tr>
                <tr>
                    <td><input type="text" class="text" name ="institution2" id="institution2" maxlength="50" value=""></td>
                    <td><input type="date" class="text" name ="startDates2" id="startDates2" value="" ></td>
                    <td><input type="date" class="text" name ="endDates2" id="endDates2" value="" ></td>
                </tr>
                <tr>
                    <td><input type="text" class="text" name ="institution3" id="institution3" maxlength="50" value="" ></td>
					<td><input type="date" class="text" name ="startDates3" id="startDates3" value="" ></td>
                    <td><input type="date" class="text" name ="endDates3" id="endDates3" value="" ></td>
                </tr>          
            </table>
	
		
		<br>
		<br>

		<label for="lang">What languages do you speak?</label><br/>
		<input type="text" class="text" name ="lang" id="lang" maxlength="100" value="" required="true">
		<br>
		<br>

		<label for="cult">Have You Worked with People of Other Cultures?</label><br/>
		<input type="text" class="text" name ="cult" id="cult" maxlength="200" value="" required="true">
		<br>
		<br>
		
		<b>Work Expericence(Past 5 years)</b>
                <table>
                    <tr>
                        <th>Organisation</th>
                        <th>Position</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Reason for leaving</th>
                    </tr>
                    <tr>
                                 <td><input type="text" name ="org1" id="org1" maxlength="50" required="true"></input></td>
                                 <td><input type="text" name ="pos1" id="pos1" maxlength="30" required="true"></input></td>
                                 <td><input type="date" name ="dateStart1" id="dateStart1" required="true"></input></td>
                                 <td><input type="date" name ="dateEnd1" id="dateEnd1" required="true"></input></td>
                                 <td><input type="text" name ="leave1" id="leave1" maxlength="255" required="true"></input></td>
                    </tr>
                    <tr>
                                  <td><input type="text" name ="org2" id="org2" maxlength="50" ></input></td>
                                 <td><input type="text" name ="pos2" id="pos2" maxlength="30" ></input></td>
                                 <td><input type="date" name ="dateStart2" id="dateStart2" ></input></td>
                                 <td><input type="date" name ="dateEnd2" id="dateEnd2" ></input></td>
                                 <td><input type="text" name ="leave2" id="leave2" maxlength="255" ></input></td>
                    </tr>
                    <tr>
                                 <td><input type="text" name ="org3" id="org3" maxlength="50" ></input></td>
                                 <td><input type="text" name ="pos3" id="pos3" maxlength="30" ></input></td>
                                 <td><input type="date" name ="dateStart3" id="dateStart3" ></input></td>
                                 <td><input type="date" name ="dateEnd3" id="dateEnd3" ></input></td>
                                 <td><input type="text" name ="leave3" id="leave3" maxlength="255" ></input></td>
                    </tr>
                    <tr>
                                 <td><input type="text" name ="org4" id="org4" maxlength="50" ></input></td>
								 <td><input type="text" name ="pos4" id="pos4" maxlength="30" ></input></td>
                                 <td><input type="date" name ="dateStart4" id="dateStart4" ></input></td>
                                 <td><input type="date" name ="dateEnd4" id="dateEnd4" ></input></td>
                                 <td><input type="text" name ="leave4" id="leave4" maxlength="255" ></input></td>
                    </tr>
                    <tr>
                                 <td><input type="text" name ="org5" id="org5" maxlength="50" ></input></td>
                                 <td><input type="text" name ="pos5" id="pos5" maxlength="30" ></input></td>
                                 <td><input type="date" name ="dateStart5" id="dateStart5" ></input></td>
                                 <td><input type="date" name ="dateEnd5" id="dateEnd5" ></input></td>
                                 <td><input type="text" name ="leave5" id="leave5" maxlength="255" ></input></td>
                    </tr>
                </table>
		<br>
		<br>

		<label for="skill">Do you have any other skills or work experience?</label><br/>
		<br>
		<input type="text" class="text" name ="skill" id="skill" maxlength="100" value="" required="true">
		<br>
		<br>

		<label for="hobby">What are your hobbies and interests?</label><br/>
		<input type="text" class="text" name ="hobby" id="hobby" maxlength="50" value="" required="true">	
		
		<h2>Experience with LIV</h2>
		
		<label for="priorHist">Have you ever worked with a non-profit organisation? If yes, please elaborate:</label><br/>
		<br>
		<input type="text" class="text" name ="priorHist" id="priorHist" maxlength="150" value="" required="true">
		<br>
		<br>
		
		<label for="liv">How do you know about LIV</label><br/>
		<input type="text" class="text" name ="liv" id="liv" maxlength="100" value="" required="true">
		<br>
		<br>
		
		
		<label for="livInvovlement">Have you been involved with LIV? If so, please elaborate (include dates, etc)</label><br/>
		<br>
		<input type="text" class="text" name ="txtlivInvolvement" id="txtlivInvovlement" maxlength="150" value="" required="true">
		<br>
		<br>

		<label for="whyLIV">Why do you want to volunteer with LIV?</label><br/>
		<input type="text" class="text" name ="txtwhyLIV" id="txtwhyLIV" maxlength="150" value="" required="true">
		<br>
		<br>

		<label for="expectaions">What expectations do you have as a volunteer?</label><br/>
		<input type="text" class="text" name ="txtexpectaions" id="txtexpectaions" maxlength="100" value="" required="true">		
		</div>
		
		<div>
		<h2>Background Information</h2>
		
		<p>We are working with children, and some are at very sensitive stages in their lives. Many of them
come from broken families and problematic lifestyles. Some of them have been involved in illegal
and other harmful activities and, through the help of our programmes, are overcoming these
problems. It is very important that the staff and volunteers of LIV help to provide a good
atmosphere that will contribute to the wholesome development of the children. If for some reason,
you have difficulty communicating your answer in writing, we can talk with you personally. Your
answers to these questions will not, in themselves, prejudice your acceptance. All answers are
confidential.</p>

<br>
<label for="drugHist">Have you used any illegal drugs or been an alcoholic? If so, please explain how recently and in
what quantities</label><br/>
<br>
		<input type="text" class="text" name ="txtdrugHist" id="txtdrugHist" maxlength="150" value="" required="true"> 
		<br>
		<br>
		
		<label for="smoke">Do you currently smoke?</label><br/>
		<input maxlength="10" type="text" class="text" name ="txtSmoke" id="txtSmoke" value="" required="true">
		<br>
		<br>

		<label for="inebriated ">Do you get inebriated occasionally or regularly?</label><br/>
		<input type="text" class="text" name ="txtInebriated" id="txtInebriated " maxlength="50" value="" required="true">
		<br>
		<br>

		<label for="psychTreatment">Have you ever had any psychiatric/psychological treatment? If so, describe treatment received,
dated and/or present difficulties.</label><br/>
<br>
		<input type="text" class="text" name ="txtPsychTreatment" id="txtPsychTreatment" maxlength="100" value="" required="true">
		<br>
		<br>

		<label for="chilhoodNote">Please describe your family or origin and the circumstances of your childhood/teenage years, etc.
Please mention any adverse issues that affected you, i.e. family mental disorder, marital instability,
child abuse, alcoholism, drug abuse or family dysfunction and how you have been dealing with
these issues in your life.</label><br/>
<br>
		<input type="text" class="text" name ="txtChilhoodNote" id="txtChilhoodNote" maxlength="100" value="" required="true">
		<br>
		<br>

		<label for="familyViews">What does your family think about your involvement with LIV?</label><br/>
		<br>
		<input type="text" class="text" name ="txtFamilyViews" id="txtFamilyViews" maxlength="100" value="" required="true">
		<br>
		<br>

		<label for="leadershipRelations">Please describe how you relate to others (Friends, leaders, colleagues, spouse and children)
Describe some positive and negative past team experiences. Describe your relationship with
leadership/authority figures.</label><br/>
<br>
		<input type="text" class="text" name ="txtLeadershipRelations" id="txtLeadershipRelations" maxlength="100" value="" required="true">
		<br>
		<br>
		
		<label for="conviction">Have you ever been convicted of a crime? If yes, please briefly describe the nature of the crime(s), the date and place of conviction and the
legal disposition of the case.</label><br/>

<br>
  <input type="text" class="text" name="con" maxlength="100" value="" required="true">

		
  
  <br>
  <br>
  
  <p>PLEASE NOTE:
The organisation will not deny involvement to any applicant solely because a person has been
convicted of a crime. The organisation, however, may consider the nature, date and circumstances
of the offence as well and whether the offence is relevant to the duties of the position applied for</p>

<br>
<p>If you have been found guilty of any child abuse offences or been suspected of child abuse
please provide reliable character witnesses on your behalf</p>
		</div>
		
		<div>
		<h2>References</h2>
		<table>
                        <tr>
                        <th>Name</th>
                        <th>Organisation</th>
                        <th>Position Held</th>
                        <th>Email</th>
                        <th>Tel</th>
                        </tr>
                        <tr>
                                 <td><input type="text" name ="txtRefName1" id="txtRefName1" maxlength="100" required="true"></input></td>
                                 <td><input type="text" name ="txtRefOrg1" id="txtRefOrg1" maxlength="50" required="true"></input></td>
                                 <td><input type="text" name ="txtRefPosition1" id="txtRefPosition1" maxlength="30" required="true"></input></td>
                                 <td><input type="email" name ="txtRefMail1" id="txtRefMail1" maxlength="254" required="true"></input></td>
                                 <td><input type="text" name ="txtRefTel1" id="txtRefTel1" maxlength="20" required="true"></input></td>
                        </tr>
                        <tr>
                                 <td><input type="text" name ="txtRefName2" id="txtRefName2" maxlength="100" ></input></td>
                                 <td><input type="text" name ="txtRefOrg2" id="txtRefOrg2" maxlength="50" ></input></td>
                                 <td><input type="text" name ="txtRefPosition2" id="txtRefPosition2" maxlength="30" ></input></td>
                                 <td><input type="email" name ="txtRefMail2" id="txtRefMail2" maxlength="254" ></input></td>
                                 <td><input type="text" name ="txtRefTel2" id="txtRefTel2" maxlength="20" ></input></td>
                        </tr>
                        <tr>
                                 <td><input type="text" name ="txtRefName3" id="txtRefName3" maxlength="100" ></input></td>
                                 <td><input type="text" name ="txtRefOrg3" id="txtRefOrg3" maxlength="50" ></input></td>
                                 <td><input type="text" name ="txtRefPosition3" id="txtRefPosition3" maxlength="30" ></input></td>
                                 <td><input type="email" name ="txtRefMail3" id="txtRefMail3" maxlength="254" ></input></td>
                                 <td><input type="text" name ="txtRefTel3" id="txtRefTel3" maxlength="20" ></input></td>
                        </tr>
                        <tr>
                                 <td><input type="text" name ="txtRefName4" id="txtRefName4" maxlength="100" ></input></td>
                                 <td><input type="text" name ="txtRefOrg4" id="txtRefOrg4" maxlength="50" ></input></td>
                                 <td><input type="text" name ="txtRefPosition4" id="txtRefPosition4" maxlength="30" ></input></td>
                                 <td><input type="email" name ="txtRefMail4" id="txtRefMail4" maxlength="254" ></input></td>
                                 <td><input type="text" name ="txtRefTel4" id="txtRefTel4" maxlength="20" ></input></td>
                        </tr>
                        <tr>
                                 <td><input type="text" name ="txtRefName5" id="txtRefName5" maxlength="100" ></input></td>
                                 <td><input type="text" name ="txtRefOrg5" id="txtRefOrg5" maxlength="50" ></input></td>
                                 <td><input type="text" name ="txtRefPosition5" id="txtRefPosition5" maxlength="30" ></input></td>
                                 <td><input type="email" name ="txtRefMail5" id="txtRefMail5" maxlength="254" ></input></td>
                                 <td><input type="text" name ="txtRefTel5" id="txtRefTel5" maxlength="20" ></input></td>
                        </tr>
                </table>
	
		<br>
		<br>
		<label for="moreInfo">Is there any other information that will be helpful for us to know as we consider your application?</label><br/>
		<br>
		<input type="text" class="text" name ="txtMoreInfo" id="txtMoreInfo" maxlength="100" value="" required="true">
		
		<br>
		<h3>APPLICATION FORM WAIVER/DECLARATION</h3>
		<p>I authorise investigation of all statements contained in this application. I understand that the misrepresentation
or omission of facts called for is cause for dismissal at any time without previous
notice.
I hereby give the organisation permission to contact schools, previous employers (unless otherwise
indicated), references and others, and hereby release the organisation from any liability as a result
of such contact.</p>

<h3>
PLEASE UPLOAD WITH YOUR APPLICATION, A COPY OF YOUR ID/PASSPORT, POLICE
CLEARANCE</h3>
<h3>Copy of ID/Passport</h3>
<input type="file" name="idOrPassport" id="idOrPassport" required="true"/>
<h3>Police Check</h3>
<input type="file" name="policeCheck" id="policeCheck" required="true"/>
<h3>Note: By submitting your application, you agree to LIV's code of conduct, which is available to read <a href="codeOfConduct.pdf" target="_blank">here</a>, as well as LIV's Volunteer policy, which is available to read <a href="volunteerPolicy.pdf" target="_blank">here</a></h3>
		</div>
		
		<button type="submit" name="submit" id="btnSubmit">Submit
		
		</button>
		</form>
</body>
</html>

<?php
	}
?>