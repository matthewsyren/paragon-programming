<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		include "navbar.php";
		include "dbConn.php";
		
		//Calls function to populate user's current data
		getData();
	}	
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
		//Function populates the from with the user's current data
		function getData(){
			$username = $_SESSION["Username"];
			$dbConnect = new dbConnect();
			$sql = "select FirstName, LastName, EmailAddress, SkypeAddress FROM tblUsers WHERE Username = '$username'";
			$result = $dbConnect->executeQuery($sql);
			if($result-num_rows > 0){
				$row = $result->fetch_assoc();
				$arrDisplay['firstName'] = $row["FirstName"];
				$arrDisplay['lastName'] = $row["LastName"];
				$arrDisplay['newEmail'] = $row["EmailAddress"];       
				$arrDisplay['skypeAddress'] = $row["SkypeAddress"];    

				//Ensures the user has logged in before using the page
				if(isset($_SESSION["Username"])){				
?>
                   
                   
<html>
	<body>
		<form name = "reset"  method = "POST">
		<h1>Your Details</h1>
		<p><label>First Name:</label> <input maxlength="55" required="true" type = "text" name = "firstName" value="<?php echo $arrDisplay['firstName']; ?>"/> </p>
		<p><label>Last Name:</label> <input maxlength="55" required="true" type = "text" name = "lastName" value="<?php echo $arrDisplay['lastName']; ?>"/> </p>
		<p><label>Email Address:</label> <input maxlength="254" required="true" type = "text" name = "newEmail" value="<?php echo $arrDisplay['newEmail']; ?>"/> </p>
		<p><label>Skype Address:</label> <input maxlength="265" required="true" type = "text" name = "skypeAddress" value="<?php echo $arrDisplay['skypeAddress']; ?>"/> </p>
		<button type="submit" name = "reset"><strong>Save</strong></button> 
		</form>
        <br/>
        <button onclick="changePassword()">Change Password</button>
	</body>
</html>
<?php
				}
			}
		}

		if(isset($_POST["getData"])){
				getData();
		}

		if(isset($_SESSION["Username"])){
			include "styles.css";
			include "sharedFunctions.php";
	  
			$arrDisplay = array('firstName' => '', 'lastName' => '', 'newEmail' => '', 'skypeAddress' => '');   
			
			//Processes input once user submits form
			if(isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["newEmail"]) && isset($_POST["skypeAddress"])){
				//Assigns the values that the user entered to variables (formatInput() ensures that the input is safe to use)
				$firstName = formatInput($_POST["firstName"]);
				$lastName = formatInput($_POST["lastName"]);
				$newEmail = formatInput($_POST["newEmail"]);
				$skypeAddress = formatInput($_POST["skypeAddress"]);
									   
				//Sets values to ensure that form input fields don't lose value after submission
				$arrDisplay['firstName'] = $firstName;
				$arrDisplay['lastName'] = $lastName;
				$arrDisplay['newEmail'] = $newEmail;
				$arrDisplay['skypeAddress'] = $skypeAddress;
									  
				//If statements ensure that the entered information is valid. If any information is not valid, $valid is set to false, and the program prompts the user for valid input
				$valid = true;
				if (empty($firstName)){
					echo "<script>alert('Please enter a first name');</script>";
					$valid = false;
				}
				if (empty($newEmail) || checkEmail($newEmail) == false){
					echo "<p class='error'>Please enter a valid email address</p>";
					$valid = false;
				}
				if (empty($lastName)){
					echo "<p class='error'>Please enter a last name</p>";
					$valid = false;
				}
				if (empty($skypeAddress)){
					echo "<p class='error'>Please enter a last name</p>";
					$valid = false;
				}
					   
				//Writes information to dataase if the information is valid
				if($valid == true){
					//Updates users name and email information
					$dbConnect = new dbConnect();
					$username = $_SESSION["Username"];
					$sql = "update tblUsers set FirstName ='$firstName',EmailAddress = '$newEmail', LastName = '$lastName', SkypeAddress = '$skypeAddress' where Username = '$username';";
					$success = $dbConnect->executeQuery($sql);
					if($success == true){
						echo "<script>alert('Your account details have been successfully updated!');</script>";
						redirectPage("index.php");
					}
					else{
						echo "<p class='error'>There was an error while updating your account details, please try again</p>";
					}                    
				}
			}
		}
?>
<script>
    //Function takes the user to the change password page    
	function changePassword(){
        window.location.pathname = "changePassword.php";
    }
</script>