<?php
	class workExperience{
		//Attributes
		public $position;
		public $organisation;
		public $startDate;
		public $endDate;
		public $reasonForLeaving;
		
		//Constructor		   
		function __construct($position, $organisation, $startDate, $endDate, $reasonForLeaving){
			$this->position = $position;
			$this->organisation = $organisation;
			$this->startDate= $startDate;
			$this->endDate = $endDate;
			$this->reasonForLeaving= $reasonForLeaving;
		}
	}
?>