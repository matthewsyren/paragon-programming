<?php
    if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		if(!isset($_POST["ajaxResponse"])){
?>
<html>
<style>
	#div1{
		background-color: #f99f18;
		cursor:pointer;
		min-height:200px;
		min-width: 48%;
        width: auto;
		display: inline-block;
		margin: auto;
        border-radius: 20px;
        height: auto;
	}
	
	#div2{
		background-color: #0fb5fc;
		cursor:pointer;
		min-height:200px;
        height: auto;
		min-width: 48%;
        width: auto;
		display: inline-block;
		margin: auto;
		float: right;
        border-radius: 20px;
	}
</style>
	<body>
        <?php
				include "styles.css";
				include "navbar.php";
			}
			
			include "sharedFunctions.php";
				
			//Sets the session variable for the appropriate accommodation type	
			if(isset($_POST["bookForLodge"])){
				$_SESSION["accommodationType"] = "Lodge";
				echo 1;
			}
			else if(isset($_POST["bookForBarracks"])){
				$_SESSION["accommodationType"] = "Barracks";
				echo 1;
			}
				
			if(!isset($_POST["ajaxResponse"])){
        ?>
		<div>
			<h1>Which accommodation would you like to stay in?</h1>
			<div onclick="bookForBarracks()"id='div1'>
				<h1>Barracks</h1>
				<p>Volunteers and gap-year students may stay here.</p>
			</div>
			<div onclick="bookForLodge()" id='div2'>
				<h1>Lodge</h1>
				<p>Guests and Volunteers may stay here.</p>
			</div>
		</div>
	</body>
</html>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
<script>
	//Function sets the accommodation to be booked to the lodge
    function bookForLodge(){
        $.ajax({
            url: window.location.pathname,
            type: "post",
            data: {"bookForLodge": "1", "ajaxResponse" : "1"},
            success: function(response){
                if(response == 1){
                    window.location = "accommodationBooking.php";
                }
                else{
                    alert("An error occured while opening the accommodation booking page, please try again...");
                }
            }           
        });
    }
        
	//Function sets the accommodation to be booked to the barracks
    function bookForBarracks(){
        $.ajax({
            url: window.location.pathname,
            type: "post",
            data: {"bookForBarracks": "1", "ajaxResponse" : "1"},
            success: function(response){
                if(response == 1){
                    window.location = "accommodationBooking.php";
                }
                else{
                    alert("An error occured while opening the accommodation booking page, please try again...");
                }                
            }          
        });
    }
</script>

<?php
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>