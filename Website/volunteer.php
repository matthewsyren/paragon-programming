<?php
	class Volunteer{
		//Atttributes
		public $username;
		public $title;
		public $address;
		public $homeTelephoneNumber;
		public $businessTelephoneNumber;
		public $cellNumber;
		public $dateOfBirth;
		public $age;
		public $gender;
		public $nationality;
		public $passportNumber;
		public $dateIssued;
		public $validUntil;
		public $maritalStatus;
		public $residentialPermit;
		public $religion;
		public $church;
		public $currentOccupation;
		public $areaToVolunteer;
		public $emergencyContactName;
		public $emergencyContactTelephone;
		public $emergencyContactAddress;
		public $relationship;
		public $medicalProblems;
		public $languagesSpoken;
		public $interculturalExperience;
		public $otherSkillsOrWorkExperience;
		public $hobbiesAndInterests;
		public $workedWithNGOBefore;
		public $howLIVWasDiscovered; 
		public $previousExperienceWithLIV;
		public $reasonsForVolunteering;
		public $expectationsAsAVolunteer;
		public $substanceAbuseHistory;
		public $smoker;
		public $inebriationFrequency;
		public $pastPsychologicalTreatment;
		public $lifeCircumstancesAndChallenges;
		public $familyOpinionOfLIV;
		public $relationshipWithOthers;
		public $criminalConvictions;
		public $additionalInformation;
		
		//Constructor
		function __construct($username, $title, $address, $homeTelephoneNumber, $businessTelephoneNumber, $cellNumber, $dateOfBirth,
		$age, $gender, $nationality, $passportNumber, $dateIssued, $validUntil, $maritalStatus, $residentialPermit, $religion, $church, $currentOccupation, $areaToVolunteer, 
		$emergencyContactName, $emergencyContactTelephone, $emergencyContactAddress, $relationship, $medicalProblems, $languagesSpoken, $interculturalExperience, 
		$otherSkillsOrWorkExperience, $hobbiesAndInterests, $workedWithNGOBefore, $howLIVWasDiscovered, $previousExperienceWithLIV, $reasonsForVolunteering, $expectationsAsAVolunteer,
		$substanceAbuseHistory, $smoker, $inebriationFrequency, $pastPsychologicalTreatment, $lifeCircumstancesAndChallenges, $familyOpinionOfLIV, $relationshipWithOthers, $criminalConvictions, 
		$additionalInformation){
			$this->username = $username;
			$this->title = $title;
			$this->address = $address;
			$this->homeTelephoneNumber = $homeTelephoneNumber;
			$this->businessTelephoneNumber = $businessTelephoneNumber;
			$this->cellNumber = $cellNumber;
			$this->dateOfBirth = $dateOfBirth;
			$this->age = $age;
			$this->gender = $gender;
			$this->nationality = $nationality;
			$this->passportNumber = $passportNumber;
			$this->dateIssued = $dateIssued;
			$this->validUntil = $validUntil;
			$this->maritalStatus = $maritalStatus;
			$this->residentialPermit = $residentialPermit;
			$this->religion = $religion;
			$this->church = $church;
			$this->currentOccupation = $currentOccupation;
			$this->areaToVolunteer = $areaToVolunteer;
			$this->emergencyContactName = $emergencyContactName;
			$this->emergencyContactTelephone = $emergencyContactTelephone;
			$this->emergencyContactAddress = $emergencyContactAddress;
			$this->relationship = $relationship;
			$this->medicalProblems = $medicalProblems;
			$this->languagesSpoken = $languagesSpoken;
			$this->interculturalExperience = $interculturalExperience;
			$this->otherSkillsOrWorkExperience = $otherSkillsOrWorkExperience;
			$this->hobbiesAndInterests = $hobbiesAndInterests;
			$this->workedWithNGOBefore = $workedWithNGOBefore;
			$this->howLIVWasDiscovered = $howLIVWasDiscovered;
			$this->previousExperienceWithLIV = $previousExperienceWithLIV;
			$this->reasonsForVolunteering = $reasonsForVolunteering;
			$this->expectationsAsAVolunteer = $expectationsAsAVolunteer;
			$this->substanceAbuseHistory = $substanceAbuseHistory;
			$this->smoker = $smoker;
			$this->inebriationFrequency = $inebriationFrequency;
			$this->pastPsychologicalTreatment = $pastPsychologicalTreatment;
			$this->lifeCircumstancesAndChallenges = $lifeCircumstancesAndChallenges;
			$this->familyOpinionOfLIV = $familyOpinionOfLIV;
			$this->relationshipWithOthers = $relationshipWithOthers;
			$this->criminalConvictions = $criminalConvictions;
			$this->additionalInformation = $additionalInformation;
		}
	}
?>