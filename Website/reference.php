<?php
	class reference{
		//Attributes
		public $name;
		public $organisation;
        public $position;
		public $telephone;
		public $email;
		
		//Constructor		   
		function __construct($name, $organisation, $position, $telephone, $email){
			$this->name = $name;
			$this->organisation = $organisation;
			$this->position= $position;
			$this->telephone = $telephone;
			$this->email= $email;
		}
	}
?>