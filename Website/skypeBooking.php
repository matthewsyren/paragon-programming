<?php
	session_start();
	include "dbConn.php";
	include "skype.php";
	include "sharedFunctions.php";
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		//Calls the dateClicked function is the data for that function has been posted
		if(isset($_POST["dateClicked"]) && isset($_POST["livEmployee"])){
			getAvailableTimes($_POST["dateClicked"], $_POST["livEmployee"]);
			unset($_POST["dateClicked"]);
			unset($_POST["livEmployee"]);
		}
		else if(isset($_POST["bookingDate"]) && isset($_POST["bookingTime"]) && isset($_POST["bookedLivEmployee"])){
			makeBooking($_POST["bookingDate"], $_POST["bookingTime"], $_POST["bookedLivEmployee"]);
			unset($_POST["bookingDate"]);
					unset($_POST["bookingTime"]);
					unset($_POST["bookedLivEmployee"]);
		}
		else if(isset($_POST["populateLivEmployees"])){
			getLivEmployees();
			unset($_POST["populateLivEmployees"]);
		}
		else if(isset($_POST["getUpcomingAppointments"])){
			   getUpcomingAppointments($_POST["livEmployee"]);
		}
	
	
		$username = $_SESSION["Username"];
		$sql = "select * from tblApplications where Username = '$username' and SkypeInterviewAccepted = 1";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows == 0){
			echo "<script>alert('LIV has not requested a Skype interview with you, please apply to come to LIV and wait for feedback before trying to book a Skype interview');</script>";
			redirectPage("skypeChoice.php");
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
   
	//Function fetches the available times for the selected date
	function getAvailableTimes($date, $livEmployee){
		$dbConnect = new dbConnect();
		$date = date("Y-m-d", strtotime($date));
		$sql = "select SkypeTime from tblSkypeTimes where SkypeDate ='$date' AND LivEmployee = '$livEmployee' AND ApplicantUsername = ''";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			while($rows = $result->fetch_assoc()){
              	echo "<option>" . $rows["SkypeTime"] . "</option>";
			}
		}
	}
   
	//Function fetches the user's upcoming Skype bookings and echoes them into a table
	function getUpcomingAppointments($livEmployee){
        $dbConnect = new dbConnect();
        $date = date('Y-m-d');
        $sql = "select * from tblSkypeTimes where LivEmployee = '$livEmployee' and ApplicantUsername = '' and SkypeDate >= '$date' order by SkypeDate asc";
        $result = $dbConnect->executeQuery($sql);
		
		//Echoes the bookings into a table
        if($result->num_rows > 0){
            echo "<tr><th>Date</th><th>Time</th><th>LIV Employee</th><th></th></tr>";
            while($row = $result->fetch_assoc()){
                echo "<tr><td>" . $row["SkypeDate"] . "</td>";
                echo "<td>" . $row["SkypeTime"] . "</td>";
                echo "<td>" . $row["LivEmployee"] . "</td>";
                echo "<td><button onclick='makeBooking(this)'>Book</button></td></tr>";
            }
        }
		else{
			echo "<center>$livEmployee has no upcoming available time slots</center>";
		}
	}
   
	//Function makes a booking for a Skype interview 
	function makeBooking ($date,$time,$livEmployee){
		//Gets required details
	    $username = $_SESSION["Username"];
	    $dbConnect = new dbConnect();
        $date = date("Y-m-d", strtotime($date));
        $skype = new Skype($date, $time, $livEmployee, $username);
       
		//Updates database to make booking
        $sql = "update tblSkypeTimes set ApplicantUsername = '$skype->applicantUsername' where SkypeDate ='$skype->date' AND LivEmployee = '$skype->livEmployee' AND SkypeTime ='$skype->time'";
        $result = $dbConnect->executeQuery($sql);
		if($result == 1){
			//Sends an email to the appropriate LIV employee informing them of the booking
			sendMailToLivEmployee($date, $time, $livEmployee);
		}
        echo $result;	   
	}
   
	//Sends an email to the appropriate LIV employee telling them the details about the Skype meeting
	function sendMailToLivEmployee($date, $time, $livEmployee){
		$username = $_SESSION["Username"];
		
		//Gets LIV employee's details
		$sql = "select FirstName, EmailAddress from tblLivEmployees where Username = '$livEmployee'";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$livEmployeeName = $row["FirstName"];
			$livEmployeeEmailAddress = $row["EmailAddress"];
			
			//Gets applicant's details
			$sql = "select FirstName, LastName, EmailAddress from tblUsers where Username = '$username'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				$row = $result->fetch_assoc();
				$applicantName = $row["FirstName"] . ' ' . $row["LastName"];
				$applicantEmailAddress = $row["EmailAddress"];
				
				//Sends email
				$eol = PHP_EOL;
				$message = stripslashes("Hi, $livEmployeeName. $eol $eol" . "$applicantName (Username: $username, Email address: $applicantEmailAddress) has booked a Skype interview with you for $date at $time. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
				mail($livEmployeeEmailAddress, $applicantName . " Skype Interview", $message);
				sendMailToApplicant($date, $time, $livEmployee, $applicantEmailAddress, $row["FirstName"], $livEmployeeEmailAddress, $livEmployeeName);
			}
		}
	}
	
	//Function sends email to the applicant confirming that they have scheduled a booking 
	function sendMailToApplicant($date, $time, $livEmployee, $applicantEmailAddress, $applicantName, $livEmployeeEmailAddress, $livEmployeeName){
		$eol = PHP_EOL;
		$message = stripslashes("Hi, $applicantName. $eol $eol". "You have successfully booked a Skype Interview with $livEmployeeName (Username: $livEmployee, Email Address: $livEmployeeEmailAddress) for $date at $time. $eol $eol". "Kind regards, $eol" . "LIV Portal");
		mail($applicantEmailAddress, "Your Skype Interview", $message);
	}
   
	//Function echoes the names of the LIV employees
	function getLivEmployees(){
	   $dbConnect = new dbConnect();
	   $sql = "select distinct LivEmployee from tblSkypeTimes";
	   $result = $dbConnect->executeQuery($sql);
	   
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
			   echo "<option>" . $row["LivEmployee"] . "</option>";
			}
		}
	}
   
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "styles.css";
			include "loaderStyle.php";
			echo "<br>";
   ?>
   
   <html>
       <h1>Skype Booking</h1>
   </html>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
<script>
	//Calls setListeners function when the page loads
	window.onload = setListeners;

	//Function fetches the upcoming available times that a LIV employee has set
    function getUpcomingAppointments(){
		displayLoader();
        var cmbLivEmployee = document.getElementById("cmbLivEmployees");
        var livEmployee = $(cmbLivEmployee).val();
            
        $.ajax({
            url: window.location.pathname, 
            type: "post",
            data: {"getUpcomingAppointments": "1", "livEmployee": livEmployee, "ajaxResponse": "1"},
            success: function(response){
                        var tblUpcomingAppointments = document.getElementById("tblUpcomingAppointments");
                        tblUpcomingAppointments.innerHTML = response;
						hideLoader();
					}
        });
    }
    
	//Function sets listeners that will perform specific actions when the user performs a specific event
    function setListeners(){
		displayLoader();
        var cmbLivEmployees = document.getElementById("cmbLivEmployees");        
		
		//Fetches available LIV employees
        $.ajax({
			url: window.location.pathname,
			type: "post",
			data: {"populateLivEmployees" : "1"}, 
			success: function(response){
				cmbLivEmployees.innerHTML = response;
                getUpcomingAppointments();
				hideLoader();
			}
		});

		//Fetches the available times for the selected LIV employee
        cmbLivEmployees.addEventListener("change", function() {
			var firstDate = $(cmbLivEmployees).val();
			getUpcomingAppointments();
		});                             
	}
                
	//Function books a time slot for a Skype interview for the user
	function makeBooking(element){
		displayLoader();
        var rowNum = element.parentNode.parentNode.rowIndex; 
		var tblAppointments = document.getElementById("tblUpcomingAppointments");
		var date = tblAppointments.rows[rowNum].cells[0].innerHTML;
        var time = tblAppointments.rows[rowNum].cells[1].innerHTML;
        var livEmployee = tblAppointments.rows[rowNum].cells[2].innerHTML;
                
		//Sends details to PHP side of the code to make a booking
        $.ajax({
			url: window.location.pathname,
			type: "post",
			data: {"bookingDate" : date, "bookingTime" : time , "bookedLivEmployee" : livEmployee , "ajaxResponse": "1"},
			success: function(response){
                if(response == 1){             
                    alert("Skype booking made");
                    location.reload();
                }
                else{
                    alert("An error occured while booking your Skype interview, please try again...");
                }
				hideLoader();
			}
		});                
	}
</script>
<html>
	<p><label>LIV Employee</label><br/>
    <select id="cmbLivEmployees"></select></p>
    <h2>Upcoming Available Appointments</h2>
    <table id="tblUpcomingAppointments" class="report"></table>
    <br>
    <h3>Note: Times listed are in Central African Time</h3>
</html>
<?php
		}
    } 
?>