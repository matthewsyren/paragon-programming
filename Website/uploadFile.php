<?php 
	session_start();
	
	include "dbConn.php";
	//Retrieves the files that the user has uploaded
	if ($_FILES['ID']['error'] != NULL && $_FILES['Clearance']['error'] != NULL) {
		echo "An error occurred while uploading your file, please try again later...";
	}
	else {
		sendEmail();
	}
	
	//Function sends an email with the gap-year student's information to the appropriate LIV employees
	function sendEmail(){  
		$username = $_SESSION["Username"];
		$sql = "select EmailAddress, FirstName, LastName from tblUsers where Username = '$username'";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$emailAddress = $row["EmailAddress"];
			$firstName = $row["FirstName"];
			$fullName = $row["FirstName"] . ' ' . $row["LastName"];
			$eol = PHP_EOL;
			$userMessage = "Hi, $firstName. $eol $eol" . "Your application to be a gap-year student at LIV has been received, please wait for a LIV employee to contact you. $eol $eol" . "Kind regards, $eol" . "LIV Portal";
			mail($emailAddress, "Your Gap-year student application", $userMessage);
			
			$sql = "select * from tblLivEmployees where JobTitle = 'Active'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				while($row = $result->fetch_assoc()){	
					$from = "noreply@a15008377.dx.am";

					//Attachment code learnt and adapted from https://www.youtube.com/watch?v=zYocypr0Xig
					//Saves the files' information into variables
					$file_name = $_FILES["ID"]["name"];
					$temp_name = $_FILES["ID"]["tmp_name"];
					$file_type = $_FILES["ID"]["type"];
									
					$file_name1 = $_FILES["Clearance"]["name"];
					$temp_name1 = $_FILES["Clearance"]["tmp_name"];
					$file_type1 = $_FILES["Clearance"]["type"];

					$from = "noreply@a15008377.dx.am";
					$to = $row["EmailAddress"];
					$name = $row["FirstName"];
					$subject = "$fullName Gap-year student application";
																		
					$message = stripslashes("Hi, $name." . "<br/><br/>" . "$fullName has applied to be a Gap-year student. <br/><br/> Here are their details- <br/>" . $_POST["EmailContent"]);                         
											 
					$file = $temp_name;
					$content = chunk_split(base64_encode(file_get_contents($file)));
					$uid = md5(uniqid(time()));
					
					//Sets header information
					$header = "From: " . $from . $eol;
					$header .= "Reply-To: " . $replyto . $eol;
					$header .= "MIME-Version: 1.0".$eol;                                     
					$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"" . $eol;
					$header .= "This is a multipart message in MIME format. " . $eol;
											
					//Content (message body)
					$header .= "--" . $uid . $eol;
					$header .= "Content-type:text/html; charset=iso-8859-1".$eol;
					$header .= "Content-Transfer-Encoding: 7bit".$eol;
					$header .= $message . $eol;
											 
					//Attachment 1
					$header .= "--" . $uid . $eol;
					$header .= "Content-type: " .$file_type . "; name=\"" . $file_name . "\"". $eol;
					$header .= "Content-Transfer-Encoding: base64" . $eol;
					$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"" .$eol;
					$header .= $content;
							 
					//Attachment 2
					$file = $temp_name1;
					$content = chunk_split(base64_encode(file_get_contents($file)));
					
					$header .= "--" . $uid . $eol;
					$header .= "Content-type: " .$file_type1 . "; name=\"" . $file_name1 . "\"". $eol;
					$header .= "Content-Transfer-Encoding: base64" . $eol;
					$header .= "Content-Disposition: attachment; filename=\"".$file_name1."\"" .$eol;
					$header .= $content . $eol;

					//Sends email
					if(mail($to, $subject, "", $header)){
						echo "Success";
					}
					else{
						echo "Email failed to send, please try again";
					}
				}
			}
		}
    }
?>