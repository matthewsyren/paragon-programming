<?php session_start(); ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Login Page</title>
<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<style>
	img{
		position: absolute;
		left: 50%;
		margin-left: -50px
	}

</style>
<body>
<?php
	
	// Declare variables.
	$displayForm = true;
	$UsernameEntered = "";
	$PasswordEntered = "";
	$Status;
	
	include("dbConn.php");
	include("sharedFunctions.php");
    include("styles.css");
		
	// If the Submit button is clicked, execute this code.
	if(isset($_POST['Login'])){
		// Bring the values from the form and store it in the variables.
		$UsernameEntered = formatInput($_POST['Username']);
		$PasswordEntered = formatInput($_POST['EnteredPassword']);
		
		// Validate the first name and last name.
		if(validateUser($UsernameEntered) && validatePassword($PasswordEntered))
		{
            $dbConnect = new dbConnect();
			// Compare the entered details to the details in the DB if user exists.
			$Status = checkIfUserExists($UsernameEntered, $PasswordEntered, $dbConnect);
			if($Status){
				$displayForm = false;
				echo "Login success!";
			}
		}
		else{
			$displayForm = true;
		}
		
	}
	if ($displayForm) {
?>
	<h1>LIV Portal Login</h1>
	<img src="loginPhoto.png" width='100' height='100'></img>
	<br/><br/><br/><br/><br/>
	<h3>Please enter your login details.</h3>
	<form name="Login Form" action="login.php" method="post">
		<p>Username: <input type="username" name="Username" value="<?php echo $UsernameEntered; ?>"/></p>
		<p>Password: <input type="password" name="EnteredPassword" value="<?php echo $PasswordEntered; ?>"/></p>
		<p><a href="registration.php">Don't have an account? Click here to register.</a></p>
		<p><a href="forgotPassword.php" >Forgot Password?</a></p>
		<p><button type="reset" > Clear Form </button>
		&nbsp;
		&nbsp;
		<button type="submit" name="Login"> Login</button></p>
	</form>
<?php	
	}
	
	// Check user is already in the DB. Return true if they are, false if they are not.
	function checkIfUserExists($Username, $PasswordEntered, $dbConnect){		
		// Get the userID from the DB.
		$SQLstring = "SELECT Username, FirstName, LastName, EmailAddress, Password FROM tblUsers WHERE Username ='".$Username."'";
		
        // Execute the SELECT.
		$QueryResult = $dbConnect-> executeQuery($SQLstring);
		
        // Store the results.
		$Row = $QueryResult-> fetch_assoc();
		
        // $Row will be NULL if there is no ID for the user -- the user does not exist in the DB.
		if($Row == NULL){
			echo "<p>User does not exit.</p>";
			return false;
		}
		else{
			if(hash("sha256",$PasswordEntered) == trim($Row['Password'])){
				echo "<p>Welcome, ".$Row['FirstName']."!</p>";
				
				$_SESSION['Username'] = $Row['Username'];
                                
				redirectPage("index.php");
			}
			else{
				echo "<p>Invalid password entered.</p>";
				return false;
			}
		}
	}
	
	// Check if email is in the correct format.
	function validateUser($UsernameEntered){
		if(empty($UsernameEntered)){
			echo "<p>Username field can't be empty</p>";
			return false;
		}
		else{
			return true;
		}
	}
	
	// Check if password is entered.
	function validatePassword($PasswordEntered){
		if(empty($PasswordEntered)){
			echo "<p>Password field can't be empty</p>";
			return false;
		}
		else{
			return true;
		}
	}
?>

</body>
</html>