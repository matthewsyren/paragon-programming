<!-- Loader code learnt and adapted from https://www.w3schools.com/howto/howto_css_loader.asp -->
<div id="loader" class="loader"></div>
<style>
.loader {
	border: 16px solid #000000;
	border-radius: 50%;
	border-top: 16px solid #ff0e0e;
	border-right: 16px solid #000000;
	border-bottom: 16px solid #ff0e0e;
	width: 100px;
	height: 100px;
	-webkit-animation: spin 2s linear infinite;
	animation: spin 2s linear infinite;
	position: absolute;
    top:0;
    bottom: 0;
    left: 0;
    right: 0;

    margin: auto;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}
</style>

<script>
	function displayLoader(){
		var loader = document.getElementById("loader");
		loader.style.display = "inline";
	}
        
    function hideLoader(){
		var loader = document.getElementById("loader");	
		loader.style.display = "none";
	}
</script>