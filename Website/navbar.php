<div class="topnav">
    <img src="navbarPhoto.jpg" height='50' width='50'/>
    <a href="index.php">Home</a>
    <a href="accommodationManagementChoice.php">Accommodation</a>
    <a href="applicationChoice.php">Applications</a>
    <a href="informationPack.pdf" target="_blank">Information</a>
    <a href="scheduleViewer.php">Schedule</a>
    <a href="skypeChoice.php">Skype</a>
    <a style="float:right" onclick="signOut()">Sign out</a>
    <a style="float:right" href="manageAccount.php">Manage Account</a> 
</div>
<br/>
<style>
	.topnav{
		cursor:pointer;
	}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
	//Function tells the PHP side of the code to sign the user out of their account
	function signOut(){
		$.ajax({
			url: window.location.pathname,
            type: "post",
            data: {"signOut" : "1"},
			success: function(response){ 
                alert("Signed out");
                window.location.href = "login.php";
            }
		});
	}
</script>

<?php
	//Calls the signOut() function if the appropriate POST variable has been set
	if(isset($_POST["signOut"])){
		signOut();
		unset($_POST["signOut"]);
	}
	
	//Function signs the user out
	function SignOut() {
		unset($_SESSION['Username']);
	}	
?>