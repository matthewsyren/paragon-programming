<?php
session_start();

if(isset($_SESSION['Username'])){
	include "navbar.php";
    include "styles.css";
    include "sharedFunctions.php";
    include "dbConn.php";
        
    $arrDisplay = array('oldPass' => '', 'newPass' => '');   
        
    if(isset($_POST["oldPass"]) && isset($_POST["newPass"])){
        //Assigns the values that the user entered to variables (formatInput() ensures that the input is safe to use)
        $oldPasswrd = formatInput($_POST["oldPass"]);
        $newPasswrd = formatInput($_POST["newPass"]);
                               
        //Sets values to ensure that form input fields don't lose value after submission
        $arrDisplay['oldPass'] = $oldPasswrd;
        $arrDisplay['newPass'] = $newPasswrd;
                              
        //If statements ensure that the entered information is valid. If any information is not valid, $valid is set to false, and the program prompts the user for valid input
        $valid = true;
        if (empty($oldPasswrd) || checkPassword($oldPasswrd) == false){
            echo "<p class='error'>Please enter a valid password (8 - 12 characters that includes at least 1 uppercase letter and 1 digit)</p>";
            $valid = false;
        }
        if (empty($newPasswrd) || checkPassword($newPasswrd) == false){
            echo "<p class='error'>Please enter a valid password (8 - 12 characters that includes at least 1 uppercase letter and 1 digit)</p>";
            $valid = false;
        }
               
        //Compares the entered information with the information in the database if all entered information is valid
        if($valid == true){
                $username = $_SESSION["Username"];
                $hashedOldPassword = hash('sha256', $oldPasswrd);
                $sql = "select Password from tblUsers where Username = '$username' and Password = '$hashedOldPassword'";
                $dbConnect = new dbConnect();
                $result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    //Updates users password and email information
                    $hashedPassword = hash('sha256', $newPasswrd);
                    $dbConnect = new dbConnect();
                    $username = $_SESSION['Username'];
                    $sql = "update tblUsers set Password ='$hashedPassword' where Username = '$username';";
                    $success = $dbConnect->executeQuery($sql);
                    if($success == true){
                        echo "<script>alert('Your account details have been successfully updated!');</script>";
                        redirectPage("index.php");
                    }
                    else{
                        echo "<p class='error'>There was an error while updating your account details, please try again</p>";
                    }   
                }
                else{
                    echo "<script>alert('Incorrect old password entered');</script>";
                }           
            }
        }
?>

<html>
	<body>
		<form name = "reset"  method = "POST">
		<h1>Change Password</h1>
		<p><label>Old password:</label> <input required="true" type = "password" name = "oldPass" value="<?php echo $arrDisplay['oldPass']; ?>"/> </p>
		<p><label>New Password:</label> <input required="true" type = "password" name = "newPass" value="<?php echo $arrDisplay['newPass']; ?>"/> </p>
		<button type="submit" name = "reset"><strong>Reset</strong></button> 
		</form>
	</body>
</html>

<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>