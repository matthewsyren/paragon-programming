<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		//Includes appropriate files when the page is called without requesting an AJAX response
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "styles.css";
		}
		include ("dbConn.php");
		include "gapYear.php";
		include "sharedFunctions.php";
		
		//Inserts data once the form has been submitted
		if(isset($_POST["submitData"])){
			$dbConnect = new dbConnect();
			$username = $_SESSION["Username"];
			$title = formatInput($_POST["Title"]);
			$Fname = formatInput($_POST["FName"]);
			$Sname = formatInput($_POST["gpSurnme"]);
			$address = formatInput($_POST["address"]);
			$homeNumber = formatInput($_POST["homeNumber"]);
			$businessNumber = formatInput($_POST["businessNum"]);
			$cellNumber = formatInput($_POST["CellphoneNum"]);
			$email = formatInput($_POST["Email"]);
			$birthDate = formatInput($_POST["birthDate"]);
			$age = formatInput($_POST["age"]);
			$gender = formatInput($_POST["gender"]);
			$nationality = formatInput($_POST["nationality"]);
			$passport = formatInput($_POST["passport"]);
			$passportIssueDate = formatInput($_POST["passportIssue"]);
			$passportExpDate = formatInput($_POST["passportExp"]);
			$martialStatus = formatInput($_POST["martial"]);
			$emergencyName = formatInput($_POST["emergencyNme"]);
			$emergencySurname = formatInput($_POST["emergencySurnme"]);
			$emergencyOtherNumber = formatInput($_POST["emergencyOtherNumber"]);
			$emergencyAddress = formatInput($_POST["emergencyAddress"]);
			$emergencyRelationship = formatInput($_POST["emergencyRelationship"]);
			$churchName = formatInput($_POST["churchName"]);
			$churchAddress = formatInput($_POST["churchAddress"]);
			$pastor = formatInput($_POST["pastor"]);
			$pastorEmail = formatInput($_POST["pastorEmail"]);
			$attendance = formatInput($_POST["attendance"]);
			$dateJoined = formatInput($_POST["joined"]);
			$churchActivity = formatInput($_POST["churchActivity"]);
			$mission = formatInput($_POST["Mission"]);
			$educationLevel = formatInput($_POST["eduLevel"]);
			$occupation = formatInput($_POST["occupation"]);
			$homeLanguage = formatInput($_POST["homeLanguage"]);
			$culturalBackground = formatInput($_POST["culturalBackground"]);
			$skills = formatInput($_POST["skills"]);
			$spareTime = formatInput($_POST["spareTime"]);
			$futureGoals = formatInput($_POST["futureGoals"]);
			$whyLiv = formatInput($_POST["whyLIV"]);
			$pastLivExperience = formatInput($_POST["pastLIV"]);
			$whyApply = formatInput($_POST["whyApply"]);
			$expectations = formatInput($_POST["expectations"]);
			$areaOfFocusChild = formatInput($_POST["areaFocusChildDev"]);
			$areaOfFocusSchool = formatInput($_POST["areaFocusSchool"]);
			$areaOfFocusMusic = formatInput($_POST["areaOfFocusMusic"]);
			$areaOfFocusSport = formatInput($_POST["areaOfFocusSport"]);
			$areaOfFocusFactory = formatInput($_POST["areaOfFocusFactory"]);
			$areaOfFocusMarket = formatInput($_POST["areaFocusMarket"]);
			$areaOfFocusMedical = formatInput($_POST["areaOfFocusMedical"]);
			$areaOfFocusSocialServ = formatInput($_POST["areaOfFocusSocialServ"]);
			$areaOfFocusAgri = formatInput($_POST["areaOfFocusAgri"]);
			$areaOfFocusMain = formatInput($_POST["areaOfFocusMain"]);
			$strength = formatInput($_POST["strength"]);
			$weakness = formatInput($_POST["weakness"]);
			$past = formatInput($_POST["past"]);
			$support = formatInput($_POST["support"]);
			$flexiability = formatInput($_POST["flex"]);
			$team = formatInput($_POST["team"]);
			$speech = formatInput($_POST["speech"]);
			$tolerance = formatInput($_POST["tolerance"]);
			$smoking = formatInput($_POST["smoking"]);
			$innebriated = formatInput($_POST["innebriated"]);
			$cult = formatInput($_POST["cult"]);
			$depression = formatInput($_POST["depression"]);
			$illness = formatInput($_POST["illness"]);
			$crime = formatInput($_POST["crime"]);
			$refName1 = formatInput($_POST["refN1"]);
			$refAdress1 = formatInput($_POST["refAddress1"]);
			$refNumber1 = formatInput($_POST["refNum1"]);
			$refCellNum1 = formatInput($_POST["refCellNum1"]);
			$refEmail1 = formatInput($_POST["refEmail1"]);
			$refRelationship1 = formatInput($_POST["refRelation1"]);
			$refName2 = formatInput($_POST["refN2"]);
			$refAddress2 = formatInput($_POST["refAddress2"]);
			$refNum2 = formatInput($_POST["refNum2"]);
			$refCellNum2 = formatInput($_POST["refCellNum2"]);
			$refEmail2 = formatInput($_POST["refEmail2"]);
			$refRelationship2 = formatInput($_POST["refRelation2"]);
			$additionalInfo = formatInput($_POST["additionalInfo"]);
			$substanceAbuse = formatInput($_POST["substanceAbuse"]);

			$date = date('Y/m/d');
			$insertApplication = "insert into tblApplications values ('$username', '$date', 0, 0, 0, 'Gap-year Student');";
			$deleteApplicationSql = "delete from tblApplications where Username = '$username';";
			$deleteSql = "delete from tblGapYearStudents where Username = '$username'";
			$deleteReferenceSql = "delete from tblGapyearReferences where ApplicantUsername = '$username'";
			$gapYear = new GapYearStudent($username,$title,$address,$homeNumber,$businessNumber,$cellNumber,$birthDate,$age,$gender,$nationality,$passport,$passportIssueDate,$passportExpDate,$martialStatus,$emergencyName,$emergencyOtherNumber,$emergencyAddress,$emergencyRelationship,$churchName,$churchAddress,$pastor,$pastorEmail,$attendance, $dateJoined, $churchActivity,$mission,$educationLevel,$occupation,$homeLanguage,$culturalBackground,$skills,$spareTime,$futureGoals,$whyLiv,$pastLivExperience,$whyApply,$expectations,$areaOfFocusMain,$strength,$weakness,$past,$support,$flexiability,$team,$speech,$tolerance,$substanceAbuse,$smoking,$innebriated,$cult,$depression,$illness,$crime, $additionalInfo);
			$sql = "insert into tblGapYearStudents (Username,Title,Address,HomeTelephoneNumber,BusinessTelephoneNumber,CellNumber,DateOfBirth,Age,Gender,Nationality,PassportNumber,DateIssued,ValidUntil,MaritalStatus,EmergencyContactName,EmergencyContactTelephone,EmergencyContactAddress,EmergencyContactRelationship,CurrentLocalChurch,ChurchAddress,ChurchPastor,ChurchEmail,TimeSpentAtChurch,ReasonsForBecomingAChristian,ChurchInvolvement,MissionWork,HighestLevelOfEducationCompleted,CurrentOccupation,LanguagesSpoken,InterculturalExperiences,OtherSkillsOrWorkExperience,HobbiesAndInterests,FuturePlans,HowLivWasDiscovered,PreviousExperienceWithLiv,ReasonsForApplying,ExpectationsForProgram,AreasToFocusOn,GreatestStrengths,GreatestWeaknesses,LifeCircumstancesAndChallenges,FamilyOpinionOfLiv,Adaptability,TeamSkills,CommunicationSkills,RelationshipWithUnlikedPeople,SubstanceAbuseHistory,Smoker,InebriationFrequency,OccultActivities,PastPsychologicalTreatment,MedicalProblems,CriminalConvictions, AdditionalInformation)values('$gapYear->username', '$gapYear->title', '$gapYear->address', '$gapYear->homeTelephoneNumber', '$gapYear->businessTelephoneNumber', '$gapYear->cellNumber', '$gapYear->dateOfBirth','$gapYear->age', '$gapYear->gender', '$gapYear->nationality', '$gapYear->passportNumber', '$gapYear->dateIssued', '$gapYear->validUntil', '$gapYear->maritalStatus', '$gapYear->emergencyContactName', '$gapYear->emergencyContactTelephone', '$gapYear->emergencyContactAddress','$gapYear->relationship', '$gapYear->currentLocalChurch', '$gapYear->cAddress', '$gapYear->pastor', '$gapYear->cEmail', '$gapYear->timeSpentAtChurch', '$gapYear->reasonsForBecomingAChristian', '$gapYear->churchLifeInvolvement', '$gapYear->missionWork','$gapYear->highestLevelOfEducationCompleted', '$gapYear->currentOccupation', '$gapYear->languagesSpoken', '$gapYear->interculturalExperience', '$gapYear->otherSkillsOrWorkExperience', '$gapYear->hobbiesAndInterests', '$gapYear->futurePlans', '$gapYear->howLivWasDiscovered','$gapYear->previousExperienceWithLIV', '$gapYear->reasonsForApplying', '$gapYear->expectationsForProgram', '$gapYear->areasToFocusOn', '$gapYear->greatestStrengths', '$gapYear->greatestWeaknessess', '$gapYear->lifeCircumstancesAndChallenges','$gapYear->familyOpinionOfLIV', '$gapYear->adaptability', '$gapYear->teamSkills', '$gapYear->communicationSkills', '$gapYear->relationshipWithUnlikedPeople', '$gapYear->substanceAbuseHistory', '$gapYear->smoker', '$gapYear->inebriationFrequency','$gapYear->occultActivities', '$gapYear->pastPsychologicalTreatment', '$gapYear->medicallssues', '$gapYear->criminalConvictions', '$gapYear->additionalInformation');";
			$reference1 = "insert into tblGapyearReferences (FullName,Address,TelephoneNumber,CellNumber,EmailAddress,RelationshipToApplicant,ApplicantUsername)
					values ('$refName1','$refAdress1','$refNumber1','$refCellNum1','$refEmail1','$refRelationship1','$username');";
					
			$reference2 = "insert into tblGapyearReferences (FullName,Address,TelephoneNumber,CellNumber,EmailAddress,RelationshipToApplicant,ApplicantUsername)
				values ('$refName2','$refAddress2','$refNum2','$refCellNum2','$refEmail2','$refRelationship2','$username');";
			$dbConnect->executeQuery($deleteApplicationSql);
			$dbConnect = new dbConnect();
			$dbConnect->executeQuery($insertApplication);
			$dbConnect = new dbConnect();
			$dbConnect->executeQuery($deleteSql);
			$dbConnect = new dbConnect();
			$dbConnect->executeQuery($deleteReferenceSql);
			$dbConnect = new dbConnect();
			$res = $dbConnect->executeQuery($sql);
			$dbConnect = new dbConnect();
			$ref1 = $dbConnect->executeQuery($reference1);
			$dbConnect = new dbConnect();
			$ref2 = $dbConnect->executeQuery($reference2);
		}

		if($res->num_rows > 0 ){
				echo "Application details recorded";
		}
		if($ref1->num_rows > 0 && $ref2->num_rows > 0 ){
				echo "An error has occurred, please try again";
		}
?>

<!DOCTYPE html>
<html>
	<head>
	<Style>
	.text{
		width: 250px;
	}

	textarea{
		width: 150px;
	}
	#div1{
	  background-color: #f99f18;
	  cursor:pointer;
	  height:200px;
	  width: 33%;
	  display: inline-block
	 }
	 #txtAddress
	 {
	 text-align:left
	 }
	 #div2{
	  background-color: #0fb5fc;
	  cursor:pointer;
	  height:200px;
	  width: 33%;
	  display: inline-block
	 }
	 
	 #div3{
	  background-color: #8cdd3b;
	  cursor:pointer;
	  height:200px;
	  width: 33%;
	  display: inline-block
	 }
	 #lblGender
	 {
	 display:block
	} 
	 h1{
	  text-align: center;
	 }
			ul{
					text-align: center; 
			}
			
			
	 img{
	  margin: auto;
	 }
			
			form{
	   margin:auto;
	   display:block;
	   width:100%;
	   text-align: center;
	  }
	  h2
	  {
	  color: #FFFFFF;
	  font-size: large;
	  background-color: #000000;
	}
	  h1{
	   text-align:center;
	  }
	  
	  p{
	   text-align:center;
	  }
	  
	  button{
	   background: #f99f18;
	   border: 1px solid #f99f18;
	   color:#ffffff;
	   margin:auto;
	   display:block;
	   padding: 5px;
	   font-size:25px;
	   cursor:pointer

	  }
	  
	  h3{
	   text-align: center;  
	   }
	  
	  p{
	  display: block;
	  }
	  #frm1
	  {
	  table-layout:inherit
	}
	#logo
	{
	}
	</Style>
		<title>Gap Year Application Form</title>
	</head>
	<body>
		<br/><br/>
    	
    	<div>
    	&nbsp;&nbsp;&nbsp;
    	<br>
    	<form name = "gapYearApplication" id="gapYearApplication" method = "POST" enctype="multipart/form-data">
    	<h1>Gap Year Application Form</h1>
    	<!-- <img id="logo" alt="" src="applicationImage.png" height="115" width="115"> -->
    	<br>
    	<h2>Personal Details</h2>
    	</div>
    	<br>
		<p>
    	<label = "Label1" for="Title">Title</label>
        <Select id="title">
           	<option value="Mrs">Mrs</option>
          	<option value="Miss">Miss</option>
          	<option value="Mr">Mr</option>
        </Select>
             	<br>
             	
             	<br>
             	<label for="Surname">Surname</label>
             	<br>
             	<input maxlength="55" class="text" required="true"    type="text" id="txtGpSurname" value="">
             	<br>
             	
             	<br>
             	<label for="firstNames">First Names</label>
             	<br>
             	<input maxlength="55" class="text" required="true"    type="text" id="gpFNnames" value="">
             	<br>
             	
             	<br>
             	<label for="address">Address</label>
             	<br>
             	<textarea maxlength="150" required="true" required="true" id="gpTxtAddress" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="email">Email</label>
             	<br>
             	<input maxlength="254" class="text" required="true"    type="email" id="gpEmail" value="">
             	<br>
             	
             	<br>
             	<label for="telHome">Tel(Home)</label>
             	<br>
             	<input maxlength="20" class="text" required="true"    type="text" id="gpTelHome" value="">
             	<br>
             	
             	<br>
             	<label for="telBusiness">Tel(Business)</label>
             	<br>
             	<input maxlength="20" class="text" required="true"    type="text" id="gpTelBusiness" value="">
             	<br>
             	
             	<br>
             	<label for="Phone">Phone</label>
             	<br>
             	<input maxlength="20" class="text" required="true"    type="text" id="gpPhone" value="">
             	<br>
             	
             	<br>
             	<label for="dateofbirth">Date of Birth</label>
             	<br>
             	<input class="text" required="true"    type="date" id="gpDateOfBirth" value="">
             	<br>
             	
             	<br>
             	<label for="age">Age</label>
             	<br>
             	<input class="text" required="true"    type="number" id="gpAge" value="">
             	<br>
             	
             	<br>
             	<label for="gender">Gender</label>
             	<br>
             	<label for="male">Male</label>
             	<input required="true"    type="radio" name="gender" id="gpMale" value="Male">
             	<label for="female">Female</label>
             	<input required="true"    type="radio" name="gender" id="gpFemale" value="Female">
             	<br>
             	
             	<br>
             	<label for="nationality">Nationality</label>
             	<br>
             	<input maxlength="40" class="text" required="true"    type="text" id="gpNationality" value="">
             	<br>
             	
             	<br>
             	<label for="passport">Passport Number</label>
             	<br>
             	<input maxlength="20" class="text" required="true"    type="text" id="gpPassport" value="">
             	<br>
             	
             	<br>
             	<label for="dateissued">Date Issued</label>
             	<br>
             	<input class="text" required="true"    type="date" id="gpDateIssued" value=""> 
             	<br>
             	
             	<br>
             	<label for="expirationdate">Expiration Date</label>
             	<br>
             	<input class="text" required="true"    type="date" id="gpExpirationdate" value="">
             	<br>
             	
             	<br>
             	<b>Marital Status:</b>
             	<br>
             	
             	<label for="single">Single</label>
             	<input required="true"    type="radio" name="marital" id="gpSingle" value="Single">
             	
             	
             	<label for="married">Married</label>
             	<input required="true"    type="radio" name="marital" id="gpMarried" value="Married">
             	
     	
             	<label for="divorced">Divorced</label>
             	<input required="true"    type="radio" name="marital" id="gpDivorced" value="Divorced">
             	
     	
             	<label for="">Widowed</label>
             	<input required="true"    type="radio" name="marital" id="gpWidowed" value="Widowed">
             	
     	
             	<label for="remarried">Remarried</label>
             	<input required="true"    type="radio" name="marital" id="gpRemarried" value="Remarried">
             	
     	
             	<label for="engaged">Engaged</label>
             	<input required="true"    type="radio" name="marital" id="gpEngaged" value="Engaged">
             	
             	
             	<br>
             	<br>
             	
             	<h2>Emergency Contact Details</h2>
             	<p>
             	<br>
             	<label for="emergencyName">Name</label>
             	<br>
             	<input maxlength="150" class="text" required="true"    type="text" id="gpemergencyName" value="">
             	<br>
             	
             	<br>
             	<label for="emergencySurname">Surname</label>
             	<br>
             	<input class="text" required="true"    type="text" id="gpemergencySurname" value="">
             	<br>
             	
             	<br>
             	<label for="emergencyOtherNum">Telephone number</label>
             	<br>
             	<input maxlength="20" class="text" required="true"    type="text" id="gpemergencyOtherNum" value="">
             	<br>
             	
             	<br>
             	<label for="emergencyAddress">Address</label>
             	<br>
             	<textarea maxlength="150" required="true" required="true" id="gpemergencyAddress" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<label>Relationship to applicant:</label><br/>
             	<input maxlength="30" class="text" required="true" type="text" name="relationship" id="emergencyContactRelationship" value="">
     	
             	<br>
             	</p>
             	<h2>Religion Background Information</h2>
             	<p>
             	<br>
             	<label for="religionPlace">Name of church</label>
             	<br>
             	<input maxlength="50" class="text" required="true"    type="text" id="gpreligionPlace" value="">
             	<br>
             	
             	<br>
             	<label for="religionAddress">Address</label>
             	<br>
             	<input maxlength="150" class="text" required="true"    type="text" id="gpreligionAddress" value="">
             	<br>
             	
             	<br>
             	<label for="religionPastor">Name of pastor</label>
             	<br>
             	<input maxlength="50" class="text" required="true"    type="text" id="gpreligionLeader" value="">
             	<br>
             	
             	<br>
             	<label for="religionEmail">Email</label>
             	<br>
             	<input maxlength="254" class="text" required="true"    type="email" id="gpreligionEmail" value="">
             	<br>
             	
             	<br>
             	<label for="religionAttendance">How long have you attended this church?</label>
             	<br>
             	<input maxlength="20" class="text" required="true"    type="text" id="gpreligionAttendance" value="">
             	<br>
             	
             	<br>
             	<label for="religionChristian">Briefly describe how and when you became a Christian?</label>
             	<br>
             	<textarea maxlength="150" required="true" required="true" id="gpReligionChristian" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="religionActivity">If applicable, please describe how you have been involved in church</label>
             	<br>
             	<textarea maxlength="100" required="true" required="true" id="gpreligionActivity" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="religionMission">Have you been involved in any outreach programs or mission work? If so, please describe it briefly.</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpreligionMission" style="height: 106px; width: 265px"></textarea>
             	<br>
             	</p>
             	<h2>Education/Work Experience</h2>
             	<p>
             	<br>
             	<label for="eduLevel">Highest level of education completed</label>
             	<br>
             	<input maxlength="50" class="text" required="true"    type="text" id="gpEduLevel" value="">
             	<br>
             	
             	<br>
             	<label for="eduOccupation">Current Occupation</label>
             	<br>
             	<input maxlength="30" class="text" required="true"    type="text" id="gpEduOccupation" value="">
             	<br>
             	
             	<br>
             	<label for="eduHomeLanguage">What languages do you speak?</label>
             	<br>
             	<input maxlength="100" class="text" required="true"    type="text" id="gpEduHomeLanguage" value="">
             	<br>
             	
             	<br>
             	<label for="eduInterRacialWork">Have you ever worked with other cultures or religions or races? If so please describe briefly.</label>
             	<br>
             	<textarea maxlength="200" required="true" id="gpeduInterRacialWork" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="eduSkills">Do you have any other skills, qualifications or work experience over the last 5 years?</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpEduSkills" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="eduFreeTime">What do you do in your spare time?</label>
             	<br>
             	<textarea maxlength="50" required="true" id="gpEduFreeTime" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="eduFutureGoals">Do you have any sense of what you want to do with the rest of your life, if so what?</label>
             	<br>
             	<textarea maxlength="150" required="true" id="gpEduFutureGoals" style="height: 106px; width: 265px"></textarea>
             	<br>
             	</p>
             	<h2>General Information</h2>
             	<p>
             	<br>
             	<label for="generalInfoWhyLiv">How did you hear about LIV 4 Change?</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpGeneralInfoWhyLiv" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="generalInfoPastWork">Have you been involved with LIV before? If so please describe briefly.</label>
             	<br>
             	<textarea maxlength="150" required="true" id="gpGeneralInfoPastWork" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="generalInfoWhyApply">Why do you want to apply for the LIV 4 Change program?</label>
             	<br>
             	<textarea maxlength="150" required="true" id="gpGeneralInfoWhyApply" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="generalInfoExpectations">What are your expectations while working in the LIV 4 Change program?</label>
             	<br>
             	<textarea maxlength="50" required="true" id="gpGeneralInfoExpectations" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	
             	
             	<br>
             	<label = "Label1" for="areaOfFocus">Is there any particular area of work in LIV that you are passionate about
             	or want to focus on? Please list your top 3.</label>
             	<br>
             	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="areaOfFocusChildDevCentre" value = "Early childhood development centre" id = "gpAreaOfFocusChildDevCentre">
             	<label for="childDev">Early childhood development centre</label>
             	<br>
             	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="primSecSchool" value = "Primary or Secondary School" id = "gpPrimSecSchool">
             	<label for="school">Primary or Secondary School</label>
             	<br>
             	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="music" value = "Music" id = "gpMusic">
             	<label for="music">Music</label>
             	<br>
             	
             	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="sport" value = "Sport" id = "gpSport">
             	<label for="sport">Sport</label>
             	<br>
             	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="factoryAndRetail" value = "Factory and Retail" id = "gpfactoryAndRetail">
             	<label for="factoryAndRetail">Factory and Retail</label>
             	<br>
             	
             	<br>
             	<input  class="areaToFocusOn" type="checkbox" name="marketing" value = "Marketing" id = "gpMarketing">
             	<label for="marketing">Marketing</label>
             	<br>
             	
             	<br>
             	<input  class="areaToFocusOn" type="checkbox" name="medical" value = "Medical" id = "gpMedical">
             	<label for="medical">Medical</label>
             	<br>
              	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="socialServ" value = "Social services" id = "gpSocialServ">
             	<label for="socialServ">Social services</label>
             	<br>
             	
             	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="agricultural" value = "Agricultural" id = "gpAgricultural">
             	<label for="agricultural">Agricultural</label>
             	<br>
             	
             	<br>
             	<input class="areaToFocusOn" type="checkbox" name="maintenance" value = "Maintenance" id = "gpMaintenance">
             	<label for="maintenance">Maintenance</label>
             	<br>
             	
             	<br>
				</p>
             	<h2>Additional Information</h2>
				<p>
             	<br>
             	
             	<br>
             	<label for="addGreatStrength">What are your two greatest strengths?</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpAddGreatStrength" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="addGreatWeaknesses">What are your two greatest weaknesses?</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpAddGreatWeaknesses" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="addFamilyBackground">Describe your family background and the circumstances of your childhood and/or years.
             	Please mention any adverse issues that have affected your life i.e family dysfunction, martial instability, child abuse,
             	drug abuse, alcoholism etc.
             	</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpFamilyBackground" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	
             	
             	<br>
             	<label for="addFamilySupport">Does your family support you applying for the LIV 4 Change program?</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpFamilySupport" style="height: 106px; width: 265px"></textarea>
             	<br>
             	</p>
             	<h2>Please describe these following qualities in yourself</h2>
             	<p>
             	<br>
             	<label for="qualFlexiability">Your adaptability to change/flexibility.</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpQualFlexiability" style="height: 106px; width: 265px"></textarea>
             	<br>
				
				<br>
             	<label for="qualTeam">Your ability to work in a team.</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpQualTeam" style="height: 106px; width: 265px"></textarea>
             	<br>
             	
             	<br>
             	<label for="qualFreeSpeach">Your ability to say what is on your mind.</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpQualFreeSpeach" style="height: 106px; width: 265px"></textarea>
             	<br>
             	<br>
             	
             	<br>
             	<label for="qualEndurance">Your ability to work with people you do not like.</label>
             	<br>
             	<textarea maxlength="100" required="true" id="gpQualEndurance" style="height: 106px; width: 265px"></textarea>
             	<br>
             	</p>
             	<h2>Background Information</h2>
             	<p>
             	<br>
             	<p>We are working with children and some are at very sensitive stages of their lives.
             	Many of them come from broken homes and problematic lifestyles. Some have been involved in illegal
             	and harmful activities. But through the help of our programmes they have been able to overcome these problems.
             	It is very important that the staff and volunteers of LIV help to provide a good atmosphere that will
             	contribute to the wholesome development of the children. If you have difficulty communicate your answers in writing
             	, we can talk with you personally. Your answers to these questions will not, in themselves, prejudice your acceptance.
            	All questions answered here are confidential.
            	</p>
				<p>
            	<br>
            	<label>Have you used any illegal drugs or been an alcoholic? If so, please explain how recently and in what quantities:</label>
				<br>
				<input maxlength="100" class="text" required="true" type="text" id="substanceAbuse"/>
				<br>
            	<br>
            	<label = "Label1" for="Smoke">Do you currently smoke?</label>
            	<Select id="smoking">
					<option value="No">No</option>
                    <option value="Yes">Yes</option>
            	</Select>
            	<br>
            	
            	<br>
            	<label = "Label1" for="drunkFreq">How often do you get inebriated?</label>
            	<Select id="innebriated">
				    <option value="Not at all">Not at all</option>
					<option value="Regularly">Regularly</option>
                    <option value="Occassionally">Occassionally</option>
            	</Select>
            	<br>
            	
            	<br>
            	<label for="backOccult">Have you ever been involved in occult or homosexual activities/tendencies and/or practices? If so please describe briefly.</label>
            	<br>
            	<textarea maxlength="100" required="true" id="gpBackCult" style="height: 106px; width: 265px"></textarea>
            	<br>
            	
            	<br>
            	<label for="backDepression">Have you ever suffered from depression or have had any psychiatric/psychological treamtment?
            	If so please describe briefly the treatment and when the treatment occurred.
            	Do you have any current difficulties in this regard?
            	</label>
            	<br>
            	<textarea maxlength="100" required="true" id="gpBackDepression" style="height: 106px; width: 265px"></textarea>
            	<br>
            	
            	<br>
            	<label for="backMedical">Do you have any other medical problems and/or illnesses that are current or are in the past?
            	If so, are you currently on any medication? Please describe briefly.
            	</label>
            	<br>
            	<textarea maxlength="50" required="true" id="gpBackMedical" style="height: 106px; width: 265px"></textarea>
            	<br>
            	
            	<br>
            	<label for="backCrime">Have you ever been convicted of a crime?
            	If so, please describe briefly the nature of the crime, the date, the place of conviction and the legal disposition of the case.
            	</label>
            	<br>
            	<textarea maxlength="100" required="true" id="gpBackCrime" style="height: 106px; width: 265px"></textarea>
            	<br>
            	
            	<h2>Please Note</h2>
            	<br>
            	<label>The organization will not deny involvement to any applicant solely because a person has been convicted of a crime. The organization, however,
            	may consider the nature, date and circumstances of the offense as well as whether the offence is relevant to the duties of the position applied for.
            	If you have been convicted of any child abuse offence/offences or have been suspected of child abuse, please provide reliable character witnesses on your behalf.
            	</label>
            	<br>
				</p>
            	</p>
            	<h2>References</h2>
				<p>
            	<br>
            	<label>Please provide the details of two references who have known you for the minimum period of two years,
             	they must not be related to you. We ask that one of the references given, must be a chrisitian leader i.e your pastor and/or
             	youth leader, and the second reference to be a more personal reference, i.e employer, colleague or close friend.
            	</label>
            	<br>
            	</p>
            	<h2>First Reference Detail</h2>
            	<p>
            	<br>
            	<label for="refName1">Name</label>
            	<br>
            	<input maxlength="100" class="text" required="true"    type="text" id="gpRefName1" value="">
            	<br>
            	
            	<br>
            	<label for="refAddress1">Address</label>
            	<br>
            	<textarea maxlength="150" required="true" id="gpRefAddress1" style="height: 106px; width: 265px"></textarea>
            	<br>
            	
            	<br>
            	<label for="refTel1">Home number</label>
            	<br>
            	<input maxlength="20" class="text" required="true"    type="text" id="gpRefTel1" value="">
            	<br>
            	
            	<br>
            	<label for="refCell1">Cell number</label>
            	<br>
            	<input maxlength="20" class="text" required="true"    type="text" id="gpRefCell1" value="">
            	<br>
            	
            	<br>
            	<label for="refEmail1">Email</label>
            	<br>
            	<input maxlength="254" class="text" required="true"    type="email" id="gpRefEmail1" value="">
            	<br>
            	
            	<br>
            	<label for="refRelationship1">Relationship</label>
            	<br>
            	<input maxlength="30" class="text" required="true"    type="text" id="gpRefRelationship1" value="">
            	<br>
            	</p>
            	<h2>Second Reference Detail</h2>
            	<p>
            	<br>
            	<label for="refName2">Name</label>
            	<br>
            	<input maxlength="100" class="text" required="true"    type="text" id="gpRefName2" value="">
            	<br>
            	
            	<br>
            	<label for="refAddress2">Address</label>
            	<br>
            	<textarea maxlength="150" required="true" id="gpRefAddress2" style="height: 106px; width: 265px"></textarea>
            	<br>
            	
            	<br>
            	<label for="refTel2">Home number</label>
            	<br>
            	<input maxlength="20"class="text" required="true"    type="text" id="gpRefTel2" value="">
            	<br>
            	
            	<br>
            	<label for="refCell2">Cell number</label>
            	<br>
            	<input maxlength="20" class="text" required="true"    type="text" id="gpRefCell2" value="">
            	<br>
            	
            	<br>
            	<label for="refEmail2">Email</label>
            	<br>
            	<input maxlength="254" class="text" required="true"    type="email" id="gpRefEmail2" value="">
            	<br>
            	
            	<br>
            	<label for="refRelationship2">Relationship</label>
            	<br>
            	<input maxlength="30" class="text" required="true"    type="text" id="gpRefRelationship2" value="">
            	<br>
            	
            	<br>
            	<label for="otherInfo">Is there any other additional information you think would be helpful when considering your application?</label>
            	<br>
            	<textarea required="true" id="gpOtherInfo" style="height: 106px; width: 265px"></textarea>
            	<br>
            	</p>
            	<h2>Application form waiver/declaration</h2>
            	<label>By submitting your application, you authorise investigation of all statements contained in this application, acknowledge that the misrepresentation or omission of facts
            	called for is cause for dismissal at any time without previous notice, and give the organisation permission to contact schools, previous employers (unless otherwise indicated),
            	references and others, and hereby release the organisation from any liability as a result of such contact. <br/><br/>By submitting your application, you also agree to LIV's code of conduct, which is available to read <a href="codeOfConduct.pdf" target="_blank">here</a>, as well as LIV's LIV 4 Change policy, which is available to read <a href="LIV4ChangePolicy.docx" target="_blank">here</a>
            	</label>
            	
            
            	
            	<h2>Essential documents to be uploaded when completing this application</h2>
            	<br>
            	
            	<label for="checkList">Please ensure that all these documents have been uploaded prior to submitting this document</label>
            	<br>
            	
            	<p>Certified copy of your ID.
                    <input required="true"    type="file" id="attachment" name="attachment">
                </p>
            	<p>Police Clearance.
                    <input required="true"    type="file" id="attachment1" name="attachment1">
				</p>
           	
           	<br>
           	
           	<br>
           	<b>Acceptance on this program is not gauranteed. If we believe you are a strong candidate for LIV 4 Change, we will arrange a skype interview.
           	</b>
           	<br>
           	<b>Payment: The initial first month payment must be made within 5 days of recieving your written acceptance from Liv.
              	Please note that cost does not include travel expenses to Durban, South Africa.
           	</b>
           	<p id = "help"></p>
			</p>
           	<button id="btnSubmit" onclick="checkForCompleteness()">Submit</button>
           	</form>
</body>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
<script>
	window.onload = setListener;

	//Prevents the user from selecting more than 3 check boxes (Learnt from https://stackoverflow.com/questions/19001844/how-to-limit-the-number-of-selected-checkboxes)
	function setListener(){
		var limit = 3;
		$('input.areaToFocusOn').on('change', function(evt) {
			if($(this).siblings('input.areaToFocusOn:checked').length >= limit) {
				this.checked = false;
			}
		});
	}

	//Function ensures that all required information has been added
	function checkForCompleteness(){
		var form = document.getElementById("gapYearApplication");
		if($(form)[0].checkValidity()) {
			//Adds input to database if all required information has been inputted
			gapYear();
		}
	}

	//Function adds all selected areas of focus to a variable
	//https://stackoverflow.com/questions/19766044/best-way-to-get-all-selected-checkboxes-values-in-jquery
	function getAreasOfFocus(){
		var areasToFocusOn = "";
		var checkedValues = $('input.areaToFocusOn:checked').map(function() {
			return $(this).val();
		}).get();
		for(var i = 0; i < checkedValues.length; i++){
			areasToFocusOn += checkedValues[i]
			if(i < checkedValues.length - 1){
				areasToFocusOn += ", ";
			}
		}
		
		return areasToFocusOn;
	}

	//Function stores the user's input in variables and sends the values to PHP to insert into the database
	function gapYear(){
    	//Applicants general information
    	var title = $(document.getElementById("title")).val();
    	var surname = $(document.getElementById("txtGpSurname")).val();
    	var firstName = $(document.getElementById("gpFNnames")).val();
    	var address = $(document.getElementById("gpTxtAddress")).val();
    	var homeNum = $(document.getElementById("gpTelHome")).val();
    	var busNum = $(document.getElementById("gpTelBusiness")).val();
    	var cellNum = $(document.getElementById("gpPhone")).val();
    	var email = $(document.getElementById("gpEmail")).val();
    	var birth = $(document.getElementById("gpDateOfBirth")).val();
    	var age = $(document.getElementById("gpAge")).val();
 
    	if(document.getElementById("gpMale").checked){
            var gender = $(document.getElementById("gpMale")).val();
    	}
		else{
            var gender = $(document.getElementById("gpFemale")).val();
    	}
    	
    	var nationality = $(document.getElementById("gpNationality")).val();
    	var passport = $(document.getElementById("gpPassport")).val();
    	
    	//check thate ID date issued is greater than current year
    	var dateIssued = $(document.getElementById("gpDateIssued")).val();
    	var expDate = $(document.getElementById("gpExpirationdate")).val();
    	
    	if(document.getElementById("gpSingle").checked){
            	var martialStatus = $(document.getElementById("gpSingle")).val();
    	}
		else if(document.getElementById("gpMarried").checked){
            var martialStatus = $(document.getElementById("gpMarried")).val();
        }
		else if(document.getElementById("gpDivorced").checked){
            var martialStatus = $(document.getElementById("gpDivorced")).val();  
        }
		else if(document.getElementById("gpWidowed").checked){
            var martialStatus = $(document.getElementById("gpWidowed")).val();  
        }
		else if(document.getElementById("gpRemarried").checked){
            var martialStatus = $(document.getElementById("gpRemarried")).val(); 
        }
		else if(document.getElementById("gpEngaged").checked){
            var martialStatus = $(document.getElementById("gpEngaged")).val();
        }
    	
    	var emergencyName = $(document.getElementById("gpemergencyName")).val();
    	var emergencySurname = $(document.getElementById("gpemergencySurname")).val();
    	var emergencyCell = $(document.getElementById("gpemergencyCellNum")).val();
    	var emergencyOtherNum = $(document.getElementById("gpemergencyOtherNum")).val();
    	var emergencyAddress = $(document.getElementById("gpemergencyAddress")).val();
    	var emergencyRelation = $(document.getElementById("emergencyContactRelationship")).val();
    	var churchNme = $(document.getElementById("gpreligionPlace")).val();
    	var churchAddress = $(document.getElementById("gpreligionAddress")).val();
    	var pastor = $(document.getElementById("gpreligionLeader")).val();
    	var pastorEmail = $(document.getElementById("gpreligionEmail")).val();
    	var churchAttend = $(document.getElementById("gpreligionAttendance")).val();
    	var churchJoinDte = $(document.getElementById("gpReligionChristian")).val();
    	var churchDesc = $(document.getElementById("gpreligionActivity")).val();
    	var churchMission = $(document.getElementById("gpreligionMission")).val();
    	var eduLevel = $(document.getElementById("gpEduLevel")).val();
    	var occupation = $(document.getElementById("gpEduOccupation")).val();
    	var homeLang = $(document.getElementById("gpEduHomeLanguage")).val();
    	var culturalBack = $(document.getElementById("gpeduInterRacialWork")).val();
    	var skills = $(document.getElementById("gpEduSkills")).val();
    	var spareTme = $(document.getElementById("gpEduFreeTime")).val();
    	var goals = $(document.getElementById("gpEduFutureGoals")).val();
    	var whyLiv = $(document.getElementById("gpGeneralInfoWhyLiv")).val();
    	var pastLiv = $(document.getElementById("gpGeneralInfoPastWork")).val();
    	var whyApply = $(document.getElementById("gpGeneralInfoWhyApply")).val();
    	var expectations = $(document.getElementById("gpGeneralInfoExpectations")).val();                  	
    	var childDev = $(document.getElementById("areaOfFocusChildDevCentre")).val();                 	
    	var school = $(document.getElementById("primSecSchool")).val();                        	
    	var music = $(document.getElementById("music")).val();                                 	
    	var sport = $(document.getElementById("sport")).val();
    	var factoryRetails = $(document.getElementById("factoryAndRetail")).val();
    	var marketing = $(document.getElementById("marketing")).val();
    	var medical = $(document.getElementById("medical")).val();
    	var socialServ = $(document.getElementById("socialServ")).val();
    	var agricultural = $(document.getElementById("agricultural")).val();
    	var maintainance = getAreasOfFocus();
    	var strength = $(document.getElementById("gpAddGreatStrength")).val();
    	var weakness = $(document.getElementById("gpAddGreatWeaknesses")).val();
    	var past = $(document.getElementById("gpFamilyBackground")).val();
    	var support = $(document.getElementById("gpFamilySupport")).val();
    	var flexiability = $(document.getElementById("gpQualFlexiability")).val();
    	var teamWork = $(document.getElementById("gpQualTeam")).val();
    	var speech = $(document.getElementById("gpQualFreeSpeach")).val();
    	var tolerance = $(document.getElementById("gpQualEndurance")).val();
		var substanceAbuse = $(document.getElementById("substanceAbuse")).val();
    	var smoke = $(document.getElementById("smoking")).val();
    	var drunkFreq = $(document.getElementById("innebriated")).val();
    	var cult = $(document.getElementById("gpBackCult")).val();
    	var depression = $(document.getElementById("gpBackDepression")).val();
    	var illness = $(document.getElementById("gpBackMedical")).val();
    	var crime = $(document.getElementById("gpBackCrime")).val();
    	var refName1 = $(document.getElementById("gpRefName1")).val();
    	var refAddress1 = $(document.getElementById("gpRefAddress1")).val();
    	var refNum1 = $(document.getElementById("gpRefTel1")).val();
    	var refCellNum1 = $(document.getElementById("gpRefCell1")).val();
    	var refEmail1 = $(document.getElementById("gpRefEmail1")).val();
    	var refRel1 = $(document.getElementById("gpRefRelationship1")).val();
    	var refName2 = $(document.getElementById("gpRefName2")).val();
    	var refAddress2 = $(document.getElementById("gpRefAddress2")).val();
    	var refNum2 = $(document.getElementById("gpRefTel2")).val();
    	var refCellNum2 = $(document.getElementById("gpRefCell2")).val();
    	var refEmail2 = $(document.getElementById("gpRefEmail2")).val();
    	var refRel2 = $(document.getElementById("gpRefRelationship2")).val();
    	var otherInfo = $(document.getElementById("gpOtherInfo")).val();
    	
		if(surname||firstName||address||homeNum||busNum||cellNum||email||birth||age||nationality||passport||dateIssued||expDate||martialStatus||emergencyName||emergencySurname||emergencyOtherNum||emergencyAddress||emergencyRelation||churchNme||churchAddress||pastor||pastorEmail||churchAttend||churchJoinDte||churchDesc||churchMission
    	||eduLevel||occupation||homeLang||culturalBack||skills||spareTme||goals||whyLiv||pastLiv||whyApply||expectations||childDev||school||music||sport||factoryRetails||marketing||medical||socialServ||agricultural||maintainance||
    	strength||weakness||past||support||flexiability||teamWork||speech||tolerance||cult||depression||illness||crime||refName1||refAddress1||refNum1||refCellNum1||refEmail1||refRel1||refName2||
    	refAddress2||refNum2||refCellNum2||refEmail2||refRel2||otherInfo){
         	var file1 = $('#attachment').prop('files')[0];
         	var file2 = $('#attachment1').prop('files')[0];
         	$.ajax({
                url: window.location.pathname,
                type: "post",
                data:{"submitData":"dummy data","Title" : title, "gpSurnme": surname, "FName":firstName , "address":address,"homeNumber":homeNum,"businessNum":busNum , "CellphoneNum":cellNum, "Email":email, "birthDate":birth, "age":age, "gender":gender, "nationality":nationality, "passport":passport, "passportIssue":dateIssued, "passportExp":expDate, "martial":martialStatus, "emergencyNme":emergencyName, "emergencySurnme":emergencySurname, "emergencyOtherNumber":emergencyOtherNum, "emergencyAddress":emergencyAddress, "emergencyRelationship":emergencyRelation,"churchName":churchNme, "churchAddress":churchAddress, "pastor":pastor, "pastorEmail":pastorEmail, "attendance":churchAttend,"joined":churchJoinDte, "churchActivity":churchDesc, "Mission":churchMission, "eduLevel":eduLevel, "occupation":occupation ,"homeLanguage":homeLang, "culturalBackground":culturalBack, "skills":skills, "spareTime":spareTme, "futureGoals":goals, "whyLIV":whyLiv, "pastLIV":pastLiv, "whyApply":whyApply, "expectations":expectations, "areaFocusChildDev":childDev, "areaFocusSchool":school, "areaOfFocusMusic":music, "areaOfFocusSport":sport, "areaOfFocusFactory":factoryRetails, "areaFocusMarket":marketing, "areaOfFocusMedical":medical, "areaOfFocusSocialServ":socialServ, "areaOfFocusAgri":agricultural, "areaOfFocusMain":maintainance, "strength":strength, "weakness":weakness, "past":past, "support":support, "flex":flexiability, "team":teamWork,"speech":speech, "tolerance":tolerance, "substanceAbuse" : substanceAbuse, "smoking":smoke, "innebriated":drunkFreq, "cult":cult, "depression":depression, "illness":illness, "crime":crime, "refN1":refName1, "refAddress1":refAddress1, "refNum1":refNum1, "refCellNum1":refCellNum1, "refEmail1":refEmail1,"refRelation1":refRel1, "refN2":refName2, "refAddress2":refAddress2, "refNum2":refNum2, "refCellNum2":refCellNum2, "refEmail2":refEmail2, "refRelation2":refRel2, "additionalInfo":otherInfo},
                success: function(response){
                }
           	});	
        }
		else{
           	alert("Please ensure all fields are completed");
        }
		var message = "Title- " + title + "<br/>FirstName- " + firstName + "<br/>Surname- " + surname + "<br/>Address- " + address + "<br/>Home number- " + homeNum+ "<br/>Business number- "+busNum + "<br/>Cellphone number- "+cellNum+ "<br/>Email- "+email+ "<br/>Birth date- "+birth+ "<br/>Age- "+age+ "<br/>Gender- "+gender+ "<br/>Nationality- "+nationality+ "<br/>Passport number"+passport+ "<br/>Date passport issued- "+dateIssued+ "<br/>Password expiry date- "+expDate+ "<br/>Marital status- "+martialStatus+ "<br/>Emergency contact name- "+emergencyName+ "<br/>Emergency contact surname- "+emergencySurname+ "<br/>Emergency contact number- "+emergencyOtherNum+ "<br/>Emergency contact address- "+emergencyAddress+ "<br/>Emergency contact relationship- "+emergencyRelation+"<br/>Church name- "+churchNme+ "<br/>Church address- "+churchAddress+ "<br/>Pastor name- "+pastor+ "<br/>Pastor email- "+pastorEmail+ "<br/>Time spent attending church- "+churchAttend+"<br/>Involvement with church- "+churchDesc+ "<br/>Mission work- "+churchMission+ "<br/>Education level- "+eduLevel+ "<br/>Occupation- "+occupation +"<br/>Home language- "+homeLang+ "<br/>Cultural background- "+culturalBack+ "<br/>Other skills- "+skills+ "<br/>Things done in spare time- "+spareTme+ "<br/>Goals for the future- "+goals+ "<br/>How LIV was found out about- "+whyLiv+ "<br/>Past experience with LIV- "+pastLiv+ "<br/>Reason for applying- "+whyApply+ "<br/>Expectations for program- "+expectations+ "<br/>Main areas of focus- "+maintainance+ "<br/>Biggest strengths- "+strength+ "<br/>Biggest weaknesses- "+weakness+ "<br/>Family background- "+past+ "<br/>Family support for application for LIV- "+support+ "<br/>Flexibility to change- "+flexiability+ "<br/>Ability to work in a team- "+teamWork+"<br/>Ability to speak freely- "+speech+ "<br/>Ability to work with people they don't like- "+tolerance+ "<br/>Substance abuse history- " + substanceAbuse + "<br/>Smoker- "+smoke+ "<br/>Inebriation frequency- "+drunkFreq+ "<br/>Involvement with occult or homosexual activities- "+cult+ "<br/>Past psychological problems- "+depression+ "<br/>Medical problems- "+illness+ "<br/>Criminal history- "+crime+ "<br/>Reference 1 name- "+refName1+ "<br/>Reference 1 address- "+refAddress1+ "<br/>Reference 1 home number- "+refNum1+ "<br/>Reference 1 cell number- "+refCellNum1+ "<br/>Reference 1 email address- "+refEmail1+"<br/>Reference 1 relationship- "+refRel1+ "<br/>Reference 2 name- "+refName2+ "<br/>Reference 2 address- "+refAddress2+ "<br/>Reference 2 hone number- "+refNum2+ "<br/>Reference 2 cell number- "+refCellNum2+ "<br/>Reference 2 email address- "+refEmail2+ "<br/>Reference 2 relationship- "+refRel2+ "<br/>Additional information- "+otherInfo+ "<br/><br/>Kind regards, <br/>LIV Portal";
		uploadImage(message);
    }
		
	//Function uploads the applicant's files to uploadFile.php, which will then send an email to the LIV employees with the applicant's details
	function uploadImage(message){
        //Uploads the files to uploadFile.php (https://stackoverflow.com/questions/23980733/jquery-ajax-file-upload-php)
        var copyOfId = $('#attachment').get(0).files[0];           
        var policClearance = $('#attachment1').get(0).files[0];
        var formData = new FormData();                  
        formData.append('ID', copyOfId);
        formData.append('Clearance', policClearance);
		formData.append('EmailContent', message);               
        $.ajax({
            url: 'uploadFile.php', 
            dataType: 'text',
			data: formData,
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(response){
				alert("Your application has been submitted successfully, please wait for a LIV employee to contact you");
            },
			error: function(response){
				alert("Your application has been submitted successfully, please wait for a LIV employee to contact you");
			}
        });
    }
</script>
</html>
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>

