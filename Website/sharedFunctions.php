<?php
        //Function ensures that the email entered is valid. It returns true if the email is valid, and false if the email is invalid
        function checkEmail($email){
            $validEmail = false;
            if(filter_var($email, FILTER_VALIDATE_EMAIL) == true){
                $validEmail = true;            
            }
            return $validEmail;
        }
                        
        //Function ensures that a password is 8-12 characters long, and includes at least 1 upper case character and 1 digit. Returns true if the password is valid and false if the password is invalid        
        function checkPassword($password){
            $validPassword = false;
            if(preg_match('/^(?=.*\d)(?=.*[A-Z])[0-9A-Za-z!@#$%]{8,12}$/', $password) == 1){
                $validPassword = true;
            }
            return $validPassword;
        }
        
        //Function is used to take the user from one page to another 
        function redirectPage($page){
            echo '<script type="text/javascript">';
            echo 'window.location.href="'.$page.'";';
            echo '</script>';
        }
        
        //Function ensures that input is safe to use in the program
        function formatInput($input){
            $input = trim($input);
            $input = stripslashes($input);
            $input = htmlspecialchars($input, ENT_QUOTES));
            return $input;
        }
?>