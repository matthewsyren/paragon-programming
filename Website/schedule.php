<?php
	class Schedule{
		//Attributes
		public $username;
		public $date;
		public $time;
		public $description;
			
		//Constructor
		function __construct($username, $date, $time, $description){
			$this->username = $username;
			$this->date = $date;
			$this->time= $time;
			$this->description = $description;
		}
	}
?>