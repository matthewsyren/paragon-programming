<?php
    session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		if(!isset($_POST["ajaxResponse"])){
?>

<html>
	<body>
		<?php
            include "navbar.php";             
            include "styles.css";
			include "loaderStyle.php";
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
		include "dbConn.php";
		include "sharedFunctions.php";

		$username = $_SESSION["Username"];
		$accommodationType = $_SESSION["accommodationType"];
				
		//Function fetches the price of the selected accommodation 
		function getAccommodationPrice($accommodationID){
			$_SESSION['accommodationID'] = $accommodationID;
			$sql = "select CostPerNight from tblAccommodation where AccommodationID = '$accommodationID'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				$row = $result->fetch_assoc();
				return $row["CostPerNight"];
			}
		}
				
		//Function makes a provisional accommodation booking for the user
		function makeBooking($checkInDate, $checkOutDate, $accommodationID, $numberOfPeople){
			$available = checkAccommodationAvailability($checkInDate, $checkOutDate, $accommodationID, $numberOfPeople);
			if($available){
				$username = $_SESSION["Username"];
				$currentDate = date("Y-m-d");
				$dbConnect = new dbConnect();
				$costPerNight = getAccommodationPrice($accommodationID);
				$dateDifference = strtotime($checkOutDate) - strtotime($checkInDate);
								
				//Calculates number of nights the user is staying for (https://stackoverflow.com/questions/2040560/finding-the-number-of-days-between-two-dates)
				$numberOfNights = floor($dateDifference / (60 * 60 * 24));
				$totalCost = $numberOfNights * $costPerNight;
				$sql = "insert into tblAccommodationBookings (CheckInDate, CheckOutDate, Username, AccommodationID, DateBooked, PaymentMade, AmountDue, NumberOfPeople) values ('$checkInDate', '$checkOutDate', '$username', '$accommodationID', '$currentDate', 0, $totalCost, $numberOfPeople);";
				$result = $dbConnect->executeQuery($sql);
							
				if($result == 1){
					echo $result;
					$sql = "update tblApplications set ApplicationExpired = 1 where Username = '$username' and ApplicationExpired = 0";
					$result = $dbConnect->executeQuery($sql);
					if($result == 1){
						$sql = "select max(BookingID) from tblAccommodationBookings where Username = '$username'";
						$dbConnect = new dbConnect();
						$result = $dbConnect->executeQuery($sql);
						if($result->num_rows > 0){
							$row = $result->fetch_assoc();
							$bookingID = $row["max(BookingID)"];
							sendMailToEmployees($checkInDate, $checkOutDate, $accommodationID, $totalCost, $bookingID, $numberOfPeople);
						}
					}
				}
			}
		}
				
		//Function sends an email to the appropriate LIV employees telling them about the booking
		function sendMailToEmployees($checkInDate, $checkOutDate, $accommodationID, $totalCost, $bookingID, $numberOfPeople){
			$username = $_SESSION["Username"];
			$sql = "select FirstName, LastName, EmailAddress from tblUsers where Username = '$username'";
		
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				$row = $result->fetch_assoc();
				$applicantName = $row["FirstName"] . " " . $row["LastName"];
				$applicantEmailAddress = $row["EmailAddress"];
						
				$sql = "select FirstName, EmailAddress from tblLivEmployees where JobTitle = 'Active'";
				$dbConnect = new dbConnect();
				$result = $dbConnect->executeQuery($sql);
				if($result->num_rows > 0){
					while($row = $result->fetch_assoc()){
						$name = $row["FirstName"];
						$emailAddress = $row["EmailAddress"];
						$eol = PHP_EOL;
						$message = stripslashes("Hi, $name. $eol $eol" . "$applicantName (Username: $username, Email Address: $applicantEmailAddress) has made a booking to stay in $accommodationID. $eol $eol" . "Here are the booking details: $eol" . "Booking ID: $bookingID $eol" . "Check-in date:  $checkInDate $eol" . "Check-out date: $checkOutDate $eol" . "Number of people booked for: $numberOfPeople $eol" . "Total amount due: R" . "$totalCost. " . "$eol $eol" . "You will receive an email once they have submitted their proof of payment. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
						mail($emailAddress, $applicantName . " accommodation booking", $message);
					}
				}
				sendMailToUser($checkInDate, $checkOutDate, $accommodationID, $totalCost, $bookingID, $numberOfPeople, $applicantName, $emailAddress);
			}
		}
			
		//Function sends an email to the user telling them they have provisionally booked accommodation
		function sendMailToUser($checkInDate, $checkOutDate, $accommodationID, $totalCost, $bookingID, $numberOfPeople, $applicantName, $emailAddress){
			$eol = PHP_EOL;
			$message = stripslashes("Hi, $applicantName. $eol $eol" . "Your accommodation at LIV has been provisionally booked with the following details: $eol" . "Booking ID: $bookingID $eol" . "Check-in date:  $checkInDate $eol" . "Check-out date: $checkOutDate $eol" . "Number of people booked for: $numberOfPeople $eol" . "Total amount due: R" . "$totalCost. " . "$eol $eol" . "Please make a payment for this booking within two weeks and upload your proof of payment to this site to confirm your booking. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
			mail($emailAddress, "Your accommodation booking", $message);
		}
			
		//Function checks to ensure that the accommodation is available at the time that the user wants to be there
		function checkAccommodationAvailability($checkInDate, $checkOutDate, $accommodationID, $numberOfPeople){
			$accommodationType = $_SESSION["accommodationType"];
			$sql = "select Capacity from tblAccommodation where AccommodationID = '$accommodationID'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				$row = $result->fetch_assoc();
				$capacity = $row["Capacity"];
							
				//Checks for fully booked accommodation
				$arrUnavailableDates = array();
				for($i = $checkInDate; $i <= $checkOutDate; $i = date('Y-m-d', strtotime($i . ' +1 day'))){
					$sql = "SELECT count(*) as BookingsOnDay FROM tblAccommodationBookings where (CheckInDate <= '$i' and CheckOutDate >= '$i') and AccommodationID = '$accommodationID'";
					$dbConnect = new dbConnect();
					$result = $dbConnect->executeQuery($sql);
					if($result->num_rows > 0){
						$row = $result->fetch_assoc();
						$bookingsOnDay = $row["BookingsOnDay"];
						if($accommodationType== "Lodge" && $bookingsOnDay > 0){
							array_push($arrUnavailableDates, $i);
						}
						else if($bookingsOnDay >= $capacity){
							array_push($arrUnavailableDates, $i);
						}
					}
				}
						
				//Checks for dates that the accommodation has been marked as unavailable	
				if(sizeof($arrUnavailableDates) == 0){
					$sql = "select * from tblAccommodationUnavailability where AccommodationID = '$accommodationID' and ((StartDate between '$checkInDate' and '$checkOutDate') or (EndDate between '$checkInDate' and '$checkOutDate') or (StartDate < '$checkInDate' and EndDate > '$checkOutDate')) ";
					$result = $dbConnect->executeQuery($sql);
					if($result->num_rows > 0){
						$row = $result->fetch_assoc();
						echo "Please note, this accommodation is unavailable between these dates: " . $row["StartDate"] . " and " . $row["EndDate"] . ". Please choose alternative dates.";
						return false;
					}	 
				}
				
				//Displays appropriate message
				if($accommodationType == "Barracks"){
					if(sizeOf($arrUnavailableDates) > 0){
						$message = "The following dates are already fully booked: ";
						for($i = 0; $i < sizeOf($arrUnavailableDates); $i++){
							$message .= $arrUnavailableDates[$i];

							if($i != (sizeof($arrUnavailableDates) - 1)){
								echo ", ";
							}							
						}
						$message .= '. Please reschedule your stay accordingly';
						echo $message;
						return false;
					}
					else {
						return true;
					}
				}
				else{
					if($numberOfPeople > $capacity){
						echo "The accommodation you have chosen can only hold " . $capacity . " people.";
						return false;
					}
					else if(sizeof($arrUnavailableDates) > 0){
						echo "The accommodation you have selected is fully booked on the following dates: ";
						for($i = 0; $i < sizeof($arrUnavailableDates); $i++){
							echo $arrUnavailableDates[$i] ;
							
							if($i != (sizeof($arrUnavailableDates) - 1)){
								echo ", ";
							}
						}
						echo ". Please reschedule your stay accordingly.";
						return false;
					}
					else{
						return true;
					}
				}
			}  
		}
				
		//Function fetches the available accommodation
		function getAccommodation(){
			$accommodationType = $_SESSION["accommodationType"];
			$sql = "select AccommodationID from tblAccommodation where Type = '$accommodationType'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				while($row = $result->fetch_assoc()){
					echo "<option>" . $row["AccommodationID"] . "</option>";
				}
			}
		}
		
		//Function displays the images in an image gallery
		function displayModalImages($accommodationID){
			$dbConnect = new dbConnect();
			$sql = "select * from tblAccommodationImages where AccommodationID = '$accommodationID'";
			$result = $dbConnect->executeQuery($sql);    
	   
			if($result->num_rows > 0){
				while($rows = $result->fetch_assoc()){
					echo "<div class='mySlides'>";
					echo "<p>" . $rows["ImageDescription"] . "</p> <img src='employee/images/" . $rows["ImageID"] . "' style='width:100%'>";
					echo "</div>";
				}
			}
			
			echo "<a class='prev' onclick='plusSlides(-1)'>&#10094;</a>";
			echo "<a class='next' onclick='plusSlides(1)'>&#10095;</a>";
		}
		
		//Function displays image gallery at the bottom of the page
		function displayImageGallery($accommodationID){
			$dbConnect = new dbConnect();
			$sql = "select * from tblAccommodationImages where AccommodationID = '$accommodationID'";
			$result = $dbConnect->executeQuery($sql);
			$count = 1;
			
			if($result->num_rows > 0){
				$columnCount = 1;
				echo "<tr>";
				while($rows = $result->fetch_assoc()){
					echo "<td><div class='column'>";
					echo "<img height='100' width='100' src='employee/images/" . $rows["ImageID"] . "' onclick='openModal();currentSlide($count)' class='hover-shadow'>";
					echo "</div></td>";
					$count++;
					
					if($columnCount % 5 == 0){
						echo "</tr><tr>";
					}
					$columnCount++;
				}
			}
		}
			
		//Ensures the user has logged in before using the page
		if(isset($_SESSION["Username"])){
			//Adds appropriate where clause to the SQL statement, as certain application types are only allowed to stay in certain accommodation
			if($accommodationType == "Barracks"){
				$applicantType = " and (ApplicationType = 'Volunteer' or ApplicationType = 'Gap-year Student')";
			}
			else if($accommodationType == "Lodge"){
				$applicantType = " and (ApplicationType = 'Volunteer' or ApplicationType = 'Guest')";
			}
			
			//Ensures the user has had the appropriate application accepted before booking accommodation
			$sql = "select * from tblApplications where Username = '$username' and ApplicationExpired = 0 and ApplicationAccepted = 1" . $applicantType;
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows == 0){
				if($accommodationType == "Lodge"){
					echo "<script>alert('You are not allowed to stay in the " . $accommodationType . ", as you have not had an application as a Guest or Volunteer accepted');</script>";
				}
				else{
					echo "<script>alert('You are not allowed to stay in the " . $accommodationType . ", as you have not had an application as a Gap-year student or Volunteer accepted');</script>";
				}
				redirectPage("accommodationTypeChoice.php");
			}
			else{
				//Calls appropriate function based on posted values
				if(isset($_POST["makeBooking"])){
					makeBooking($_POST["checkInDate"], $_POST["checkOutDate"], $_POST["accommodationID"], $_POST["numberOfPeople"]);
					unset($_POST["checkInDate"]);
					unset($_POST["checkOutDate"]);
					unset($_POST["accommodationID"]);
					unset($_POST["numberOfPeople"]);
				}
				else if(isset($_POST["getAccommodation"])){
					getAccommodation();
					unset($_POST["getAccommodation"]);
				}
				else if(isset($_POST["getAccommodationPrice"])){
					$costPerNight = getAccommodationPrice($_POST["accommodationID"]);
					unset($_POST["getAccommodationPrice"]);
					unset($_POST["accommodationID"]);
					echo $costPerNight;
				}
				else if(isset($_POST["fetchImages"])){
					displayImageGallery($_POST["accommodationID"]);
				}
				else if(isset($_POST["fetchModalImages"])){
					displayModalImages($_POST["accommodationID"]);
				}
				
				if(!isset($_POST["ajaxResponse"])){
        ?>
		<style>
		<!-- Style and methods for image gallery obtained from https://www.w3schools.com/howto/howto_js_lightbox.asp -->
	.row > .column {
  padding: 0 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 25%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 1;
  padding-top: 100px;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: black;
  
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  width: 90%;
  max-width: 1200px;
  
}

/* The Close Button */
.close {
  color: white;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
   vertical-align: middle;
  width: auto;
  padding: 16px;
  
  color: white;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* Position the "next button" to the right */
.prev {
  left: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

.caption-container {
  text-align: center;
  background-color: black;
  padding: 2px 16px;
  color: white;
}

img.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
}
</style>
	
		<h1><?php echo $_SESSION["accommodationType"]; ?> Accommodation Booking</h1>
		<?php
			include "calendar.php";
		?>
                
        <p>Accommodation: <select id="cmbAccommodationID"></select>
		<p id="txtAccommodationPrice"></p>
		<p float='left'>Please select the dates that you would like to stay at LIV by clicking on the calendar above, or on the date pickers below.</p>

		<p class='form'>Note: Dates highlighted in green are the dates you have selected.</p>
		<p class='form'><label>Check In Date:</label><br/> <input type="date" id="checkInDate" name="checkInDate" value="<?php echo date('Y-m-d'); ?>"/> </p>
		<p class='form'><label>Check Out Date:</label><br/> <input type="date" id="checkOutDate" name="checkOutDate" value="<?php echo date('Y-m-d'); ?>"/> </p>
		<p id="numberOfPeopleStaying"><label>Number of people staying</label><br/><input type='number'min='1' value='1' name="spnNumberOfPeople" id="spnNumberOfPeople"/>
		<h3 id="txtTotalCost"></h3>
		<h3>Please pay within 2 weeks and send through your proof of payment, otherwise your booking will be cancelled</h2>
		<button onclick="makeBooking()">Place booking</button>
        <br/>
        <br/>
        <h2>Image Gallery</h2>

        <table id='tblImages' width="100%">
		<tr>
			<div class="row"></div>
		</tr>
		<div id="myModal" class="modal" style="height: 100%">
		  <span class="close cursor" onclick="closeModal()">&times;</span>
		  <div class="modal-content" id="modalContent"></div>
		</div>
	</body> 
	
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>	
    <script>
        var previousClick = null, firstDate = null, secondDate = null, counter = 1, currentMonth = null;
        window.onload = setListeners;
		var costPerNight;
		var accommodationType = '<?php echo $_SESSION["accommodationType"]; ?>';
          
		//Function sets listeners that will perform a specific task when a specific event occurs
        function setListeners(){
			displayLoader();
            var cmbCheckInDate = document.getElementById("checkInDate");
            var cmbCheckOutDate = document.getElementById("checkOutDate");
            var cmbAccommodationID = document.getElementById("cmbAccommodationID");
			var spnNumberOfPeople = document.getElementById("spnNumberOfPeople");
			
			if(accommodationType == "Barracks"){
				var numberOfPeople = document.getElementById("numberOfPeopleStaying");
				
				numberOfPeople.style.display = "none";
			}
			
			spnNumberOfPeople.addEventListener("change", function() {
				displaySelectedDates();
			});

            cmbCheckInDate.addEventListener("change", function() {
				firstDate = $(cmbCheckInDate).val();
				displaySelectedDates();
			});
		
			cmbCheckOutDate.addEventListener("change", function() {
				secondDate = $(cmbCheckOutDate).val();
				displaySelectedDates();
			});
                        
            $.ajax({
                url : window.location.pathname,
                type : "post",
                data: {"ajaxResponse":"1", "getAccommodation":"1"},
                success: function(response){
                    cmbAccommodationID.innerHTML = response;
                    getAccommodationPrice($(cmbAccommodationID).val());
					fetchImages($(cmbAccommodationID).val());
                }                
            });
                      
            cmbAccommodationID.addEventListener("change", function() {
				getAccommodationPrice($(cmbAccommodationID).val());	
				var accommodationID = $(cmbAccommodationID).val();		
				fetchImages(accommodationID);
			});
                        
            firstDate = $(cmbCheckInDate).val();
            secondDate = $(cmbCheckOutDate).val();
            displaySelectedDates();					
		}
		
		//Function fetches the images for the image gallery
		function fetchImages(accommodationID){
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"accommodationID": accommodationID, "fetchImages": "1", "ajaxResponse": "1"},
				success: function(response){
							var tblImages = document.getElementById("tblImages");
							tblImages.innerHTML = response;
						}
			}); 
						
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"accommodationID": accommodationID, "fetchModalImages": "1", "ajaxResponse": "1"},
				success: function(response){
							var modalContent = document.getElementById("modalContent");
							modalContent.innerHTML = response;
						}
			}); 
		}
         
		//Function displays the price per night for the accommodation
        function getAccommodationPrice(accommodationID){
			displayLoader();
            $.ajax({
                url: window.location.pathname, 
                type: "post",
                data: {"getAccommodationPrice": "1", "accommodationID": accommodationID, "ajaxResponse": "1"},
                success: function(response){
							var txtAccommodationPrice = document.getElementById("txtAccommodationPrice");
                            txtAccommodationPrice.innerHTML = 'Cost per night: R' + response;
							costPerNight = response;
							displaySelectedDates();
							hideLoader();
                        }
            });
        }
    
		//Function displays the date that was clicked in the appropriate date pickers
        function handleDateClick(date){                   
            date = new Date(date);
            var formattedDate = formatDate(date);
       
            if(counter == 0){
                firstDate = formattedDate;
                counter++;
                var cmbCheckIn = document.getElementById("checkInDate");
                $(cmbCheckIn).val(formattedDate);
                secondDate = firstDate;
                var cmbCheckOut = document.getElementById("checkOutDate");
                $(cmbCheckOut).val(formattedDate);
            }
            else{
                secondDate = formattedDate;
                counter--;
                var cmb = document.getElementById("checkOutDate");
                $(cmb).val(formattedDate);
            }
            if(firstDate >= secondDate){
                secondDate = formattedDate;
                firstDate = formattedDate;
                if(counter == 0){
                    counter++;
                }
                var cmb = document.getElementById("checkInDate");
                $(cmb).val(formattedDate);
            }
            if(firstDate != null && secondDate != null){
                displaySelectedDates();
            }
        }
         
		//Function displays the selected dates in the calendar 
        function displaySelectedDates(){
            var table = document.getElementById("table");
			var date1 = new Date(firstDate);
			var date2 = new Date(secondDate);
			var timeDifference = Math.abs(date2.getTime() - date1.getTime());
			var daysDifference = Math.ceil(timeDifference / (1000 * 3600 * 24)); 
			var txtTotalCost = document.getElementById("txtTotalCost");
			var totalCost = daysDifference * costPerNight;
			var spnNumberOfPeople = document.getElementById("spnNumberOfPeople");
			
			if(daysDifference == 0){
				totalCost = 0;
			}
			totalCost *= $(spnNumberOfPeople).val();
			txtTotalCost.innerHTML = "Total cost: R" + totalCost;
			
			for (var i = 2, row; row = table.rows[i]; i++) {
				for (var j = 0, col; col = row.cells[j]; j++) {
					currentMonth = table.rows[0].cells[1].innerHTML;
					var colValue = new Date(col.innerHTML + ' ' + currentMonth);
					colValue = formatDate(colValue);
					if(colValue >= firstDate && colValue <= secondDate && col.innerHTML != ""){
						$(col).addClass('selectedDate');
						$(col).removeClass('unselectedDate');
				    }
                    else{
                        $(col).addClass('unselectedDate');
						$(col).removeClass('selectedDate');
                    }
				} 
			}
		}

		//Function clears the selected dates from the calendar
		function clearSelection(date){
			var selectedDates = document.getElementsByClassName("selectedDate");
			for(var i = 0; i < selectedDates.length; i++){
				var cel = selectedDates[i];
				$(cel).removeClass('selectedDate');
				$(cel).addClass('unselectedDate');
                alert(cel.innerHTML + "  " + cel.classList);
			}
		}

		//Function formats the date in yyyy-mm-dd format
		//https://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
		function formatDate(date) {
			var month = '' + (date.getMonth() + 1),
			day = '' + date.getDate(),
			year = date.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('-');
		}
         
		//Function makes a provisional accommodation booking
        function makeBooking(){
			displayLoader();
            var cmbCheckInDate = document.getElementById("checkInDate");
            var cmbCheckOutDate = document.getElementById("checkOutDate");
            var cmbAccommodationID = document.getElementById("cmbAccommodationID");
			var spnNumberOfPeople = document.getElementById("spnNumberOfPeople");
                  
			if($(cmbAccommodationID).has('option').length == 0){
				alert('Please select accommodation');
			}	
			else{
				var checkInDate = $(cmbCheckInDate).val();
				var checkOutDate = $(cmbCheckOutDate).val();
				var accommodationID = $(cmbAccommodationID).val();
				var numberOfPeople = $(spnNumberOfPeople).val();
				
				if(checkOutDate <= checkInDate){
					alert("Please enter a check out date that is after your check in date");
				}
				else{
					$.ajax({
						url: window.location.pathname,
						type: "post",
						data: {"makeBooking": "1", "checkInDate" : checkInDate, "checkOutDate": checkOutDate, "accommodationID": accommodationID , "numberOfPeople": numberOfPeople, "ajaxResponse": "1"},
						success: function(response){
									hideLoader();
									if(response == 1){
										alert("Accommodation provisionally booked");
										window.location = "accommodationTypeChoice.php";
									}
									else {
										alert(response);
									}
								}                        
					});
				}
			}
        }
		
		//Function opens the modal window
		function openModal() {
			document.getElementById('myModal').style.display = "block";
		}

		//Function closes the modal window
		function closeModal() {
			document.getElementById('myModal').style.display = "none";
		}

		var slideIndex = 1;
		showSlides(slideIndex);

		//Functions to navigate the image gallery
		function plusSlides(n) {
		  showSlides(slideIndex += n);
		}

		function currentSlide(n) {
		  showSlides(slideIndex = n);
		}

		function showSlides(n) {
			var i;
			var slides = document.getElementsByClassName("mySlides");
			var dots = document.getElementsByClassName("demo");
			var captionText = document.getElementById("caption");
			if (n > slides.length) {slideIndex = 1}
			if (n < 1) {slideIndex = slides.length}
			for (i = 0; i < slides.length; i++) {
				slides[i].style.display = "none";
			}
			for (i = 0; i < dots.length; i++) {
				dots[i].className = dots[i].className.replace(" active", "");
			}
			slides[slideIndex-1].style.display = "block";
			dots[slideIndex-1].className += " active";
			captionText.innerHTML = dots[slideIndex-1].alt;
		}
    </script>
</html>
<?php
			}
		}
	}
?>