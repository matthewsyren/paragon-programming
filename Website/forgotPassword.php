<?php
    if(!isset($_POST["ajaxResponse"])){
        include "styles.css";
    }
    include "dbConn.php";
	include "sharedFunctions.php";
        
	//Calls appropriate function based on posted values	
    if(isset($_POST["sendResetCode"])){
        sendResetCode($_POST["username"]);
        unset($_POST["username"]);
    }
    else if(isset($_POST["resetPassword"])){
        resetPassword($_POST["username"], $_POST["resetCode"], $_POST["newPassword"]);
        unset($_POST["username"]);
        unset($_POST["resetCode"]);
        unset($_POST["newPassword"]);
    }
        
    //Generates a randomised 8-character code, adds it to the database and sends it to the user
    function sendResetCode($username){
		//Ensures that the username value is safe to work with (prevents SQL injection etc.)
		$username = formatInput($username);
        $sql = "select * from tblUsers where Username = '$username';";
        $dbConnect = new dbConnect();
        $result = $dbConnect->executeQuery($sql);
        if($result->num_rows > 0){
            $row = $result->fetch_assoc();
            $firstName = $row["FirstName"];
            $emailAddress = $row["EmailAddress"];
			
			//Generates code
            $code = '';
            for($i = 0; $i < 8; $i++){
                $asc = rand(65, 90);
                $code .= chr($asc);                           
            }
                   
			//Adds code to the database
            $sql = "update tblUsers set ResetPasswordCode = '$code' where Username = '$username';";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
            if($result == 1){       
				//Sends code to user
                $message = "Hi, $firstName. \nThe code to reset your password is $code. Enter this code on the forgot password page into the 'Reset Code' field, type in your username and new password and then click on the 'Reset Password' button. ";
                echo mail($emailAddress,"Password Recovery",$message);
            }
        }
        else{
            echo -1;
        }         
    }
        
    //Function resets the user's password if the reset code is correct
    function resetPassword($username, $resetCode, $newPassword){
		//Ensures that the variable values are safe to work with (prevents SQL injection etc.)
		$username = formatInput($username);
		$resetCode = formatInput($resetCode);
		$newPassword = formatInput($newPassword);
		
		//Compares reset code entered by user to the reset code in the database
        $sql = "select * from tblUsers where Username = '$username' and ResetPasswordCode = '$resetCode';";
        $dbConnect = new dbConnect();
        $result = $dbConnect->executeQuery($sql);
        if($result->num_rows > 0){
            $hashedPassword = hash('sha256', $newPassword);
            $sql = "update tblUsers set Password = '$hashedPassword' where Username = '$username'";
            $result = $dbConnect->executeQuery($sql);
            echo $result;
        }
        else{
            echo -1;
        }    
    }

    if(!isset($_POST["ajaxResponse"])){
?>
<style>
	input{
		width: 200px;
	}
</style>
<html>
        <h1>Forgot Password</h1>
        
        <p>Username:<br/><input type="text" id="txtUsername"/></p>
        <p>Reset Code:<br/><input type="text" id="txtResetCode"/></p>
        <p>New Password:<br/><input type="password" id="txtNewPassword"/></p>
        <button onclick="resetPassword()">Reset Password</button>
		<p>To get the reset code, enter your username and click on the Send Reset Code button below. An email with a randomised code will be emailed to you. Enter this code into the Reset Code field when entering your new password, and then click on the Reset Password button to reset your password. 
        <button onclick="sendResetCode()">Send Reset Code</button> 
        </p>
</html>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
	
	//Function sends an email to the user with the reset code for their account
    function sendResetCode(){
        var txtUsername = document.getElementById("txtUsername");
        var username = $(txtUsername).val();
        if(username.length == 0){
            alert("Please enter your username");
        }
        else{
            $.ajax({
                url: window.location.pathname,
                type: "post",
                data: {"sendResetCode": "1", "username" : username, "ajaxResponse": "1"},
                success: function(response){
                    if(response == 1){
                        alert("An email has been sent to you with the reset code");
                    }
                    else if(response == -1){
                        alert("There is no account associated with the username you have entered, please enter a valid username");
                    }
                    else{
                        alert("A problem occurred while sending the email, please try again");
                    }
                }
            });
        }
    }
        
	//Function resets the password of the user by passing the appropriate data to the PHP page	
    function resetPassword(){
        var txtUsername = document.getElementById("txtUsername");
        var txtResetCode = document.getElementById("txtResetCode");
        var txtNewPassword = document.getElementById("txtNewPassword");
               
        var username = $(txtUsername).val();
        var resetCode = $(txtResetCode).val();
        var newPassword = $(txtNewPassword).val();
                
        $.ajax({
            url: window.location.pathname,
            type: "post",
            data: {"resetPassword": "1", "username": username, "resetCode": resetCode, "newPassword": newPassword, "ajaxResponse": "1"},
            success: function(response){
                if(response == 1){
					alert("Password changed successfully");    
					window.location = "login.php";					
                }
                else if(response == -1){
                    alert("There is no username associated with that reset code, please ensure you enter the correct username and reset code");
                }
                else{
                    alert("An error occurred while resetting your password, please try again...");
                }
            }
        });
    }
</script>

<?php
    }
?>