<?php
    class dbConnect{
        private $hostIP;
        private $dbName;
        private $username;
        private $password;
               
		function __construct(){
            $this->hostIP = "pdb9.awardspace.net";
            $this->dbName = "2349078_matthewsyren";
            $this->username= "2349078_matthewsyren";
            $this->password = "Password123";
        }
                
        //Function establishes a connection between the program and the database
        function openConnection(){
            $dbConnect = mysqli_connect($this->hostIP, $this->username, $this->password, $this->dbName);
            return $dbConnect;
        }
                
        //Function executes the SQL query that is passed in as a parameter, and returns the result of the query
        function executeQuery($sql){
            $dbConnect = $this->openConnection();
            //Stores the result of the query and returns it if the connection was successful
            if (!$dbConnect->connect_error) {
                //Returns the result from executing the query
                $result = $dbConnect->query($sql);
                $dbConnect->close();
            } 
            else{
                $result = false;
                echo "<p class='error'>Couldn't connect to database...</p>";
            }
            return $result;
        }
    }
?>