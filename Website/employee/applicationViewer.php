<?php
session_start();

//Ensures the user has logged in before using the page
if(isset($_SESSION["LivEmployeeUsername"])){
	if(!isset($_POST["ajaxResponse"])){
		include "navbar.php";
		include "../styles.css";
?>
<html>
<style>
    p{
		display: block;
        text-align: centre;
	}
</style>
<br/>
<br/>
	<h1>Application Viewer</h1>
	<p><label>Application type</label>
		<select id="cmbApplicationType">
			<option>Gap-year Student</option>
            <option>Guest</option>
			<option>Volunteer</option>
		</select>
    </p>
    <p><label>Username</label>
    <select id="cmbApplicants"></select></p>
    <h2>Application details</h2>    

	<div id="applicationContent"></div><br/>
	<button onclick="requestSkypeInterview()">Request Skype Interview</button><br/>
	<p><label>Reason for rejection</label><br/><input type="text" id="txtReasonForRejection"></input></p>
	<button onclick="rejectApplication()">Reject Application</button><br/>
	<button onclick="acceptApplication()">Accept Application</button><br/>
</html>

<?php
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
	include "../dbConn.php";
       
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		//Calls the appropriate method based on the posted variables
		if(isset($_POST["requestSkypeInterview"])){
			requestSkypeInterview($_POST["applicantUsername"], $_POST["applicationType"]);
			unset($_POST["requestSkypeInterview"]);
			unset($_POST["applicantUsername"]);
			unset($_POST["applicantType"]);
		}
		else if(isset($_POST["rejectApplication"])){
			rejectApplication($_POST["applicantUsername"], $_POST["applicationType"], $_POST["reasonForRejection"]);
			unset($_POST["rejectApplication"]);
			unset($_POST["applicantUsername"]);
			unset($_POST["applicantType"]);
		}
		else if(isset($_POST["acceptApplication"])){
			acceptApplication($_POST["applicantUsername"], $_POST["applicationType"]);
			unset($_POST["acceptApplication"]);
			unset($_POST["applicantUsername"]);
			unset($_POST["applicantType"]);
		}
		else if(isset($_POST["applicationType"])){
			getApplicants($_POST["applicationType"]);
			unset($_POST["getVolunteerApplications"]);
		}
		else if(isset($_POST["applicantUsername"]) && isset($_POST["userType"])){
			if($_POST["userType"] == "Volunteer"){
				getVolunteerApplicant($_POST["applicantUsername"]);
			}
			else if($_POST["userType"] == "Gap-year Student"){
				getGapYearApplicant($_POST["applicantUsername"]);
			}
			else{
				getGuestApplicant($_POST["applicantUsername"]);
			}
			unset($_POST["applicantUsername"]);
		}
	}
		
	//Method sends an email to the applicant telling them to book a Skype Interview, and updates the database to reflect that a Skype Interview has been requested (the user can only book an interview if the database says an interview has been requested)	
	function requestSkypeInterview($applicantUsername, $applicationType){
		$dbConnect = new dbConnect();
		$sql = "update tblApplications set SkypeInterviewAccepted = 1 where Username = '$applicantUsername' and ApplicationAccepted = 0 and ApplicationType = '$applicationType';";
		$result = $dbConnect->executeQuery($sql);
                
        //Sends an email requesting a Skype Interview
		if($result == 1){
            $dbConnect = new dbConnect();
            $sql = "select FirstName, EmailAddress from tblUsers where Username = '$applicantUsername'";
            $result = $dbConnect->executeQuery($sql);
            if($result->num_rows > 0){
				$row = $result->fetch_assoc();
                $eol = PHP_EOL;
                $livUsername = $_SESSION["LivEmployeeUsername"];
                $emailAddress = $row["EmailAddress"];
                $message = "Hi, " . $row["FirstName"] . ". $eol $eol" . "Your application to become a $applicationType has been reviewed, and we would like to schedule a Skype interview with you. Please head over to the Skype page of the site and book an interview with $livUsername. $eol $eol" . "Kind regards, $eol" . "LIV Portal";
                echo mail($emailAddress, "Your " . $applicationType . " application", $message);
            }
        }
	}
		
	//Method sends an email to the applicant saying their application has been rejected, and reflects this in the database	
	function rejectApplication($applicantUsername, $applicationType, $reason){
		$dbConnect = new dbConnect();
		$sql = "delete from tblApplications where Username = '$applicantUsername'";
		$result = $dbConnect->executeQuery($sql);
		
        //Sends an email to the applicant telling them their application has been rejected
        if($result == 1){
            $dbConnect = new dbConnect();
            $sql = "select FirstName, EmailAddress from tblUsers where Username = '$applicantUsername'";
            $result = $dbConnect->executeQuery($sql);
                       
            if($result->num_rows > 0){
                $row = $result->fetch_assoc();
				$eol = PHP_EOL;
                $livUsername = $_SESSION["LivEmployeeUsername"];
                $emailAddress = $row["EmailAddress"];
                $message = "Hi, " . $row["FirstName"] . ". $eol $eol" . "Your application to become a $applicationType has been rejected for the following reason: " . $reason . "$eol $eol" . "Kind regards, $eol" . "LIV Portal";
                echo mail($emailAddress, "Your " . $applicationType . " application", $message);
            }
        }
	}
		
	//Method sends an email to the applicant saying their application has been accepted, and reflects this in the database		
	function acceptApplication($applicantUsername, $applicationType){
		$dbConnect = new dbConnect();
		$sql = "update tblApplications set ApplicationAccepted = 1 where Username = '$applicantUsername' and ApplicationAccepted = 0 and ApplicationType = '$applicationType';";
		$result = $dbConnect->executeQuery($sql);
		
        //Sends an email to the applicant saying their application has been accepted
        if($result == 1){
            $dbConnect = new dbConnect();
			$sql = "select FirstName, EmailAddress from tblUsers where Username = '$applicantUsername'";
            $result = $dbConnect->executeQuery($sql);
            if($result->num_rows > 0){
                $row = $result->fetch_assoc();
                $eol = PHP_EOL;
                $livUsername = $_SESSION["LivEmployeeUsername"];
                $emailAddress = $row["EmailAddress"];
                $message = "Hi, " . $row["FirstName"] . ". $eol $eol" . "Your application to become a $applicationType has been accepted. Please head over to the Accommodation page of the site and book accommodation for the dates that you would like to come here. $eol $eol" . "Kind regards, $eol" . "LIV Portal";
                echo mail($emailAddress, "Your " . $applicationType . " application", $message);
            }
        }
	}
        
	//Method fetches the usernames of the applicants that have applied for a specific role e.g. Volunteer
    function getApplicants($applicantType){
        $dbConnect = new dbConnect();
        $sql = "select Username from tblApplications where ApplicationType = '$applicantType' and ApplicationAccepted = 0";
        $result = $dbConnect->executeQuery($sql);
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                echo "<option>" . $row["Username"] . "</option>";
            }
        }
    }
        
	//Function displays details for a gap-year application	
    function getGapYearApplicant($username){
		$dbConnect = new dbConnect();
        $sql = "select * from tblGapYearStudents where Username = '$username'";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			while($rows = $result->fetch_assoc()){
				echo "<p><strong>Username: </strong>" . $rows["Username"] . "</p>";
				echo "<p><strong>Title: </strong>" . $rows["Title"] . "</p>";
				echo "<p><strong>Address: </strong>" . $rows["Address"] . "</p>";
				echo "<p><strong>Home Telephone Number: </strong>" . $rows["HomeTelephoneNumber"] . "</p>";
				echo "<p><strong>Business Telephone Number: </strong>" . $rows["BusinessTelephoneNumber"] . "</p>";
				echo "<p><strong>Cell Number: </strong>" . $rows["CellNumber"] . "</p>";
				echo "<p><strong>Date of Birth: </strong>" . $rows["DateOfBirth"] . "</p>";
				echo "<p><strong>Age: </strong>" . $rows["Age"] . "</p>";
				echo "<p><strong>Gender: </strong>" . $rows["Gender"] . "</p>";
				echo "<p><strong>Nationality: </strong>" . $rows["Nationality"] . "</p>";
				echo "<p><strong>Passport Number: </strong>" . $rows["PassportNumber"] . "</p>";
				echo "<p><strong>Date Issued: </strong>" . $rows["DateIssued"] . "</p>";
				echo "<p><strong>Valid Until: </strong>" . $rows["ValidUntil"] . "</p>";
				echo "<p><strong>MaritalStatus: </strong>" . $rows["MaritalStatus"] . "</p>";
				echo "<p><strong>Emergency Contact Name: </strong>" . $rows["EmergencyContactName"] . "</p>";
				echo "<p><strong>Emergency Contact Number: </strong>" . $rows["EmergencyContactTelephone"] . "</p>";
				echo "<p><strong>Emergency Contact Address: </strong>" . $rows["EmergencyContactAddress"] . "</p>";
				echo "<p><strong>Emergency Contact Relationship: </strong>" . $rows["EmergencyContactRelationship"] . "</p>";
				echo "<p><strong>Current Local Church: </strong>" . $rows["CurrentLocalChurch"] . "</p>";
				echo "<p><strong>Church Address: </strong>" . $rows["ChurchAddress"] . "</p>";
				echo "<p><strong>Church Pastor: </strong>" . $rows["ChurchPastor"] . "</p>";
				echo "<p><strong>Church Email: </strong>" . $rows["ChurchEmail"] . "</p>";
				echo "<p><strong>Time Spend At Church: </strong>" . $rows["TimeSpentAtChurch"] . "</p>";
				echo "<p><strong>Reasons For Being A Christian: </strong>" . $rows["ReasonsForBecomingAChristian"] . "</p>";
				echo "<p><strong>Church Involvement: </strong>" . $rows["ChurchInvolvement"] . "</p>";
				echo "<p><strong>Mission Work: </strong>" . $rows["MissionWork"] . "</p>";
				echo "<p><strong>Highest Level Of Education Completed: </strong>" . $rows["HighestLevelOfEducationCompleted"] . "</p>";
				echo "<p><strong>Current Occupation: </strong>" . $rows["CurrentOccupation"] . "</p>";
				echo "<p><strong>Languages Spoken: </strong>" . $rows["LanguagesSpoken"] . "</p>";
				echo "<p><strong>Intercultural Experiences: </strong>" . $rows["InterculturalExperiences"] . "</p>";
				echo "<p><strong>Other Skills Or Work Experience: </strong>" . $rows["OtherSkillsOrWorkExperience"] . "</p>";
				echo "<p><strong>Hobbies And Interests: </strong>" . $rows["HobbiesAndInterests"] . "</p>";
				echo "<p><strong>Future Plans: </strong>" . $rows["FuturePlans"] . "</p>";
				echo "<p><strong>How Liv Was Discovered: </strong>" . $rows["HowLivWasDiscovered"] . "</p>";
				echo "<p><strong>Previous Experience With LIV: </strong>" . $rows["PreviousExperienceWithLiv"] . "</p>";
				echo "<p><strong>Reasons For Applying: </strong>" . $rows["ReasonsForApplying"] . "</p>";
				echo "<p><strong>Expectations For Program: </strong>" . $rows["ExpectationsForProgram"] . "</p>";
				echo "<p><strong>Areas To Focus On: </strong>" . $rows["AreasToFocusOn"] . "</p>";
				echo "<p><strong>Greatest Strengths: </strong>" . $rows["GreatestStrengths"] . "</p>";
				echo "<p><strong>Greatest Weaknesses: </strong>" . $rows["GreatestWeaknesses"] . "</p>";
				echo "<p><strong>Life Circumstances And Challenges: </strong>" . $rows["LifeCircumstancesAndChallenges"] . "</p>";
				echo "<p><strong>Family Opinion Of LIV: </strong>" . $rows["FamilyOpinionOfLiv"] . "</p>";
				echo "<p><strong>Adaptability: </strong>" . $rows["Adaptability"] . "</p>";
				echo "<p><strong>Team Skills: </strong>" . $rows["TeamSkills"] . "</p>";
				echo "<p><strong>Communication Skills: </strong>" . $rows["CommunicationSkills"] . "</p>";
				echo "<p><strong>Relationship With Unliked People: </strong>" . $rows["RelationshipWithUnlikedPeople"] . "</p>";
				echo "<p><strong>Substance Abuse History: </strong>" . $rows["SubstanceAbuseHistory"] . "</p>";
				echo "<p><strong>Smoker: </strong>" . $rows["Smoker"] . "</p>";
				echo "<p><strong>Inebriation Frequency: </strong>" . $rows["InebriationFrequency"] . "</p>";
				echo "<p><strong>Occult Activities: </strong>" . $rows["OccultActivities"] . "</p>";
				echo "<p><strong>Past Pshychological Treatment: </strong>" . $rows["PastPsychologicalTreatment"] . "</p>";
				echo "<p><strong>Medical Problems: </strong>" . $rows["MedicalProblems"] . "</p>";
				echo "<p><strong>Criminal Convictions: </strong>" . $rows["CriminalConvictions"] . "</p>";
			}
		}
        
		//Displays the references' details
        $dbConnect = new dbConnect();
        $sql = "select * from tblGapyearReferences where ApplicantUsername = '$username'";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
            $count = 1;
			while($rows = $result->fetch_assoc()){
                echo "<p><strong>Reference " . $count . "</strong></p>";
                echo "<p><strong>Full Name: </strong>" . $rows["FullName"] . "</p>";
                echo "<p><strong>Address: </strong>" . $rows["Address"] . "</p>";
                echo "<p><strong>Telephone Number: </strong>" . $rows["TelephoneNumber"] . "</p>";
                echo "<p><strong>Cell Number: </strong>" . $rows["CellNumber"] . "</p>";
                echo "<p><strong>Email Address: </strong>" . $rows["EmailAddress"] . "</p>";
				echo "<p><strong>Relationship To Applicant: </strong>" . $rows["RelationshipToApplicant"] . "</p>";
                $count++;                   
            }         
        }
	}
     
	//Function displays details from a volunteer application
    function getVolunteerApplicant($username){
		$dbConnect = new dbConnect();
        $sql = "select * from tblVolunteers where Username = '$username'";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			while($rows = $result->fetch_assoc()){
				echo "<p><strong>Username: </strong>" . $rows["Username"] . "</p>";
				echo "<p><strong>Title: </strong>" . $rows["Title"] . "</p>";
				echo "<p><strong>Address: </strong>" . $rows["Address"] . "</p>";
				echo "<p><strong>Home Telephone Number: </strong>" . $rows["HomeTelephoneNumber"] . "</p>";
				echo "<p><strong>Business Telephone Number: </strong>" . $rows["BusinessTelephoneNumber"] . "</p>";
				echo "<p><strong>Cell Number: </strong>" . $rows["CellNumber"] . "</p>";
				echo "<p><strong>Date of Birth: </strong>" . $rows["DateOfBirth"] . "</p>";
				echo "<p><strong>Age: </strong>" . $rows["Age"] . "</p>";
				echo "<p><strong>Gender: </strong>" . $rows["Gender"] . "</p>";
				echo "<p><strong>Nationality: </strong>" . $rows["Nationality"] . "</p>";
				echo "<p><strong>Passport Number: </strong>" . $rows["PassportNumber"] . "</p>";
				echo "<p><strong>Date Issued: </strong>" . $rows["DateIssued"] . "</p>";
				echo "<p><strong>Valid Until: </strong>" . $rows["ValidUntil"] . "</p>";
				echo "<p><strong>MaritalStatus: </strong>" . $rows["MaritalStatus"] . "</p>";
				echo "<p><strong>Residential Permit: </strong>" . $rows["ResidentialPermit"] . "</p>";
				echo "<p><strong>Religion: </strong>" . $rows["Religion"] . "</p>";
				echo "<p><strong>Church: </strong>" . $rows["Church"] . "</p>";
				echo "<p><strong>Current Occupation: </strong>" . $rows["CurrentOccupation"] . "</p>";
				echo "<p><strong>Area To Volunteer: </strong>" . $rows["AreaToVolunteer"] . "</p>";
				echo "<p><strong>Emergency Contact Name: </strong>" . $rows["EmergencyContactName"] . "</p>";
				echo "<p><strong>Emergency Contact Number: </strong>" . $rows["EmergencyContactTelephone"] . "</p>";
				echo "<p><strong>Emergency Contact Address: </strong>" . $rows["EmergencyContactAddress"] . "</p>";
				echo "<p><strong>Emergency Contact Relationship: </strong>" . $rows["EmergencyContactRelationship"] . "</p>";
				echo "<p><strong>Medical Problems: </strong>" . $rows["MedicalProblems"] . "</p>";
				echo "<p><strong>Intercultural Experiences: </strong>" . $rows["InterculturalExperiences"] . "</p>";
				echo "<p><strong>Other Skills Or Work Experience: </strong>" . $rows["OtherSkillsOrWorkExperience"] . "</p>";
				echo "<p><strong>Hobbies And Interests: </strong>" . $rows["HobbiesAndInterests"] . "</p>";
				echo "<p><strong>Worked With NGO Before: </strong>" . $rows["WorkedWithNGOBefore"] . "</p>";
				echo "<p><strong>How Liv Was Discovered: </strong>" . $rows["HowLivWasDiscovered"] . "</p>";
				echo "<p><strong>Previous Experience With LIV: </strong>" . $rows["PreviousExperienceWithLiv"] . "</p>";
				echo "<p><strong>Reasons For Volunteering: </strong>" . $rows["ReasonsForVolunteering"] . "</p>";
				echo "<p><strong>Expectations As A Volunteer: </strong>" . $rows["ExpectationsAsAVolunteer"] . "</p>";
				echo "<p><strong>Substance Abuse History: </strong>" . $rows["SubstanceAbuseHistory"] . "</p>";
				echo "<p><strong>Smoker: </strong>" . $rows["Smoker"] . "</p>";
				echo "<p><strong>Inebriation Frequency: </strong>" . $rows["InebriationFrequency"] . "</p>";
				echo "<p><strong>Past Pshychological Treatment: </strong>" . $rows["PastPsychologicalTreatment"] . "</p>";
				echo "<p><strong>Life Circumstances And Challenges: </strong>" . $rows["LifeCircumstancesAndChallenges"] . "</p>";
				echo "<p><strong>Family Opinion Of LIV: </strong>" . $rows["FamilyOpinionOfLiv"] . "</p>";
				echo "<p><strong>Relationship With Others: </strong>" . $rows["RelationshipsWithOthers"] . "</p>";
				echo "<p><strong>Criminal Convictions: </strong>" . $rows["CriminalConvictions"] . "</p>";
				echo "<p><strong>Additional Information: </strong>" . $rows["AdditionalInformation"] . "</p>"; 
            }
		}
               
		//Displays the references' information
        $dbConnect = new dbConnect();
        $sql = "select * from tblVolunteerReferences where ApplicantUsername = '$username'";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
            $count = 1;
			while($rows = $result->fetch_assoc()){
                echo "<p><strong>Reference " . $count . "</strong></p>";
                echo "<p><strong>Full Name: </strong>" . $rows["FullName"] . "</p>";
                echo "<p><strong>Organisation: </strong>" . $rows["Organisation"] . "</p>";
                echo "<p><strong>Position Held: </strong>" . $rows["PositionHeld"] . "</p>";
                echo "<p><strong>Telephone Number: </strong>" . $rows["TelephoneNumber"] . "</p>";
                echo "<p><strong>Email Address: </strong>" . $rows["EmailAddress"] . "</p>";
                $count++;                       
            }         
        }
        
		//Displays the applicant's education information
        $dbConnect = new dbConnect();
        $sql = "select * from tblApplicantEducation where Username = '$username'";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
            $count = 1;
			while($rows = $result->fetch_assoc()){
                echo "<p><strong>School " . $count . "</strong></p>";
                echo "<p><strong>Name of School: </strong>" . $rows["NameOfSchool"] . "</p>";
                echo "<p><strong>Date Started: </strong>" . $rows["DateStarted"] . "</p>";
                echo "<p><strong>Date Ended: </strong>" . $rows["DateEnded"] . "</p>";
                $count++;                        
            }         
        }
              
		//Displays the applicant's work experience information
        $dbConnect = new dbConnect();
        $sql = "select * from tblApplicantWorkExperience where Username = '$username'";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
            $count = 1;
			while($rows = $result->fetch_assoc()){
                echo "<p><strong>Organisation " . $count . "</strong></p>";
				echo "<p><strong>Company: </strong>" . $rows["Organisation"] . "</p>";
                echo "<p><strong>Position Held: </strong>" . $rows["PositionHeld"] . "</p>";
                echo "<p><strong>Date Started: </strong>" . $rows["DateStarted"] . "</p>";
                echo "<p><strong>Date Ended: </strong>" . $rows["DateEnded"] . "</p>";
                echo "<p><strong>Reason for Leaving: </strong>" . $rows["ReasonForLeaving"] . "</p>";
                $count++;                    
            }         
		}
    }
	
	//Function displays the details from a guest application
	function getGuestApplicant($username){
		$sql = "select * from tblUsers where Username = '$username'";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			echo "<p><strong>Name: " . $row["FirstName"] . ' ' . $row["LastName"] . "</strong></p>";
			echo "<p><strong>Email Address: " . $row["EmailAddress"] . "</strong></p>";
		}
	}
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		if(!isset($_POST["ajaxResponse"])){
			include "../loaderStyle.php";
?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
    var cmbApplicationType = document.getElementById("cmbApplicationType");
    var cmbApplicants = document.getElementById("cmbApplicants");
     
	//Calls fetchApplications function when the window loads 
    document.onload = fetchApplications();

	//Fetches application whenever a new username is selected
    cmbApplicants.addEventListener("change", function() {
        fetchApplication();
	});
       
	//Fetches application whenever a new application type is selected	   
    cmbApplicationType.addEventListener("change", function() {
        fetchApplications();
    });
           
	//Fetches the usernames of people who have applied to come to LIV that match the criteria (application type)
    function fetchApplications(){
		displayLoader();
        var applicationType = cmbApplicationType.options[cmbApplicationType.selectedIndex].innerHTML;
        $.ajax({
            url: window.location.pathname,
            type: "post",
            data: {"applicationType" : applicationType, "ajaxResponse" : "1"},
            success: function(response){
                cmbApplicants.innerHTML = response;
				hideLoader();
                fetchApplication();
            }
        });
    }

	//Function displays the selected username's application information
	function fetchApplication(){
		displayLoader();
		var applicantUsername = cmbApplicants.options[cmbApplicants.selectedIndex].innerHTML;
        var userType = $(cmbApplicationType).val();
        
		//Fetches and displays data
		$.ajax({
			url: window.location.pathname,
   		    type: "post",
			data: {"applicantUsername" : applicantUsername, "userType": userType, "ajaxResponse" : "1"},
			success: function(response){
			    var applicationContent = document.getElementById("applicationContent");
			    applicationContent.innerHTML = response;
				hideLoader();
		    }
	    });
	}

	//Function sends data to the PHP side to send an email to the current applicant telling them to book a Skype interview with the LIV employee who is signed in
	function requestSkypeInterview(){
		if($(cmbApplicants).has('option').length == 0){
			alert('Please select an applicant');
		}	
		else{
			var applicantUsername = cmbApplicants.options[cmbApplicants.selectedIndex].innerHTML;
			var applicationType =  cmbApplicationType.options[cmbApplicationType.selectedIndex].innerHTML;
			
			//Sends data if the user confirms their decision
			if(confirm("Are you sure you want to request a Skype Interview with " + applicantUsername + "?")){
				displayLoader();
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"requestSkypeInterview" : "1", "applicantUsername" : applicantUsername, "applicationType" : applicationType, "ajaxResponse" : "1"},
					success : function(response){
						if(response == 1){
							alert("An email has been sent to the applicant requesting them to book a Skype Interview with you");
							location.reload();
						}
						else{
							alert("An error occured while requesting a Skype Interview, please try again...");
						}
						hideLoader();
					}
				});
			}
		}
	}

	//Function sends data to the PHP side to send an email to the applicant telling them their application has been rejected
	function rejectApplication(){
		if($(cmbApplicants).has('option').length == 0){
			alert('Please select an applicant');
		}	
		else{
			var applicantUsername = cmbApplicants.options[cmbApplicants.selectedIndex].innerHTML;
			var applicationType =  cmbApplicationType.options[cmbApplicationType.selectedIndex].innerHTML;
			var txtReasonForRejection = document.getElementById("txtReasonForRejection");
			var reasonForRejection = $(txtReasonForRejection).val();
			
			//Rejects application if user confirms their decision
			if(confirm("Are you sure you would like to reject " + applicantUsername + "'s application?")){
				displayLoader();
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"rejectApplication" : "1", "applicantUsername" : applicantUsername, "applicationType" : applicationType, "reasonForRejection": reasonForRejection, "ajaxResponse" : "1"},
					success : function(response){
						if(response == 1){
							alert("An email has been sent to the applicant explaining them that you have rejected their application");
							location.reload();
						}
						else{
							alert("An error occured while rejecting the application, please try again...");
						}
						hideLoader();
					}
				});
			}
		}
	}

	//Function sends data to PHP side to send an email to the applicant telling them that their application has been accepted and they may book accommodation at LIV
	function acceptApplication(){
		if($(cmbApplicants).has('option').length == 0){
			alert('Please select an applicant');
		}	
		else{
			var applicantUsername = cmbApplicants.options[cmbApplicants.selectedIndex].innerHTML;
			var applicationType =  cmbApplicationType.options[cmbApplicationType.selectedIndex].innerHTML;
			
			//Accepts applicant if the user confirms their decision
			if(confirm("Are you sure you would like to accept " + applicantUsername + "'s application?")){
				displayLoader();
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"acceptApplication" : "1", "applicantUsername" : applicantUsername, "applicationType" : applicationType, "ajaxResponse" : "1"},
					success : function(response){
						if(response == 1){
							alert("An email has been sent to the applicant explaining them that you have accepted their application");
							location.reload();
						}
						else{
							alert("An error occured while accepting the application, please try again...");
						}
						hideLoader();
					}
				});
			}
		}
	}
</script>
<?php
		}
    }
?>