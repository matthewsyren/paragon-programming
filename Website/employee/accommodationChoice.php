<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
?>
<html>
<style>
	#div1{
		background-color: #f99f18;
		cursor:pointer;
		min-height:200px;
		width: 33%;
        height: auto;
		display: inline-block;
		vertical-align: top;
        border-radius: 20px;
	}
	
	#div2{
		background-color: #0fb5fc;
		cursor:pointer;
		min-height:200px;
		width: 33%;
        height: auto;
		display: inline-block;
		vertical-align: top;
        border-radius: 20px;
	}
	
	#div3{
		background-color: #8cdd3b;
		min-height:200px;
		width: 33%;
        height: auto;
		display: inline-block;
		vertical-align: top;
		cursor:pointer;
        border-radius: 20px;
	}
</style>
	
	<body>
	<?php
        include "styles.css";
        include "navbar.php";
    ?>
		
<div>
<h1>What would you like to do?</h1>
<div onclick="location.href='accommodationAdding.php'; " id='div1'>
	<h1>Add Accommodation</h1>
	<p>Add new accommodation</p>
</div>
</a>
<div onclick="location.href='accommodationManagementChoice.php'; " id='div2'>
	<h1>Manage Accommodation</h1>
	<p>Update existing accommodation or select dates when the accommodation is unavailable</p>
</div>
<div onclick="location.href='accommodationImages.php';" id='div3'>
<h1>Add Accommodation Images</h1>
<p>Upload images of the available accommodation</p>
</div>
</div>
</body>
</html>
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>