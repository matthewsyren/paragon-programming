<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
?>

<html>
	<style>
		#div1{
			background-color: #f99f18;
			cursor:pointer;
			min-height:200px;
			min-width: 48%;
			width: auto;
			display: inline-block;
			margin: auto;
			border-radius: 20px;
			height: auto;
		}
		
		#div2{
			background-color: #0fb5fc;
			cursor:pointer;
			min-height:200px;
			height: auto;
			min-width: 48%;
			width: auto;
			display: inline-block;
			margin: auto;
			float: right;
			border-radius: 20px;
		}
	</style>
	<body>
        <?php
            include "styles.css";
            include "navbar.php";
        ?>
	<div>
		<h1>What would you like to do?</h1>
		<div onclick="location.href='accommodationManagement.php';"id='div1'>
			<h1>Manage Accommodation</h1>
			<p>Update details about existing accommodation</p>
		</div>
		<div onclick="location.href='accommodationAvailabilityManagement.php';" id='div2'>
			<h1>Mark accommodation as unavailable</h1>
			<p>Select dates when your accommodation will be unavailable to stay in</p>
		</div>
	</div>
</body>
</html>
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>	