<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		include "../styles.css";
		include "../sharedFunctions.php";
		include "../dbConn.php";
		include "navbar.php";
		include "livEmployee.php";
			
		$arrDisplay = array('username' => '', 'firstName' => '', 'lastName' => '', 'emailAddress' => '', 'password' => '', 'jobTitle' => '');   
			
		if(isset($_POST["username"]) && isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["emailAddress"]) && isset($_POST["password"]) && isset($_POST["jobTitle"])){
			//Assigns the values that the user entered to variables (formatInput() ensures that the input is safe to use)
			$username = formatInput($_POST["username"]);
			$firstName = formatInput($_POST["firstName"]);
			$lastName = formatInput($_POST["lastName"]);
			$emailAddress = formatInput($_POST["emailAddress"]);
			$password = formatInput($_POST["password"]);
			$jobTitle = formatInput($_POST["jobTitle"]);
								   
			//Sets values to ensure that form input fields don't lose value after submission
			$arrDisplay['username'] = $username;
			$arrDisplay['firstName'] = $firstName;
			$arrDisplay['lastName'] = $lastName;
			$arrDisplay['emailAddress'] = $emailAddress;
			$arrDisplay['password'] = $password;
			$arrDisplay['jobTitle'] = $jobTitle;
								  
			//If statements ensure that the entered information is valid. If any information is not valid, $valid is set to false, and the program prompts the user for valid input
			$valid = true;
			if (empty($username)){
				echo "<p class='error'>Please enter your desired username</p>";
				$valid = false;
			}
			if (empty($emailAddress) || checkEmail($emailAddress) == false){
				echo "<p class='error'>Please enter a valid email address</p>";
				$valid = false;
			}
			if (empty($firstName)){
				echo "<p class='error'>Please enter your first name</p>";
				$valid = false;
			}
			if (empty($lastName)){
				echo "<p class='error'>Please enter your last name</p>";
				$valid = false;
			}
			if (empty($password) || checkPassword($password) == false){
				echo "<script>alert('Please enter a valid password (8 - 12 characters that includes at least 1 uppercase letter and 1 digit');</script>";
				$valid = false;
			}
			if (strlen($username) > 29 ){
				echo "<p class='error'>Username length needs to less than 30 Characters</p>";
				$valid = false;
			}
			if (empty($jobTitle)){
				echo "<p class='error'>Job Title length needs to less than 30 Characters</p>";
				$valid = false;
			}
				   
			//Compares the entered information with the information in the database if all entered information is valid
			if($valid == true){
				$dbConnect = new dbConnect();
				$sql = "select * from tblLivEmployees where Username = '$username'";
				$result = $dbConnect->executeQuery($sql);
						   
				//Outputs error message if the user's desired username is taken
				if($result->num_rows > 0){
					echo "<script>alert('" . $username . " has already been taken, please choose another username');</script>";
				}
				else{
					//Registers user if their information is valid and their email addrress hasn't been used already
					$hashedPassword = hash('sha256', $password);
					$dbConnect = new dbConnect();
					$livEmployee = new livEmployee($username, $firstName, $lastName, $emailAddress, $hashedPassword, $jobTitle);
					$sql = "insert into tblLivEmployees (Username, FirstName, LastName, EmailAddress, Password, JobTitle, ResetPasswordCode) values ('$livEmployee->username', '$livEmployee->firstName', '$livEmployee->lastName', '$livEmployee->emailAddress', '$livEmployee->password', '$livEmployee->jobTitle', '');";                
					echo $sql;
					$success = $dbConnect->executeQuery($sql);
					if($success == true){
						echo "<script>alert('Account has been created successfully!');</script>";
						redirectPage("createAccount.php");
					}
					else{
						echo "<p class='error'>There was an error while creating the account, please try again</p>";
					}
				}                    
			}
		}
?>

<html>
	<body>
		<form name = "registration"  method = "POST">
		<br/><br/>
		<h1>Create account</h1>
        <p><label>Username:</label> <input maxlength="30" required="true" type = "text" name = "username" value="<?php echo $arrDisplay['username']; ?>"/> </p>
		<p><label>First Name:</label> <input maxlength="55" required="true" type = "text" name = "firstName" value="<?php echo $arrDisplay['firstName']; ?>"/> </p>
		<p><label>Last Name:</label> <input maxlength="55" required="true" type = "text" name = "lastName" value="<?php echo $arrDisplay['lastName']; ?>"/> </p>
		<p><label>Email Address:</label> <input maxlength="254" required="true" type = "text" name = "emailAddress" value="<?php echo $arrDisplay['emailAddress']; ?>"/> </p>
		<p><label>Password:</label> <input required="true" type = "password" name = "password" value="<?php echo $arrDisplay['password']; ?>"/> </p>
		<p><label>Job Title:</label><select name="jobTitle"><option>Active</option><option>Viewer</option></select> </p>
		<button type="submit" name = "registration"><strong>Sign up</strong></button> 
		</form>
	</body>
</html>

<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>