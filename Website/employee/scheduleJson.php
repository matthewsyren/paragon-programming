<?php
	session_start();
	include "../dbConn.php";
	
	//Connects to database and assigns posted variables to variables
	$dbConnect = new dbConnect();
	$username = $_POST["username"];
	$usersPassword = $_POST["password"];
	$date = $_POST["selectedDate"];
	
	//Hashes the password provided
	$hashedPassword = hash('sha256', $usersPassword);

	//Compares login details to the details in the database to ensure that it is a valid user
	$checkUsersCredentials = "Select Username,Password FROM tblUsers where Username  = '$username' and Password = '$hashedPassword';";
	$dbUserResults = $dbConnect->executeQuery($checkUsersCredentials);
	if($dbUserResults->num_rows > 0){
		//Outputs user's schedule in JSON format if the login details are valid
        $sqlData = "Select * FROM tblSchedule where Username = '$username' and ScheduleDate = '$date'";
        $dbResults = $dbConnect->executeQuery($sqlData);
        $remArray = array();
        while($row = $dbResults->fetch_assoc()){
            $remArray[] = $row;
        }
        echo json_encode($remArray);
    }
    else{
        echo "Login failed";
    }
?>