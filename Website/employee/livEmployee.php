<?php
	class livEmployee{
		//Attributes
		public $username;
		public $firstName;
		public $lastName;
		public $emailAddress;
		public $password;
		public $jobTitle;
				   
		//Constructor
		function __construct($username, $firstName, $lastName, $emailAddress, $password, $jobTitle){
			$this->username = $username;
			$this->firstName = $firstName;
			$this->lastName= $lastName;
			$this->emailAddress = $emailAddress;
			$this->password= $password;
			$this->jobTitle = $jobTitle;
		}
	}
?>