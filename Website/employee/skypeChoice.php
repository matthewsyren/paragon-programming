<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
?>
<html>
	<style>
		#div1{
			background-color: #f99f18;
			cursor:pointer;
			min-height:200px;
			min-width: 48%;
			width: auto;
			display: inline-block;
			margin: auto;
			border-radius: 20px;
			height: auto;
		}
		
		#div2{
			background-color: #0fb5fc;
			cursor:pointer;
			min-height:200px;
			height: auto;
			min-width: 48%;
			width: auto;
			display: inline-block;
			margin: auto;
			float: right;
			border-radius: 20px;
		}
	</style>
	<body>
        <?php
            include "styles.css";
            include "navbar.php";
        ?>
	<div>
		<h1>What would you like to do?</h1>
		<div onclick="location.href='skypeAvailabilityManagement.php';"id='div1'>
			<h1>Set Skype Availability</h1>
			<p>Select time slots where you will be available to Skype with applicants</p>
		</div>
		<div onclick="location.href='skypeBookingViewer.php';" id='div2'>
			<h1>View upcoming Bookings</h1>
			<p>View your upcoming Skype bookings and change the details of a booking, or cancel a booking</p>
		</div>
	</div>
</body>
</html>
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>