<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		if(isset($_SESSION['LivEmployeeUsername'])){
			include "../dbConn.php";
		}
		if(isset($_POST["dateClicked"])){
			getAccomDetails($_POST["dateClicked"], $_POST["applicationType"], $_POST["accommodationType"]);
		}
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}

	//Function fetches accommodation bookings that match the search criteria for a specific date
	function getAccomDetails($date, $applicationType, $accommodationType){
		$dbConnect = new dbConnect();
		$sql = "select tab.BookingID, tab.Username, tab.AccommodationID, tacc.Type, tbu.EmailAddress, tbu.FirstName, tbu.LastName, ta.ApplicationType from tblAccommodationBookings tab inner join tblUsers tbu on tab.Username = tbu.Username inner join tblApplications ta on tab.Username = ta.Username inner join tblAccommodation tacc on tab.AccommodationID = tacc.AccommodationID where '$date' between CheckInDate and CheckOutDate;";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$count = 0;

			//Outputs data in a tabular format
			while($rows = $result->fetch_assoc()){
				if(($applicationType == "All" || $rows["ApplicationType"] == $applicationType) && ($accommodationType == "All" || $rows["Type"] == $accommodationType)){
					if($count == 0){
						echo "<tr>";
						echo "<th>Booking ID</th>";
						echo "<th>Accomodation</th>";
						echo "<th>Username</th>";
						echo "<th>Name</th>";
						echo "<th>Email Address</th>";
						echo "<th>User Type</th>";
						echo "</tr>";
					}
					
					//Displays the data
					echo "<tr>";
					echo "<td>" . $rows["BookingID"] . "</td>";
					echo "<td>" . $rows["AccommodationID"] . "</td>";
					echo "<td>" . $rows["Username"] . "</td>";
					echo "<td>" . $rows["FirstName"] . ' ' .  $rows["LastName"]  . "</td>";
					echo "<td>" . $rows["EmailAddress"] . "</td>";
					echo "<td>" . $rows["ApplicationType"] . "</td>";
					echo "</tr>";	
					$count++;
				}  
			}
			if($count == 0){
				echo "<center>There are no accommodation bookings for this date</center>";
			}
		}
		else{
			echo "<center>There are no accommodation bookings for this date</center>";
		}	
	}

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		if(!isset($_POST["ajaxResponse"])){
			include "../loaderStyle.php";
?>

<html>
    <h1>Accomodation Bookings</h1>
</html>
<?php
	include "calendar.php";
?>
<html>
	<p><label>Date</label><br/>
	<input type="date" id="cmbDate" name="cmbDate" value="<?php echo date('Y-m-d'); ?>"/></p>
                <p><label>Accommodation</label><br/>
				<select id="cmbAccommodationType">
                        <option>All</option>
                        <option>Barracks</option>
                        <option>Lodge</option>
                </select></p>
				<p><label>Applicant type</label><br/>
                <select id="cmbApplicationType">
                        <option>All</option>
                        <option>Gap-year Student</option>
                        <option>Guest</option>
                        <option>Volunteer</option>
                </select></p>
				<h2>Bookings</h2>
		<table style = "width:100%" id = "accom" class="report">
			
		</table>
	<br>
</html>

<script>
    window.onload = setListeners;
        
    //Function sets listeners that will filter the data whenever the user changes the combo box selection 
    function setListeners(){
        var cmbAccommodationType = document.getElementById("cmbAccommodationType");
        var cmbApplicantType = document.getElementById("cmbApplicationType");
		var cmbDate = document.getElementById("cmbDate");
                
        cmbAccommodationType.addEventListener("change", function() {
			getData();
		});
        
		cmbApplicantType.addEventListener("change", function() {
			getData();
		});
		
		cmbDate.addEventListener("change", function() {
			var firstDate = $(cmbDate).val();
			displaySelectedDate(firstDate);
		});
		
		var firstDate = $(cmbDate).val();
        displaySelectedDate(firstDate);
    }
	
	//Function fetches the data 
	function getData(){
		displayLoader();
		//Element assignments
		var cmdDate = document.getElementById("cmbDate");
		var booking = document.getElementById("accom");
		var cmbAccommodationType = document.getElementById("cmbAccommodationType");
        var cmbApplicantType = document.getElementById("cmbApplicationType");
		
		//Variable assignments
		var date = $(cmbDate).val();
		var acccommodationType = $(cmbAccommodationType).val();
		var applicationType = $(cmbApplicantType).val();
		
		$.ajax({
			url: window.location.pathname,
			type: "post",
			data: {"dateClicked" : date,"applicationType": applicationType, "accommodationType": acccommodationType, "ajaxResponse":"1"},
			success:function(response){
				booking.innerHTML = response;
				hideLoader();
			}
		});
	}
	
	//Function displays the selected date from the calendar in the date picker
	function handleDateClick(date){
		var formattedDate = new Date(date);
		formattedDate = formatDate(formattedDate);
		var cmbDate = document.getElementById("cmbDate");
        $(cmbDate).val(formattedDate);
        displaySelectedDate(formattedDate);
	}
	
	//Function formats the date into yyyy-mm-dd format
	//https://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
	function formatDate(date) {
		var month = '' + (date.getMonth() + 1),
		day = '' + date.getDate(),
		year = date.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	} 
		
	//Function displays the selected date in the calendar	
	function displaySelectedDate(firstDate){
        var table = document.getElementById("table");

		for (var i = 2, row; row = table.rows[i]; i++) {
			for (var j = 0, col; col = row.cells[j]; j++) {
				currentMonth = table.rows[0].cells[1].innerHTML;     
				var colValue = new Date(col.innerHTML + ' ' + currentMonth);
				colValue = formatDate(colValue);
				if(colValue == firstDate && col.innerHTML != ""){
					$(col).addClass('selectedDate');
					$(col).removeClass('unselectedDate');
			    }
                   else{
                    $(col).addClass('unselectedDate');
					$(col).removeClass('selectedDate');
                }
			} 
		}
		
		getData();
    }
</script>
<?php
		}
	}
?>