<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
        if(!isset($_POST["ajaxResponse"])){
            include "navbar.php";
            include "./styles.css";
        }
        include "../dbConn.php";
		include "../sharedFunctions.php";
      
		//Calls the appropriate function based on the posted values
        if(isset($_POST["getAccommodation"])){
            getAccommodation();
            unset($_POST["getAccommodation"]);
        }
        else if(isset($_POST["getAccommodationImages"])){
            getAccommodationImages();
            unset($_POST["getAccommodationImages"]);
        }
        else if(isset($_POST["deleteImage"])){
            deleteImage($_POST["deleteAccommodationID"], $_POST["deleteImageID"]);
            unset($_POST["deleteAccommodationID"]);
            unset($_POST["deleteImageID"]);
			unset($_POST["deleteImage"]);
        }
        else if(isset($_POST["imageDescription"]) && isset($_POST["accommodationID"])){
            uploadImage($_POST["accommodationID"], $_POST["imageDescription"]);
            unset($_POST["accommodationID"]);
            unset($_POST["imageDescription"]);
        }
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
            
        //Function fetches the accommodation IDs from the database and echoes them into a combo box
		function getAccommodation(){
            $sql = "select AccommodationID from tblAccommodation";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
            if($result->num_rows > 0){
                while($row = $result->fetch_assoc()){
                    echo "<option>" . $row["AccommodationID"] . "</option>";
                }
            }
        }
           
		//Function fetches the images that have already been added and shows them in a table
        function getAccommodationImages(){
            $sql = "select * from tblAccommodationImages";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
            if($result->num_rows > 0){
                echo "<tr><th>Accommodation ID</th><th>Image ID</th><th>Image Description</th><th>Image</th><th></th></tr>";
                while($row = $result->fetch_assoc()){
                    echo "<tr><td>" . $row["AccommodationID"] . "</td>";
                    echo "<td>" . $row["ImageID"] . "</td>";
                    echo "<td>" . $row["ImageDescription"] . "</td>";
                    echo "<td> <img src='images/" . $row["ImageID"] . "' height='100' width='100'/></td>";
                    echo "<td><button onclick='deleteImage(this)'>Delete</button></td></tr>";
                }
            }
			else{
				echo "<center>There are currently no accommodation images added</center>";
			}
        }
            
		//Function deletes an image from the database	
        function deleteImage($accommodationID, $imageID){
			//Deletes image from database
            $sql = "delete from tblAccommodationImages where AccommodationID = '$accommodationID' and ImageID = '$imageID'";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
			unset($_POST["accommodationID"]);
            unset($_POST["imageDescription"]);
            echo $result;
        }
            
		//Function inserts an image into the database	
        function insertImage($accommodationID, $imageDescription, $fileName){
			$sql = "select * from tblAccommodationImages where AccommodationID = '$accommodationID' and ImageID = '$fileName'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			
			//Adds image if it hasn't already been added for the accommdoation
			if($result->num_rows > 0){
				echo "<script>alert('You have already added this image for this accommodation.');</script>";
			}
			else{
				$sql = "insert into tblAccommodationImages values ('$accommodationID', '$fileName', '$imageDescription')";
				$dbConnect = new dbConnect();
				$result = $dbConnect->executeQuery($sql);
				if($result == 1){
					echo "<script>alert('Image successfully added');</script>";
					redirectPage($_SERVER['PHP_SELF']);
				}
			}
        }
            
        //Fucntion uploads the image that the user chose to the images folder, and uses the ItemID as the file name
        function uploadImage($accommodationID, $imageDescription){
            //Specifies information regarding the uploading of the file
            $targetfolder = "images/";
            $fileName = $_FILES["fileUpload"]["name"];
            $targetfolder = $targetfolder . basename($fileName);
            $fileType = $_FILES['fileUpload']['type'];
                
            //Returns false if the user hasn't uploaded a file, if an error occurs while uploading the file, or if the user uploads a file that isn't a PNG image file
            if($fileType == null){
                echo "<script>alert('Please upload a PNG or JPEG file to use as an image for the item...');</script>";
                return false;
            }
            else if ($fileType=="image/png" || $fileType=="image/jpeg"){
                if(!move_uploaded_file($_FILES['fileUpload']['tmp_name'], $targetfolder)){
                    echo "<script>alert('There was an error while uploading your image, please try again...');</script>";
                    return false;
                }
                else{
                    insertImage($accommodationID, $imageDescription, $fileName);
                }
            }
            else {
                echo "<script>alert('You may only upload PNG or JPEG images, please choose a different image.');</script>";
                return false;
            }
            return true;
        }
            
		//Ensures the user has logged in before using the page
		if(isset($_SESSION["LivEmployeeUsername"])){	
			if(!isset($_POST["ajaxResponse"])){
				include "../loaderStyle.php";
?>
<html>
        <form name = "accommodationImages" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method = "POST" enctype="multipart/form-data">
        <h1>Accommodation Images</h1>
		<p><label>Image</label>
        <input type="file" id="fileUpload" name="fileUpload" required="true"/></p>
		<p><label>Accommodation ID</label>
        <select id="cmbAccommodationID" name="accommodationID"></select></p>
		<p><label>Description</label>
        <input maxlength="40" type="text" id="txtImageDescription" name="imageDescription" required="true"/></p>
        <button type="submit" >Add</button>
        <br/>
        <table id="tblAccommodationImages" class="report" width="100%"></table>
        </form>
</html>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>	
<script>
		//Calls getAccommodation function when window loads
        window.onload = getAccommodation;
        
		//Function displays accommodation IDs
        function getAccommodation(){
			displayLoader();
            $.ajax({
                url : window.location.pathname,
                type : "post",
                data: {"ajaxResponse":"1", "getAccommodation":"1"},
                success: function(response){
                    var cmbAccommodationID = document.getElementById("cmbAccommodationID");
                    cmbAccommodationID.innerHTML = response;
					hideLoader();
                }                   
            });               
            getAccommodationImages();
        }
        
		//Function displays the images that have already been added
        function getAccommodationImages(){
			displayLoader();
            $.ajax({
                url: window.location.pathname,
                type: "post",
                data: {"getAccommodationImages":"1", "ajaxResponse":"1"},
                success: function(response){
                    var tblAccommodationImages = document.getElementById("tblAccommodationImages");
                    tblAccommodationImages.innerHTML = response;
					hideLoader();
                }
            });
        }
        
		//Function deletes an image from the database
        function deleteImage(element){
			if(confirm("Are you sure you want to delete this image?")){
				displayLoader();
				var table = document.getElementById("tblAccommodationImages");
				var rowNum = element.parentNode.parentNode.rowIndex; 
				var accommodationID = table.rows[rowNum].cells[0].innerHTML;
				var imageID= table.rows[rowNum].cells[1].innerHTML;

				//Sends data to PHP side, where it will be deleted from the database
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"deleteImage": "1", "deleteAccommodationID": accommodationID, "deleteImageID": imageID, "ajaxResponse" : "1"},
					success: function(response){
						if(response == 1){
							alert("Image deleted successfully");
							location.reload();
						}
						else{
							alert("A problem occurred when deleting the image, please try again...");
						}
						hideLoader();
					}
				});
			}
        }
</script>

<?php
		}
    }
?>