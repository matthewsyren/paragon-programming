<?php 
	session_start();
	include "../dbConn.php";
	include "../sharedFunctions.php";
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		//Calls appropriate function if the correct post variables are set
		if(isset($_POST["AccommodationID"]) && isset($_POST["accType"]) && isset($_POST["accDescription"]) && isset($_POST["accCostPerNight"]) && isset($_POST["accCapacity"])){
			AddAccomodation($_POST["AccommodationID"], $_POST["accType"], $_POST["accDescription"] , $_POST["accCostPerNight"], $_POST["accCapacity"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
	//Sets the array to make a sticky form
	$arrDisplay = array('accommodationID' => '', 'accommodationType' => '', 'accommodationDescription' => '', 'accommodationCostPerNight' => '', 'accommodationCapacity' => '');   
	
	//Function inserts accommodation into the database	
	function AddAccomodation($accID, $type, $description, $costPerNight ,$capacity){
		//Saves values to array to make a sticky form
		$arrDisplay['accommodationID'] = formatInput($accID);
		$arrDisplay['accommodationType'] = formatInput($type);
		$arrDisplay['accommodationDescription'] = formatInput($description);
		$arrDisplay['accommodationCostPerNight'] = formatInput($costPerNight);
		$arrDisplay['accommodationCapacity'] = formatInput($capacity);
		
		//Saves array to session variable to ensure it isn't deleted
		$_SESSION["arrAccommodationDisplay"] = $arrDisplay;
		
		//Inserts into the database
		$dbConnect = new dbConnect();
		$sql = "select * from tblAccommodation where AccommodationID = '$accID'";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			//Informs the ajax side of the page that there is already an existing accommodation with the desired accommodation ID
			$result = -1;
		}
		else{
			//Inserts data into the database if the accommodation ID is available
			$sql = "INSERT INTO tblAccommodation ( AccommodationID, Description, Capacity, Type, CostPerNight) VALUES ('$accID','$description', $capacity, '$type', $costPerNight)";
			$result = $dbConnect->executeQuery($sql);
			if($result == 1){
				//Clears form once inserted
				unset($_SESSION["arrAccommodationDisplay"]);
			}
		}
        echo $result;
    }

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		//Includes appropriate files when no ajax response is expected
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "styles.css";
?>

<h1>New Accommodation</h1>
<form id="accommodationForm" method="post">
	<p><label>Accommodation ID</label>
	<input maxlength="20" value="<?php echo $_SESSION["arrAccommodationDisplay"]['accommodationID']; ?>" id="accID" type="text"  required="true"><br></p>
    <p><label>Type</label>
    <select id="cmbType" value="<?php echo $_SESSION["arrAccommodationDisplay"]['accommodationType']; ?>" >
		<option value="Lodge">Lodge</option>
        <option value="Barracks">Barracks</option>
	</select></p>
	<p><label>Description</label>
	<input maxlength="255" value="<?php echo $_SESSION["arrAccommodationDisplay"]['accommodationDescription']; ?>" id="description" type="text" required="true" ></p>
	<p><label>Cost per Night</label>
	<input value="<?php echo$_SESSION["arrAccommodationDisplay"]['accommodationCostPerNight']; ?>" id="costPerNight" type="number" required="true"></p>
	<p><label>Capacity</label>
	<input value="<?php echo $_SESSION["arrAccommodationDisplay"]['accommodationCapacity']; ?>" id="capacity" type="number"  required="true"></p>
    <button onclick="checkForCompleteness()">Add Accomodation</button>
</form>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
	<script>
	
	//Function ensures that all required information has been entered
	function checkForCompleteness(){
		var form = document.getElementById("accommodationForm");
        if($(form)[0].checkValidity()) {
			//Inserts data if all required information has been inserted
            insertAccommodation();
        }
	}
	
	//Function saves the inputted data to the databse
    function insertAccommodation(){
		//Assignments
        var cmbType = document.getElementById("cmbType");
		var type = $(cmbType).val();
		var txtDescription = document.getElementById("description");
        var description = $(txtDescription).val();
		var txtAccID = document.getElementById("accID");
		var AccId = $(txtAccID).val();
		var txtCapacity = document.getElementById("capacity");
		var Capacity = $(txtCapacity).val();  
		var txtCost = document.getElementById("costPerNight");
		var Cost = $(txtCost).val();
			
		//Passes data to the PHP side to insert into the database	
        $.ajax({
            url: window.location.pathname,
			type: "post",
			data: {"AccommodationID" : AccId, "accType": type, "accDescription": description, "accCostPerNight": Cost, "accCapacity" : Capacity, "ajaxResponse": "1"},
			success: function(response){ 
				//Displays appropriate message based on result of adding data to database
				if(response == -1){
					alert(AccId + " has already been used as an Accommodation ID, please choose another one");
				}
				else if (response == 1){
					alert("Accommodation successfully added!");
					location.reload();
				}
			},
			error: function(response){
				alert("Accommodation successfully added!");
				location.reload();
			}
        });
	}
</script>
<?php
		}
	}
?>