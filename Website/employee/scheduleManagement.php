<?php
	session_start();
	include "../dbConn.php";
    include "../schedule.php";
	include "../sharedFunctions.php";
        
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		//Calls the appropriate function based on posted data
		if(isset($_POST["Username"]) && isset($_POST["dateClicked"])){
			getSchedule(($_POST["Username"]), ($_POST["dateClicked"]));
			unset($_POST["dateClicked"]);
			unset($_POST["Username"]);
		}
		else if(isset($_POST["oldDate"]) && isset($_POST["oldTime"]) && isset($_POST["newDate"]) && isset($_POST["newUsername"]) && isset($_POST["newTime"]) && isset($_POST["newDescription"])){
			updateSchedule($_POST["oldDate"], $_POST["oldTime"], $_POST["newUsername"], $_POST["newDate"], $_POST["newTime"], formatInput($_POST["newDescription"]));
		}
		else if(isset($_POST["deleteUsername"])&& isset($_POST["deleteDate"])&& isset($_POST["deleteTime"])&& isset($_POST["deleteDescription"])){
			deleteSchedule($_POST["deleteUsername"],$_POST["deleteDate"],$_POST["deleteTime"],$_POST["deleteDescription"]);
		}
		else if (isset($_POST["Username"]) && isset($_POST["scheduleDate"]) && isset($_POST["scheduleTime"]) && isset($_POST["Description"])){
			makeBooking(($_POST["Username"]), $_POST["scheduleDate"], $_POST["scheduleTime"], formatInput($_POST["Description"]));
			unset($_POST["Username"]);
			unset($_POST["scheduleDate"]);
			unset($_POST["scheduleTime"]);
			unset($_POST["Description"]);
		}
		else if(isset($_POST["populateUsernames"])){
			populateUsernames();
			unset($_POST["populateUsernames"]);
		}
		else if(isset($_POST["selectedUsername"])){
			$_SESSION["selectedUsername"] = $_POST["selectedUsername"];
			unset($_POST["selectedUsername"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
    
	//Function echoes the schedule for the selected date into a table
    function getSchedule($username, $date){  
        $dbConnect = new dbConnect();
        $date = date("Y-m-d", strtotime($date));
        $sql = "SELECT * FROM tblSchedule WHERE Username ='$username' AND ScheduleDate = '$date'";
        $result = $dbConnect->executeQuery($sql);
        if($result->num_rows > 0){
            echo "<tr><th>Time</th><th>Description</th><th></th><th></th></td>";
            while($rows = $result->fetch_assoc()){
                echo "<tr>";
                echo "<td>" . $rows["ScheduleTime"] . "</td>";
                echo "<td>" . $rows["Description"] . "</td>";
                echo "<td><button type='button' onclick='openModal(this)'>Update</button></td>";
                echo "<td><button type='button' onclick='deleteModal(this)'>Delete</button></td>";
                echo "</tr>";
            }
        }
		else{
			echo "<center>There are no schedule entries for this date</center>";
		}
    }

	//Function inserts a schedule slot into the database
	function makeBooking($username, $date, $time, $description){
		$dbConnect = new dbConnect();
		$date = date ("Y-m-d", strtotime($date));
        $schedule = new Schedule($username, $date, $time, $description);
		$sql = "INSERT INTO tblSchedule (Username, ScheduleDate, ScheduleTime, Description) VALUES ('$schedule->username', '$schedule->date', '$schedule->time', '$schedule->description')";
        $result = $dbConnect->executeQuery($sql);
        echo $result;
	}
        
	//Function updates schedule details in the database	
    function updateSchedule($oldDate, $oldTime, $username, $newDate, $newTime, $newDescription){    	
		$dbConnect = new dbConnect();
        $oldDate = date ("Y-m-d", strtotime($oldDate));
        $newDate = date ("Y-m-d", strtotime($newDate));
		$schedule1 = new Schedule($username, $newDate, $newTime, $newDescription);
		$sql = "UPDATE tblSchedule SET ScheduleDate = '$schedule1->date', ScheduleTime ='$schedule1->time', Description = '$schedule1->description' WHERE Username = '$schedule1->username' AND ScheduleDate = '$oldDate' AND ScheduleTime ='$oldTime'";
		$result = $dbConnect->executeQuery($sql);
		echo $result;
	}
       
	//Function deletes a schedule slot from the database
    function deleteSchedule($username, $date, $time, $description){
		$dbConnect = new dbConnect();
        $date = date("Y-m-d", strtotime($date));
		$sql = "DELETE FROM tblSchedule WHERE Username = '$username' AND ScheduleDate = '$date' AND ScheduleTime = '$time' AND Description = '$description';";
		$res = $dbConnect->executeQuery($sql);
		echo $res;
    }
    
	//Fucntion echoes out the usernames of applicants into a <select> element
    function populateUsernames(){
		$dbConnect = new dbConnect();
		$sql = "SELECT Username FROM tblUsers";
		$result = $dbConnect->executeQuery($sql);
	   
		if($result->num_rows > 0){
		    while($row = $result->fetch_assoc()){
				echo "<option>" . $row["Username"] . "</option>";
		    }
	    }
    }
        
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){	
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "../loaderStyle.php";
   ?>
   
   <html>
        <head>
        <?php
            include "styles.css";
			include "../modalStyle.css";
        ?>
    
        </head>
        <body>
			<div id="modalWindow" class="modal">
				<!-- Modal content -->
				<div class="modal-content">
					<div class="modal-header">
					  <span id="close">&times;</span>
					  <h1>Update Schedule</h1>
					</div>
					<div class="modal-body">
						<center><label>New Date</label><br/>
						<input type = "date" id="newDate"></center>
						<p><label>Time</label><br/>
						<select id="cmbUpdateTimes">
		<?php
			}
		  
			//Displays time slots that haven't been used for a schedule slot yet
			if(isset($_POST["getAvailableUpdateTimes"])){
                $date = $_POST["date"];
                $username = $_POST["applicantUsername"];
                unset($_POST["date"]);
                unset($_POST["applicantUsername"]);
                $arrTimes = array("06:00", "06:30","07:00", "07:30","08:00", "08:30","09:00", "09:30","10:00", "10:30","11:00", "11:30","12:00", "12:30","13:00", "13:30","14:00", "14:30","15:00", "15:30","16:00", "16:30","17:00", "17:30","18:00", "18:30","19:00", "19:30","20:00", "20:30","21:00", "21:30");
                $dbConnect = new dbConnect();
                $sql = "select ScheduleTime from tblSchedule where Username = '$username' and ScheduleDate = '$date'";
				$result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
						$time = date("H:i", strtotime($row["ScheduleTime"]));
                        if(($key = array_search($time, $arrTimes)) !== false) {
							//Removes time slots that have been used already
                            unset($arrTimes[$key]);
                        }
					}
                }
				
				//Displays time slots that haven't been used for a schedule slot already
                for($i = 0; $i < sizeof($arrTimes); $i++){
                    if(isset($arrTimes[$i])){
                        echo "<option value = '$arrTimes[$i]'>$arrTimes[$i]</option>";
                    }
                }
            }
            
            if(!isset($_POST["ajaxResponse"])){
            ?>
					</select></p>
					<form id="scheduleUpdateForm">
					<p><label>Description</label>
						<input maxlength="255" id="cmbUpdateDescription" type="text" size="50" required="true"><br></p>
					</form>
					</div>
					
					<div class="modal-footer">
						<button onclick="updateSchedule(this)">Update schedule</button>
					</div>
				</div>
			</div>
			<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
			<h1>Schedule Management</h1>
   <?php 
       include "calendar.php";
    ?>
    <script>
		//Calls populateUsernames function when the window loads
        window.onload = populateUsernames;
        
        var oldDate, oldTime, oldDescription;
        
		//Function sets listeners that perform specific tasks when a specific event occurs
        function setListeners(){
            var cmbDate = document.getElementById("cmbDate");
            var cmbUsernames = document.getElementById("cmbUsernames");
            var newDate = document.getElementById("newDate");
			
			//Fetches schedule and available times when the date is changed
            cmbDate.addEventListener("change", function() {
				var firstDate = $(cmbDate).val();
                var username = $(cmbUsernames).val();
				displaySelectedDate(firstDate);
                getSchedule(username, firstDate);
                getAvailableTimes(username, firstDate);
			});
              
			//Fetches schedule and available times when the username is changed
            cmbUsernames.addEventListener("change", function() {
				var firstDate = $(cmbDate).val();
				var username = $(cmbUsernames).val();
                getSchedule(username, firstDate);
                getAvailableTimes(username, firstDate);
				
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"selectedUsername": username},
					success: function(response){
						
					}
				});
			});
            
			//Displays availabe times when the modal date is changed
			newDate.addEventListener("change", function() {
				var firstDate = $(cmbDate).val();
				var username = $(newDate).val();
                getAvailableUpdateTimes(username, firstDate);
			});

            var firstDate = $(cmbDate).val();
            displaySelectedDate(firstDate);
		}
        
        //Function fetches the time slots that haven't been used for a schedule slot yet
        function getAvailableTimes(username, date){
			displayLoader();
            $.ajax({
                url: window.location.pathname,
                type: "post",
                data: {"getAvailableTimes": "1", "date": date, "applicantUsername": username, "ajaxResponse": "1"},
                success: function(response){
                    var cmbTimes = document.getElementById("cmbTimes");
                    cmbTimes.innerHTML = response;
					hideLoader();
                }
            });
        }
		
		//Function fetches the available times that haven't been used for a schedule slot yet
		function getAvailableUpdateTimes(username, date){
			displayLoader();
            $.ajax({
                url: window.location.pathname,
                type: "post",
                data: {"getAvailableTimes": "1", "date": date, "applicantUsername": username, "ajaxResponse": "1"},
                success: function(response){
                    var cmbTimes = document.getElementById("cmbUpdateTimes");
                    cmbTimes.innerHTML = response;
					hideLoader();
                }
            });
        }
               
		//Function displays the selected date in the calendar	   
        function displaySelectedDate(firstDate){
            var table = document.getElementById("table");

			for (var i = 2, row; row = table.rows[i]; i++) {
				for (var j = 0, col; col = row.cells[j]; j++) {
					currentMonth = table.rows[0].cells[1].innerHTML;     
					var colValue = new Date(col.innerHTML + ' ' + currentMonth);
					colValue = formatDate(colValue);
					if(colValue == firstDate && col.innerHTML != ""){
						$(col).addClass('selectedDate');
						$(col).removeClass('unselectedDate');
				    }
                    else{
                        $(col).addClass('unselectedDate');
						$(col).removeClass('selectedDate');
                    }
				} 
			}
        }
     
		//Function displays a list of usernames that the employee can set a schedule for
		function populateUsernames(){
			displayLoader();
            var cmbUsernames = document.getElementById("cmbUsernames");
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"populateUsernames" : "1", "ajaxResponse": "1"}, 
				success: function(response){
					cmbUsernames.innerHTML = response;
					
					//Picks up the last username that the employee was working on and displays it
					var selectedUsername = '<?php echo $_SESSION["selectedUsername"]; ?>';
					if(selectedUsername != ''){
						$(cmbUsernames).val(selectedUsername);
					}
					
                    var cmbDate = document.getElementById("cmbDate");                       
                    var firstDate = $(cmbDate).val();
                    var username = $(cmbUsernames).val();
					hideLoader();
					getSchedule(username, firstDate);
                    getAvailableTimes(username, firstDate);
				}
			});
            setListeners();
		}
        
		//Function fetches the schedule that has been set for the current applicant
        function getSchedule(Username, date){
			displayLoader();
            $.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"Username" : Username, "dateClicked" : date, "ajaxResponse": "1"},
				success: function(response){ 
					tblSchedule.innerHTML = response;
					hideLoader();
				}
			});
        }
    
		//Displays the selected date in the calendar in the date picker
		function handleDateClick(date){
			var tblSchedule = document.getElementById("tblSchedule");
			var cmbUsernames = document.getElementById("cmbUsernames");
			var Username = cmbUsernames.options[cmbUsernames.selectedIndex].text;
            var formattedDate = new Date(date);
            var cmbDate = document.getElementById("cmbDate");
            formattedDate = formatDate(formattedDate);
            $(cmbDate).val(formattedDate);
            displaySelectedDate(formattedDate);
			
			getSchedule(Username, formattedDate);
            getAvailableTimes(Username, formattedDate);
		}  
		
		//Function inserts a schedule slot into the database
		function makeBooking(element){
			var form = document.getElementById("scheduleForm");
			
			//Inserts the data if the data is valid
			if($(form)[0].checkValidity()) {
				var cmbUsernames = document.getElementById("cmbUsernames");
				var cmbTimes = document.getElementById("cmbTimes");
				if($(cmbUsernames).has('option').length == 0 || $(cmbTimes).has('option').length == 0){
					alert('Please select an applicant and a time');
				}	
				else{
					var table = document.getElementById("table");
					var descriptionField = document.getElementById("description");	
					
					if(cmbUsernames.options.length == 0 || cmbTimes.options.length == 0){
						alert("Please make sure you have selected a username, as well as a time");
					}
					else{
						displayLoader();
						var cmbDate = document.getElementById("cmbDate");
						var date = $(cmbDate).val();
						var time = cmbTimes.options[cmbTimes.selectedIndex].text;
						var description = descriptionField.value;
						var cmbUsernames = document.getElementById("cmbUsernames");
						var Username = cmbUsernames.options[cmbUsernames.selectedIndex].text;
						
						//Sends the data to PHP
						$.ajax({
							url: window.location.pathname,
							type: "post",
							data: {"Username" : Username, "scheduleDate" : date, "scheduleTime" : time, "Description" : description, "ajaxResponse": "1"}, 
							success: function(response){  
								if(response == 1){
									alert("Schedule Updated");
									getSchedule(Username, date);
								}
								else{
									alert("An error occured while updating the schedule, please try again");
								}
								hideLoader();
							},
							error: function(response){
								alert("Schedule Updated");
								getSchedule(Username, date);
							}
						});
					}
				}
			}
		}
                
        //Modal
        var modal = document.getElementById('modalWindow');
               
        //Close button
        var closeButton = document.getElementById("close");
               
        var globalUsername;
        
        //Function opens the modal
        function openModal(element) {
            modal.style.display = "block"; 
        
			var cmbUsernames = document.getElementById("cmbUsernames");
			globalUsername = cmbUsernames.options[cmbUsernames.selectedIndex].text; 
			var dtePicker = document.getElementById("newDate");		  
			var cmbUpdateTimes = document.getElementById("cmbUpdateTimes");			
			var cmbUpdateDescription = document.getElementById("cmbUpdateDescription");              
			var rowNum = element.parentNode.parentNode.rowIndex; 
			var tblSchedule = document.getElementById("tblSchedule");
			var description = tblSchedule.rows[rowNum].cells[1].innerHTML;
			var time = tblSchedule.rows[rowNum].cells[0].innerHTML;
			oldTime = time;
			oldDescription = description;
        
			var table = document.getElementById("table");
			var cmbDate = document.getElementById("cmbDate");
			var date = $(cmbDate).val();
			oldDate = date;
            date = new Date(date);
			date = formatDate(date);
			$(dtePicker).val(date);
			$(cmbUpdateDescription).val(oldDescription);
			
			//Fetches the time slots that are available to use (haven't been used for a schedule yet)
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"getAvailableUpdateTimes":"1", "date": date, "applicantUsername": globalUsername, "ajaxResponse": "1"},
				success: function(response){
							var cmbUpdateTimes = document.getElementById("cmbUpdateTimes");
							cmbUpdateTimes.innerHTML = response;
						}
				
			});
        }
               
        //Closes the modal when the user clicks on the close button
        closeButton.onclick = function() {
            modal.style.display = "none";
        }
                
        //Closes the modal when the user clicks outside of the modal
		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
			}
        }
        
		//Function updates a schedule slot
        function updateSchedule(element){
			var form = document.getElementById("scheduleUpdateForm");
			
			//Updates the schedule slot if the data is valid
			if($(form)[0].checkValidity()) {
				var cmbUpdateTimes = document.getElementById("cmbUpdateTimes");
				if($(cmbUpdateTimes).has('option').length == 0){
					alert('Please select a time');
				}	
				else{
					var cmbUpdateDescription = document.getElementById("cmbUpdateDescription");
					var datePicker = document.getElementById("newDate");
					var date = $(datePicker).val();			
					var time = cmbUpdateTimes.options[cmbUpdateTimes.selectedIndex].value;
					var upDesc = cmbUpdateDescription.value;
					if(cmbUpdateTimes.options.length == 0){
						alert("Please ensure that you have selected a time for the Schedule, as well as a Username.");
					}
					else{
						displayLoader();
						
						//Sends the data to PHP
						$.ajax({
							url: window.location.pathname,
							type: "post",
							data: {"oldDate": oldDate, "oldTime": oldTime, "newDate": date, "newTime": time, "newUsername": globalUsername, "newDescription": upDesc, "ajaxResponse": "1"},
							success: function(response){
								if(response == 1){
									alert("Update successful");
									location.reload();
								}
								else{
									alert("An error occured while updating your schedule, please try again");
								}
								hideLoader();
							}	
						});
					}
				}
			}
			else{
				alert("Please enter a value into all fields");
			}
		}

		//Function deletes a schedule slot from the database
		function deleteModal(element){
			//Deletes the schedule slot if the user confirms their decision
			if(confirm("Are you sure you would like to delete this schedule slot?")){
				displayLoader();
				var table = document.getElementById("tblSchedule");
				var calendar = document.getElementById("table");
				var cmbUsernames = document.getElementById("cmbUsernames");
				var upUser = cmbUsernames.options[cmbUsernames.selectedIndex].value;        
				var selectedDate = document.getElementsByClassName("selectedDate");
				var currentMonth = calendar.rows[0].cells[1].innerHTML;
				var date = selectedDate[0].innerHTML + " " + currentMonth;
				var rwNum = element.parentNode.parentNode.rowIndex; 
				var getTime = table.rows[rwNum].cells[0].innerHTML;
				var getDescription = table.rows[rwNum].cells[1].innerHTML;
				
				//Sends the data to PHP
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"deleteUsername":upUser, "deleteDate":date, "deleteTime":getTime,"deleteDescription":getDescription, "ajaxResponse": "1"},
					success: function(response){
						if(response == 1){
							alert("Schedule deleted successfully");
							location.reload();
						}
						else{
							alert("There was an error while trying to delete your schedule, please try again...");
						}
						hideLoader();
					}
				});
			}
		}

		//Formats the data in yyyy-mm-dd format
		//https://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
		function formatDate(date) {
			var month = '' + (date.getMonth() + 1),
			day = '' + date.getDate(),
			year = date.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('-');
		}            
    </script>
	<html>
    <br>
	<p><label>Username</label><br/>
        
    <select id="cmbUsernames">
    </select></p>
    <p><label>Date</label><br/>
    <input type="date" id="cmbDate" name="cmbDate" value="<?php echo date('Y-m-d'); ?>"/></p>
    <p><label>Times</label><br/>
    <select id="cmbTimes">
		<?php
          }
		  
		  //Displays the time slots that haven't already been used for a schedule slot
          if(isset($_POST["getAvailableTimes"])){
                $date = $_POST["date"];
                $username = $_POST["applicantUsername"];
                unset($_POST["date"]);
                unset($_POST["applicantUsername"]);
                $dbConnect = new dbConnect();
				$arrTimes = array("06:00", "06:30","07:00", "07:30","08:00", "08:30","09:00", "09:30","10:00", "10:30","11:00", "11:30","12:00", "12:30","13:00", "13:30","14:00", "14:30","15:00", "15:30","16:00", "16:30","17:00", "17:30","18:00", "18:30","19:00", "19:30","20:00", "20:30","21:00", "21:30");
                $sql = "select ScheduleTime from tblSchedule where Username = '$username' and ScheduleDate = '$date'";
                $result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
						$time = date("H:i", strtotime($row["ScheduleTime"]));
                        if(($key = array_search($time, $arrTimes)) !== false) {
							//Removes time slot that has been used already
                            unset($arrTimes[$key]);
                        }
					}
                }
				//Displays available time slots that haven't been used in a schedule slot yet
                for($i = 0; $i < sizeof($arrTimes); $i++){
                    if(isset($arrTimes[$i])){
                        echo "<option value = '$arrTimes[$i]'>$arrTimes[$i]</option>";
                    }
                }
            }
            
            if(!isset($_POST["ajaxResponse"])){
            ?>
	</select><p>
	<form id="scheduleForm">
	<p><label>Description</label>
	<input maxlength="255" id="description" type="text" size="50" required="true"></p>
    <button onclick="makeBooking(this)">Add</button>
	</form>
    <br>
	<h2>Schedule</h2>
    <table style="width:100%" id="tblSchedule" class="report"></table>
</html>
<?php
		}
	}
?>