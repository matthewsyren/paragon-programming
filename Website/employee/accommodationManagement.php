<?php
	session_start();
	include "../dbConn.php";
	include "../sharedFunctions.php";
        
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "styles.css";
			include "../modalStyle.css";
		}

		//Calls appropriate function based on posted values
		if(isset($_POST["Accommodations"])){
			getAccommodation();	
			unset($_POST["Accommodations"]);	
		}
		if(isset($_POST["accommodationID"])&& isset($_POST["description"]) && isset($_POST["type"]) && isset($_POST["capacity"])&& isset($_POST["costPerNight"])){
			updateAccomodation(formatInput($_POST["accommodationID"]), formatInput($_POST["description"]), formatInput($_POST["type"]), formatInput($_POST["capacity"]), formatInput($_POST["costPerNight"]));
			unset($_POST["accommodationID"]);
			unset($_POST["description"]);
			unset($_POST["type"]);
			unset($_POST["capacity"]);
			unset($_POST["costPerNight"]);
		}
		if(isset($_POST["deleteAccID"])){
			deleteAccomodation($_POST["deleteAccID"]);
			unset($_POST["deleteAccID"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
	//Function updates accommodation details in the database
	function updateAccomodation($AccID, $Description, $Type, $Capacity, $CostPerNight){
		$dbConnect = new dbConnect();
		$sql = "update tblAccommodation set AccommodationID = '$AccID',  Description = '$Description', Capacity = '$Capacity',  Type= '$Type' , CostPerNight ='$CostPerNight'  where AccommodationID = '$AccID'";
		$result = $dbConnect->executeQuery($sql);
		echo $result;
	}
	
	//Function deletes accommodation from the database
	function deleteAccomodation($AccID){
		$dbConnect = new dbConnect();
		$sql = "delete from tblAccommodation where AccommodationID = '$AccID'";
		$res = $dbConnect->executeQuery($sql);
		echo $res;
	}
	
	//Function fetches accommodation details from the database
	function getAccommodation(){
		$dbConnect = new dbConnect();
		$sql = "select AccommodationID, Description, Capacity, Type, CostPerNight from tblAccommodation";
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			echo "<tr>";
			echo "<th>Accommodation ID</th>";
			echo "<th>Description</th>";
			echo "<th>Capacity</th>";
			echo "<th>Type</th>";
			echo "<th>Cost Per Night</th>";
			echo "<th></th><th></th>";
			echo "</tr>";

			//Echoes the data into a table
			while($rows = $result->fetch_assoc()){
				echo "<tr>";
				echo "<td>" . $rows["AccommodationID"] . "</td>";
				echo "<td>" . $rows["Description"] . "</td>";
				echo "<td>" . $rows["Capacity"] . "</td>";
				echo "<td>" . $rows["Type"] . "</td>";
				echo "<td>" . $rows["CostPerNight"] . "</td>";
				echo "<td><button type='button' onclick='openModal(this)'>Update</button></td>";
				echo "<td><button type='button' onclick='modalDelete(this)'>Delete</button></td>";
				echo "</tr>";
			}
		}
		else{
			echo "<center>There is currently no accommodation information stored</center>";
		}
	}
    
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
        if(!isset($_POST["ajaxResponse"])){
			include "../loaderStyle.php";
	?>
        <html>
            <h1>Accommodation</h1>
            <br>
            <table id="tblAccommodation" class="report" width="100%"></table>
                
            <div id="modalWindow" class="modal">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header">
					<span id="close">&times;</span>
					<h1>Update Accommodation</h1>
				</div>
				<div class="modal-body">
				<form id="accommodationForm">
					<p><label>Accomodation ID</label>
					<input id="accID" type="text"  required="true" readonly/></p>
					<p><label>Type</label>
					<select id="cmbType">
						<option value="Lodge">Lodge</option>
						<option value="Barracks">Barracks</option>
					</select></p>
					<p><label>Description</label>
					<input id="description" type="text" required="true" /></p>
					<p><label>Cost per Night</label>
					<input id="costPerNight" type="number" required="true"/></p>
					<p><label>Capacity</label>
					<input id="capacity" type="number"  required="true"/></p>
				</form>
			</div>
		<div class="modal-footer">
			<button onclick="checkForCompleteness()"  type='submit'>Update Accomodation</button>
		</div>
		</div>
		</div>
        </html>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
        <script>
			//Calls getAccommodation function when window loads
            window.onload = getAccommodation;
                
			//Function displays accommodation information	
            function getAccommodation(){
				displayLoader();
                $.ajax({
                    url: window.location.pathname,
                    type: "post",
                    data: {"Accommodations": "1", "ajaxResponse":"1"},
                    success: function(response){
                        var tblAccommodation = document.getElementById("tblAccommodation");
                        tblAccommodation.innerHTML = response;
						hideLoader();
                    }
                });
            }
                
            //Modal
			var modal = document.getElementById('modalWindow');
				   
			//Button that closes the modal
			var closeButton = document.getElementById("close");
					
			//Function used to open the modal
			function openModal(element) {
				modal.style.display = "block"; 
			   
				//Assignments
				var rowNum = element.parentNode.parentNode.rowIndex; 
				var tblAccommodation = document.getElementById("tblAccommodation");
				var accommodationID = tblAccommodation.rows[rowNum].cells[0].innerHTML;
				var description = tblAccommodation.rows[rowNum].cells[1].innerHTML;
				var capacity = tblAccommodation.rows[rowNum].cells[2].innerHTML;
				var type = tblAccommodation.rows[rowNum].cells[3].innerHTML;
				var costPerNight = tblAccommodation.rows[rowNum].cells[4].innerHTML;
				var txtAccommodationID = document.getElementById("accID");
				var txtDescription = document.getElementById("description");
				var cmbType = document.getElementById("cmbType");
				var txtCapacity = document.getElementById("capacity");
				var txtCostPerNight = document.getElementById("costPerNight");
				
				//Displays existing data in modal (pre-populates elements)
				$(txtAccommodationID).val(accommodationID);
				$(txtDescription).val(description);
				$(txtCapacity).val(capacity);
				$(txtCostPerNight).val(costPerNight);
				$(cmbType).val(type);
			}
                
            //Closes modal when the button is clicked
			closeButton.onclick = function() {
				modal.style.display = "none";
			}
                
			//Closes modal when the user clicks outside of it
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			}
			
			//Function calls update function if all entered details are valid
			function checkForCompleteness(){
				var form = document.getElementById("accommodationForm");
                if($(form)[0].checkValidity()) {
                    updateAccommodation();
                }
				else{
					alert("Please enter a value into all fields");
				}
			}
			
			//Function sends accommodation information to PHP side to get updated in the database
			function updateAccommodation(){
				displayLoader();
                var txtAccommodationID = document.getElementById("accID");
                var txtDescription = document.getElementById("description");
                var cmbType = document.getElementById("cmbType");
                var txtCostPerNight = document.getElementById("costPerNight");
                var txtCapacity = document.getElementById("capacity");
                                
				var accommodationID = $(txtAccommodationID).val();
				var description = $(txtDescription).val();
				var capacity = $(txtCapacity).val();
				var type = $(cmbType).val();
				var costPerNight = $(txtCostPerNight).val();
                
				//Sends data to PHP 
				$.ajax({
                    url: window.location.pathname,
                    type: "post",
                    data: {"accommodationID" : accommodationID,"description": description ,"capacity": capacity, "type" : type, "costPerNight": costPerNight, "ajaxResponse": "1"},
                    success: function(response){
								if(response == 1){
                                    alert("Update successful");
                                    location.reload();
                                }
                                else{
                                    alert("An error occured while updating your accommodation, please try again");
                                }
								hideLoader();
                            }	
                }); 
            }
				
			//Function deletes data from the database	
			function modalDelete(element){
				if(confirm("Are you sure you want to delete this accommodation?")){
					displayLoader();
					var rowNum = element.parentNode.parentNode.rowIndex; 
					var tblAccommodation = document.getElementById("tblAccommodation");
					var accommodationID = tblAccommodation.rows[rowNum].cells[0].innerHTML;
					
					//Sends data to PHP side to be deleted
					$.ajax({
						url: window.location.pathname,
						type: "post",
						data: {"deleteAccID":accommodationID, "ajaxResponse": "1"},
						success: function(response){
							if(response == 1){
								alert("Accommodation deleted successfully");
								location.reload();
							}
							else{
								alert("There was an error while trying to delete the accommodation, please try again...");
							}
							hideLoader();
						}
					});
				}
			}
        </script>
<?php
		}
	}
?>