<?php 
	session_start();
	include "../dbConn.php";
	include "../sharedFunctions.php";

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		//Calls appropriate function based on posted values
		if(isset($_POST["reminderDate"]) && isset($_POST["reminderTime"]) && isset($_POST["reminderDescription"])){
			makeReminder($_POST["reminderDate"], $_POST["reminderTime"], formatInput($_POST["reminderDescription"]));
		}
		else if(isset($_POST["getReminders"])){
			getReminderDetails($_POST["selectedDate"]);
			unset($_POST["getReminders"]);
			unset($_POST["selectedDate"]);
		}
		if(isset($_POST["oldDate"])&& isset($_POST["oldTime"]) && isset($_POST["oldLivEmployee"]) && isset($_POST["newDate"])&& isset($_POST["newTime"]) && isset($_POST["newLivEmployee"])){
			updateReminders($_POST["oldDate"], $_POST["oldTime"], $_POST["oldLivEmployee"], $_POST["newDate"], $_POST["newTime"], formatInput($_POST["newLivEmployee"]));
			unset($_POST["bookingDate"]);
		}
		if(isset($_POST["deleteDate"])&& isset($_POST["deleteDescription"]) && isset($_POST["deleteTime"])){
			deleteReminders($_POST["deleteDate"],$_POST["deleteDescription"],$_POST["deleteTime"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}

	//Function writes a reminder to the database
	function makeReminder($date, $time, $description){
        $username = $_SESSION["LivEmployeeUsername"];
		$dbConnect = new dbConnect();
		$date = date ("Y-m-d", strtotime($date));
		$sql = "INSERT INTO tblReminders (LivEmployee, ReminderDate, ReminderTime, Description) VALUES ('$username', '$date', '$time', '$description')";
		$result = $dbConnect->executeQuery($sql);
        echo $result;
    }
         
	//Function updates a reminder's details in the database
    function updateReminders($oldDate, $oldTime, $oldLivEmployee, $newDate, $newTime, $newLivEmployee){
		$username = $_SESSION["LivEmployeeUsername"];
		$dbConnect = new dbConnect();
		$sql = "update tblReminders set ReminderDate  = '$newDate', ReminderTime = '$newTime',Description = '$newLivEmployee' 
                where ReminderDate  = '$oldDate' AND ReminderTime  = '$oldTime' AND Description  = '$oldLivEmployee';";
		$res = $dbConnect->executeQuery($sql);
		echo $res;
	}

	//Function deletes a reminder from the databse
	function deleteReminders($tblDate,$livEmp,$skypeTme){
		$dbConnect = new dbConnect();
		$sql = "Delete from tblReminders
                where ReminderDate = '$tblDate' and Description = '$livEmp' and  ReminderTime = '$skypeTme' ;";
		$res = $dbConnect->executeQuery($sql);
		echo $res;
	}

	//Function echoes reminder details into a table
	function getReminderDetails($date){
		$dbConnect = new dbConnect();
		$username = $_SESSION['LivEmployeeUsername'];
		$sql = "select ReminderDate, ReminderTime, Description from tblReminders where LIVEmployee = '$username' and ReminderDate = '$date';";
		$result = $dbConnect->executeQuery($sql);

		if($result->num_rows > 0){
			echo "<tr>";
			echo "<th>Date</th>";
			echo "<th>Time</th>";
			echo "<th>Description</th>";
			echo "<th></th><th></th>";
			echo "</tr>";

			while($rows = $result->fetch_assoc()){
				echo "<tr>";
				echo "<td>" . $rows["ReminderDate"] . "</td>";
				echo "<td>" . $rows["ReminderTime"] . "</td>";
				echo "<td>" . $rows["Description"] . "</td>";
				echo "<td><button type='button' onclick='openModal(this)'>Update</button></td>";
				echo "<td><button type='button' onclick='modalDelete(this)'>Delete</button></td>";
				echo "</tr>";
			}
		}
		else{
			echo "<center>There are no reminders set for this date</center>";
		}
	}

//Ensures the user has logged in before using the page
if(isset($_SESSION["LivEmployeeUsername"])){	
	if(!isset($_POST["ajaxResponse"])){
		include "navbar.php";
		include "../loaderStyle.php";
?>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta content="en-za" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />


	<?php
		include "../styles.css";
		include "../modalStyle.css";
	?>
<br/><br/>
<h1>Reminder management</h1>
</head>

<body>
<?php
	include "calendar.php";
?>

    <br>
    <p><label>Date</label><br/>
    <input id="cmbDate" type="date" value="<?php echo date('Y-m-d'); ?>"/></p>
    <p><label>Time</label><br/>
    <select id="cmbTimes">
		<?php
          }
		  
		  //Displays available time slots, and hides time slots that have been used already
          if(isset($_POST["getAvailableTimes"])){
                $date = $_POST["date"];
                $username = $_SESSION["LivEmployeeUsername"];
                unset($_POST["date"]);
                
                $arrTimes = array("06:00", "06:30","07:00", "07:30","08:00", "08:30","09:00", "09:30","10:00", "10:30","11:00", "11:30","12:00", "12:30","13:00", "13:30","14:00", "14:30","15:00", "15:30","16:00", "16:30","17:00", "17:30","18:00", "18:30","19:00", "19:30","20:00", "20:30","21:00", "21:30");
                    
                $dbConnect = new dbConnect();
                $sql = "select ReminderTime from tblReminders where LivEmployee = '$username' and ReminderDate = '$date'";
                $result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
						$time = date("H:i", strtotime($row["ReminderTime"]));
                        if(($key = array_search($time, $arrTimes)) !== false) {
							//Removes time slot if it has been used for a reminder already
                            unset($arrTimes[$key]);
                        }
					}
                }
				
				//Echoes out time slots into a <select> element
                for($i = 0; $i < sizeof($arrTimes); $i++){
                    if(isset($arrTimes[$i])){
                        echo "<option value = '$arrTimes[$i]'>$arrTimes[$i]</option>";
                    }
                }
            }
            
            if(!isset($_POST["ajaxResponse"])){
            ?>
	</select></p>
    <form id="reminderForm">    
	<p><label>Description</label><br/>
	<input maxlength="255" id="description" type="text" size="50" required="true"><br></p>
    <button onclick="makeReminder(this)">Add Reminder</button>
	</form>
    <br/>
    
    <!-- The Modal -->
			<div id="modalWindow" class="modal">
				<!-- Modal content -->
				<div class="modal-content">
					<div class="modal-header">
					<span id="close">&times;</span>
					<h1>Update Reminders</h1>
				</div>
				<div class="modal-body">
					<center><label>Date</label><br/>
					<input type = "date" id="newDate"></center>
					<p><label>Time</label><br/>
					<select id="cmbUpdateTimes">
        <?php
          }
		  //Displays available time slots in the modal window, and hides time slots that have been used already
          if(isset($_POST["getAvailableUpdateTimes"])){
                $date = $_POST["date"];
                $username = $_SESSION["LivEmployeeUsername"];
                unset($_POST["date"]);
                
                $arrTimes = array("06:00", "06:30","07:00", "07:30","08:00", "08:30","09:00", "09:30","10:00", "10:30","11:00", "11:30","12:00", "12:30","13:00", "13:30","14:00", "14:30","15:00", "15:30","16:00", "16:30","17:00", "17:30","18:00", "18:30","19:00", "19:30","20:00", "20:30","21:00", "21:30");
                    
                $dbConnect = new dbConnect();
                $sql = "select ReminderTime from tblReminders where LivEmployee = '$username' and ReminderDate = '$date'";
                $result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
						$time = date("H:i", strtotime($row["ReminderTime"]));
                        if(($key = array_search($time, $arrTimes)) !== false) {
							//Removes time slot if it has been used for a reminder already
                            unset($arrTimes[$key]);
                        }
					}
                }
				
				//Echoes all available time slots into a <select> element
                for($i = 0; $i < sizeof($arrTimes); $i++){
                    if(isset($arrTimes[$i])){
                        echo "<option value = '$arrTimes[$i]'>$arrTimes[$i]</option>";
                    }
                }
            }
            
            if(!isset($_POST["ajaxResponse"])){
            ?>
     </select></p>
					<form id="reminderUpdateForm">
					<center><label>Description</label><br/>
					<textarea maxlength="255" id="cmbDescription" required="true"></textarea></center>
					</form>
					<br>
				</div>
				<div class="modal-footer">
				  <button onclick="updateBooking(this)">Update Reminder</button>
				</div>
			  </div>
			</div>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script>
        var oldDate, oldTime, oldDescription;
        window.onload = setListeners;
        
		//Function sets listeners that will perform a specific task when a certain event occurs
        function setListeners(){
            var cmbDate = document.getElementById("cmbDate");

            cmbDate.addEventListener("change", function() {
				var firstDate = $(cmbDate).val();
				displaySelectedDate(firstDate);
                getAvailableTimes(firstDate);
                getReminders(firstDate);
                                
			});
                        
			var firstDate = $(cmbDate).val();
            displaySelectedDate(firstDate);
            getAvailableTimes(firstDate);
            getReminders(firstDate);
		}
               
		//Function displays reminders that have been set for the selected date
        function getReminders(date){
			displayLoader();
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"getReminders": "1", "selectedDate" : date, "ajaxResponse": "1"},
				success: function(response){ 
					tblReminders.innerHTML = response;
					hideLoader();
				}
			});  
        }
    
		//Function displays the selected date in the calendar
		function displaySelectedDate(firstDate){ 
            var table = document.getElementById("table");
			for (var i = 2, row; row = table.rows[i]; i++) {
				for (var j = 0, col; col = row.cells[j]; j++) {
					currentMonth = table.rows[0].cells[1].innerHTML;     
					var colValue = new Date(col.innerHTML + ' ' + currentMonth);
					colValue = formatDate(colValue);
					if(colValue == firstDate && col.innerHTML != ""){
						$(col).addClass('selectedDate');
						$(col).removeClass('unselectedDate');
				    }
                    else{
                        $(col).addClass('unselectedDate');
						$(col).removeClass('selectedDate');
                    }
				} 
			}
		}
	
		//Function displays the date that was selected in the calendar in the date picker
		function handleDateClick(date){
			var formattedDate = new Date(date);
			formattedDate = formatDate(formattedDate);
			var cmbDate = document.getElementById("cmbDate");
			var tblReminders = document.getElementById("tblReminders");
			$(cmbDate).val(formattedDate);
			displaySelectedDate(formattedDate);
			getAvailableTimes(formattedDate);
			
			//Fetches reminders for the selected date
			getReminders(formattedDate);
		}

		//Function displays the time slots that are still available (haven't been used by other reminders)
		function getAvailableTimes(date){
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"getAvailableTimes": "1", "date": date, "ajaxResponse": "1"},
				success: function(response){
					var cmbTimes = document.getElementById("cmbTimes");
					cmbTimes.innerHTML = response;
				}
			});
		}
    
		//Function adds a reminder to the database by sending the required data to the PHP side
		function makeReminder(element){
			var form = document.getElementById("reminderForm");
			
			//Sets reminder if all data is valid
			if($(form)[0].checkValidity()) {
				var cmbTimes = document.getElementById("cmbTimes");
				if($(cmbTimes).has('option').length == 0){
					alert('Please select a time');
				}	
				else{
					displayLoader();
					var txtDescription = document.getElementById("description");
					var time = $(cmbTimes).val();
					var description = $(txtDescription).val();
					var cmbDate = document.getElementById("cmbDate");
					var date = $(cmbDate).val();
					
					//Sends data to PHP
					$.ajax({
						url: window.location.pathname,
						type: "post",
						data: {"reminderDate" : date, "reminderTime": time, "reminderDescription": description, "ajaxResponse": "1"},
						success: function(response){ 
							if(response == 1){
								alert("Reminder successfully added");
							}
							else{
								alert("A problem occurred while adding the reminder, please try again...");
							}
							hideLoader();
						},
						error: function(response){
							alert("Reminder successfully added");
						}
					});
				}
			}
		}
    
		//Function formats a date into a format suitable for a datepicker (yyyy-mm-dd
		function formatDate(date) {
			var month = '' + (date.getMonth() + 1),
			day = '' + date.getDate(),
			year = date.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('-');
		}
                
            //Modal
			var modal = document.getElementById('modalWindow');

			//Close button for the modal
			var closeButton = document.getElementById("close");


			// When the user clicks the appropriate button, open the modal 
			function openModal(element) {
				modal.style.display = "block";
				var cmbDescription = document.getElementById("cmbDescription");              
				var cmbTimes = document.getElementById("cmbTimes");			
				var dtePicker = document.getElementById("newDate");              
				var rowNum = element.parentNode.parentNode.rowIndex; 
				var table = document.getElementById("tblReminders");
				var date = table.rows[rowNum].cells[0].innerHTML;
				var description = table.rows[rowNum].cells[2].innerHTML;
				var time = table.rows[rowNum].cells[1].innerHTML;
				date = new Date(date);
				date = formatDate(date);
				oldDate = date;
				oldTime = time;
				oldDescription = description;
				$(dtePicker).val(date);
				$(cmbDescription).val(oldDescription);
                getAvailableUpdateTimes(date);
			}


			//Closes the modal when the user clicks on the close button
			closeButton.onclick = function() {
				modal.style.display = "none";
			}

			//Closes the modal when the user clicks outside the modal
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			}

			//Function updates a reminder by sending the data to the PHP side
			function updateBooking(element){
				var form = document.getElementById("reminderUpdateForm");
				
				//Updates reminder if the data is valid
				if($(form)[0].checkValidity()) {
					var cmbDescription = document.getElementById("cmbDescription");		
					var cmbTimes = document.getElementById("cmbUpdateTimes");
					if($(cmbTimes).has('option').length == 0){
						alert('Please select a time');
					}
					else{
						displayLoader();
						var datePicker = document.getElementById("newDate");
						var date = $(datePicker).val();
						var time = cmbTimes.options[cmbTimes.selectedIndex].value;
						var description = $(cmbDescription).val();
						
						//Sends data to PHP
						$.ajax({
							url: window.location.pathname,
							type: "post",
							data: {"oldDate" : oldDate,"oldTime": oldTime ,"oldLivEmployee": oldDescription, 
												"newDate" : date, "newTime": time, "newLivEmployee": description, 
												"ajaxResponse": "1"},
							success: function(response){
								if(response == 1){
									alert("Update successful");
									location.reload();
								}
								else{
									alert("An error occured while updating your reminders, please try again");
								}
								hideLoader();
							}	
						});
					}
				}
				else{
					alert("Please enter a value into all fields");
				}
			}
                         
			//Function deletes the reminder from the database
			function modalDelete(element){
				//Deletes reminder if the user confirms their choice
				if(confirm("Are you sure you would like to delete this reminder?")){
					displayLoader();
					var rwNum = element.parentNode.parentNode.rowIndex; 
					var table = document.getElementById("tblReminders");
					var getDescription = table.rows[rwNum].cells[2].innerHTML;
					var getDate = table.rows[rwNum].cells[0].innerHTML;
					var getReminderTme = table.rows[rwNum].cells[1].innerHTML;
					
					//Sends data to PHP
					$.ajax({
						url: window.location.pathname,
						type: "post",
						data: {"deleteDate":getDate,"deleteDescription":getDescription,"deleteTime":getReminderTme, "ajaxResponse": "1"},
						success: function(response){
							if(response == 1){
								alert("Reminder deleted successfully");
								location.reload();
							}
							else{
								alert("There was an error while trying to delete your reminder, please try again...");
							}
							hideLoader();
						}
					});
				}
			}

			//Function displays time slots that haven't been used for a reminder already
			function getAvailableUpdateTimes(date){
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"getAvailableUpdateTimes": "1", "date": date, "ajaxResponse": "1"},
					success: function(response){
							var cmbUpdateTimes = document.getElementById("cmbUpdateTimes");
							cmbUpdateTimes.innerHTML = response;
					}
				});
			}
</script>
	<h2>Reminders</h2>
	<table id="tblReminders" class="report"></table>
</body>

</html>
<?php
        }
	}
?>