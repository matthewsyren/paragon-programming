<?php
	session_start();
	include "../dbConn.php";
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		//Calls the appropriate function based on posted values
		if(isset($_POST["bookingDate"]) &&isset($_POST["bookingTime"])){
			makeBooking($_POST["bookingDate"],$_POST["bookingTime"] );
			unset($_POST["bookingDate"]);
			unset($_POST["bookingTime"]);
		}
        else if(isset($_POST["getTimes"])){
            getUnavailableTimes($_POST["date"]);
            unset($_POST["date"]);
            unset($_POST["getTimes"]);
        }
        else if(isset($_POST["deleteTime"])){
            deleteTime($_POST["date"], $_POST["time"]);
            unset($_POST["date"]);
            unset($_POST["time"]);
            unset($_POST["deleteTime"]);
        }
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
	//Function writes an available Skype slot to the database
	function makeBooking($date,$bookingTme){
		$dbConnect = new dbConnect();
		$username = $_SESSION['LivEmployeeUsername'];
		$date = date("Y-m-d", strtotime($date));
		$sql = "Insert into tblSkypeTimes (SkypeDate,SkypeTime,LivEmployee,ApplicantUsername)Values ('$date','$bookingTme','$username','');";
		$result = $dbConnect->executeQuery($sql);
		echo $result;
	}
       
	//Function fetches time slots that the LIV employee has marked as available for Skype interviews and echoes them into a table
    function getUnavailableTimes($date){
        $dbConnect = new dbConnect();
        $username = $_SESSION["LivEmployeeUsername"];
        $sql = "select * from tblSkypeTimes where LivEmployee = '$username' and ApplicantUsername = '' and SkypeDate = '$date'";
        $result = $dbConnect->executeQuery($sql);
        if($result->num_rows > 0){
            echo "<tr><th>Date</th><th>Time</th><th></th>";
            while($row = $result->fetch_assoc()){
                echo "<tr><td>" . $row["SkypeDate"] . "</td>";
                echo "<td>" . $row["SkypeTime"] . "</td>";
                echo "<td><button onclick='deleteAvailability(this)'>Delete</button></td></tr>";
            }
        }
		else{
			echo "<center>You haven't set any available slots for this date</center>";
		}
    }
        
	//Function deletes a time slot that the user has marked as available	
    function deleteTime($date, $time){
        $username = $_SESSION["LivEmployeeUsername"];
        $sql = "delete from tblSkypeTimes where LivEmployee = '$username' and SkypeDate = '$date' and SkypeTime = '$time'";
        $dbConnect = new dbConnect();
        $result = $dbConnect->executeQuery($sql);
        echo $result;           
    }

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		if(!isset($_POST["ajaxResponse"])){
		   include "navbar.php";
		   echo "<br>";
		   include "../loaderStyle.php";
	?>
   
    <html>
       <h1>Skype Availability</h1>
	
	<?php
		include "calendar.php";
	?>
	
    <br>
    <p><label>Interview Date</label><br/><input type="date" id="cmbInterviewDate" name="cmbInterviewDate" value="<?php echo date('Y-m-d'); ?>"/> </p>
	<p><label>Time</label><br/>
    <select id='livEmpSkypeTime'>
        <?php
        }	
			//Displays the time slots that the LIV employee can mark as available (slots that haven't been marked as available yet)
			if(isset($_POST["getUnavailableTimes"])){
                $date = $_POST["date"];
                $livEmployee = $_SESSION["LivEmployeeUsername"];
                unset($_POST["date"]);                   
                $arrTimes = array("06:00", "06:30","07:00", "07:30","08:00", "08:30","09:00", "09:30","10:00", "10:30","11:00", "11:30","12:00", "12:30","13:00", "13:30","14:00", "14:30","15:00", "15:30","16:00", "16:30","17:00", "17:30","18:00", "18:30","19:00", "19:30","20:00", "20:30","21:00", "21:30");
                $dbConnect = new dbConnect();
                $sql = "select SkypeTime from tblSkypeTimes where LivEmployee = '$livEmployee' and SkypeDate = '$date'";
                   
                $result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
                        $time = date("H:i", strtotime($row["SkypeTime"]));
                        if(($key = array_search($time, $arrTimes)) !== false) {
							//Removes time that has been used already
                            unset($arrTimes[$key]);
                        }
                    }
                }
				
				//Displays times that haven't been marked as available yet
                for($i = 0; $i < sizeof($arrTimes); $i++){
                    if(isset($arrTimes[$i])){
                        echo "<option value = '$arrTimes[$i]'>$arrTimes[$i]</option>";
                    }
                }
            }
            
            if(!isset($_POST["ajaxResponse"])){
            ?>
    </select></p>
    <button onclick='makeBooking(this)'>Set availability</button>
    <br/>
    <h2>Available times</h2>
    <table id="tblUnavailableTimes" width="100%" class="report"></table>
    </html>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
   <script>
   //Calls setListeners function when the window loads
   window.onload = setListeners;
   
   //Function sets listeners that will perform a specific action when a specific event occurs
   function setListeners(){
		hideLoader();
        var cmbInterviewDate = document.getElementById("cmbInterviewDate");

		//Updates calendar date whenever the user changes the date in the date picker
        cmbInterviewDate.addEventListener("change", function() {
			var firstDate = $(cmbInterviewDate).val();
			displaySelectedDate(firstDate);
		});
                        
        var firstDate = $(cmbInterviewDate).val();
        displaySelectedDate(firstDate);
	}
    
    //Function sets a time slot when the LIV employee is available to have a Skype interview
	function makeBooking(element){
		var cmbTimes = document.getElementById("livEmpSkypeTime");
		
		//Sets the time slot if all data is valid
		if(cmbTimes.options.length == 0){
            alert("Please ensure that you have selected a time for the Skype meeting");
        }
        else{
			displayLoader();
			var cmbInterviewDate = document.getElementById("cmbInterviewDate");
            var date = $(cmbInterviewDate).val();
                        
			var time = cmbTimes.options[cmbTimes.selectedIndex].text;
			
			//Sends data to PHP
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"bookingDate" : date, "bookingTime": time, "ajaxResponse" : "1" },
				success: function(response){
                    if(response == 1){
						alert("Slot made available");
						location.reload();
					}
					else{
						alert("An error occured while processing your request, please try again...");
					}
					hideLoader();
				}
			});
		}
	}
        
    //Fuction populates the cmbTimes combobox with available Skype interview times (which are returned when the ajax post is successful)
    function handleDateClick(date){
		var formattedDate = new Date(date);
        var cmbInterviewDate = document.getElementById("cmbInterviewDate");
        var cmbLivEmployees = document.getElementById("cmbLivEmployees");
        formattedDate = formatDate(formattedDate);
        $(cmbInterviewDate).val(formattedDate);
        displaySelectedDate(formattedDate);
		
		displayLoader();
		var livEmployee = cmbLivEmployees.options[cmbLivEmployees.selectedIndex].text;
        $.ajax({
            url: window.location.pathname,
            type: "post",
            data: {"dateClicked" : date, "livEmployee": livEmployee},
			success: function(response){ 
                cmbTimes.innerHTML = response;
				hideLoader();
            }
        });
    }      
    
    //Function formats a date into a format suitable for a datepicker (yyyy-mm-dd)
	//https://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
    function formatDate(date) {
		var month = '' + (date.getMonth() + 1),
		day = '' + date.getDate(),
		year = date.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	}
                
    //Function displays selected date in the calendar            
    function displaySelectedDate(firstDate){         
        var table = document.getElementById("table");

		for (var i = 2, row; row = table.rows[i]; i++) {
			for (var j = 0, col; col = row.cells[j]; j++) {
				currentMonth = table.rows[0].cells[1].innerHTML;   
				var colValue = new Date(col.innerHTML + ' ' + currentMonth);
				colValue = formatDate(colValue);
				if(colValue == firstDate && col.innerHTML != ""){
					$(col).addClass('selectedDate');
					$(col).removeClass('unselectedDate');
                }
                else{
                    $(col).addClass('unselectedDate');
                    $(col).removeClass('selectedDate');
                }
			} 
        }
		
		//Fetches times that the LIV employee has marked as available
        displayLoader();
        $.ajax({
            url: window.location.pathname,
            type: "post",
            data: {"getUnavailableTimes": "1", "date": firstDate, "ajaxResponse":"1"},
            success: function(response){
                var cmb = document.getElementById("livEmpSkypeTime");
                cmb.innerHTML = response;
				hideLoader();
            }
        });
				
		//Fetches times that the LIV employee hasn't marked as available
        displayLoader();
        $.ajax({
			url: window.location.pathname,
            type: "post",
            data: {"getTimes": "1", "date": firstDate, "ajaxResponse": "1"},
            success: function(response){
                var tblTimes = document.getElementById("tblUnavailableTimes");
                tblTimes.innerHTML = response;
				hideLoader();
            }
        });
	}
       
	//Function deletes a slot that the LIV employee has marked as available
    function deleteAvailability(element){
		//Deletes slot if the user confirms their decision
		if(confirm("Are you sure you would like to delete this slot?")){
			var table = document.getElementById("tblUnavailableTimes");
			var rowNum = element.parentNode.parentNode.rowIndex; 
			var date = table.rows[rowNum].cells[0].innerHTML;
			var time = table.rows[rowNum].cells[1].innerHTML;
               
			//Sends data to PHP
            $.ajax({
                url: window.location.pathname,
                type: "post",
                data: {"deleteTime": "1", "date": date, "time": time, "ajaxResponse": "1"},
                success: function(response){
                    if(response == 1){
                        alert("Time successfully deleted");
						location.reload();
                    }
                    else{
                        alert("A problem occurred while deleting your time, please try again later...");
                    }
                }
            });
		}
	}                    
</script>

<?php
		}
	}
?>