<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		include "navbar.php";
		include "../dbConn.php";
		
		//Calls function to populate user's current data
		getData();
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
    
	//Function populates the from with the user's current data
	function getData(){
		$username = $_SESSION["LivEmployeeUsername"];
		$dbConnect = new dbConnect();
		$sql = "select FirstName, LastName, EmailAddress FROM tblLivEmployees WHERE Username = '$username'";
        $result = $dbConnect->executeQuery($sql);
        if($result-num_rows > 0){
            $row = $result->fetch_assoc();
            $arrDisplay['firstName'] = $row["FirstName"];
            $arrDisplay['lastName'] = $row["LastName"];
            $arrDisplay['newEmail'] = $row["EmailAddress"];  

			//Ensures the user has logged in before using the page
			if(isset($_SESSION["LivEmployeeUsername"])){
?>
                   
                   
<html>
	<body>
		<form name = "reset"  method = "POST">
		<h1>Update Details</h1>
		<p><label>First Name:</label> <input maxlength="55" type = "text" name = "firstName" value="<?php echo $arrDisplay['firstName']; ?>"/> </p>
		<p><label>Last Name:</label> <input maxlength="55" type = "text" name = "lastName" value="<?php echo $arrDisplay['lastName']; ?>"/> </p>
		<p><label>Email Address:</label> <input maxlength="254" type = "text" name = "newEmail" value="<?php echo $arrDisplay['newEmail']; ?>"/> </p>
		<button type="submit" name = "reset"><strong>Save</strong></button> 
		</form>
        <br/>
        <button onclick="changePassword()">Change Password</button>
	</body>
</html>
<?php
			}
        }
    }

    if(isset($_POST["getData"])){
            getData();
    }

    if(isset($_SESSION['LivEmployeeUsername'])){
        include "../styles.css";
        include "../sharedFunctions.php";
  
		$arrDisplay = array('firstName' => '', 'lastName' => '', 'newEmail' => '');   
        
		//Processes input once user submits form
		if(isset($_POST["firstName"]) && isset($_POST["lastName"]) && isset($_POST["newEmail"])){
			//Assigns the values that the user entered to variables (formatInput() ensures that the input is safe to use)
			$firstName = formatInput($_POST["firstName"]);
			$lastName = formatInput($_POST["lastName"]);
			$newEmail = formatInput($_POST["newEmail"]);
								   
			//Sets values to ensure that form input fields don't lose value after submission
			$arrDisplay['firstName'] = $firstName;
			$arrDisplay['lastName'] = $lastName;
			$arrDisplay['newEmail'] = $newEmail;
								  
			//If statements ensure that the entered information is valid. If any information is not valid, $valid is set to false, and the program prompts the user for valid input
			$valid = true;
			if (empty($firstName)){
				echo "<p class='error'>Please enter a first name</p>";
				$valid = false;
			}
			if (empty($newEmail) || checkEmail($newEmail) == false){
				echo "<p class='error'>Please enter a valid email address</p>";
				$valid = false;
			}
			if (empty($lastName)){
				echo "<p class='error'>Please enter a last name</p>";
				$valid = false;
			}
				   
			//Writes information to database if the information is valid
			if($valid == true){
				//Updates users name and email information
				$dbConnect = new dbConnect();
				$username = $_SESSION['LivEmployeeUsername'];
				$sql = "update tblLivEmployees set FirstName ='$firstName',EmailAddress = '$newEmail', LastName = '$lastName' where Username = '$username';";
				$success = $dbConnect->executeQuery($sql);
				if($success == true){
					echo "<script>alert('Your account details have been successfully updated!');</script>";
					redirectPage("index.php");
				}
				else{
					echo "<p class='error'>There was an error while updating your account details, please try again</p>";
				}                    
			}
		}
	}
?>
<script>
    //Function takes the user to the change password page    
	function changePassword(){
        window.location.pathname = "employee/changePassword.php";
    }
</script>