<?php
	session_start();
	
	if(isset($_SESSION['LivEmployeeUsername'])){
		include "../dbConn.php";
		
		//Calls appropriate function based on posted values
		if(isset($_POST["dateClicked"])){
			getAccomDetails($_POST["dateClicked"]);
		}
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
		}
		else if(isset($_POST["deleteBooking"])){
			deleteBooking($_POST["applicantUsername"], $_POST["skypeDate"], $_POST["skypeTime"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
	//Deletes the specified Skype booking from the database, and informs the applicant that the Skype meeting has been cancelled
	function deleteBooking($applicantUsername,$skypeDate,$skypeTime){
		$dbConnect = new dbConnect();
        $username = $_SESSION["LivEmployeeUsername"];        
		$sql = "delete from tblSkypeTimes where ApplicantUsername = '$applicantUsername' and LivEmployee = '$username' and SkypeDate = '$skypeDate' and SkypeTime = '$skypeTime'";
		$res = $dbConnect->executeQuery($sql);
		if($res == 1){
			sendMailToLivEmployee($skypeDate, $skypeTime, $applicantUsername);
		}
		echo $res;
	}
	
	//Function sends a confirmation email to the LIV employee that their Skype interview has been cancelled
	function sendMailToLivEmployee($date, $time, $username){
		//Fetches LIV employee details
		$livEmployee = $_SESSION["LivEmployeeUsername"];
		$sql = "select FirstName, EmailAddress from tblLivEmployees where Username = '$livEmployee'";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$livEmployeeName = $row["FirstName"];
			$livEmployeeEmailAddress = $row["EmailAddress"];
			
			//Fetches applicant details
			$sql = "select FirstName, LastName, EmailAddress from tblUsers where Username = '$username'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				//Sends an email to the LIV employee confirming the cancellation
				$row = $result->fetch_assoc();
				$applicantName = $row["FirstName"] . ' ' . $row["LastName"];
				$applicantEmailAddress = $row["EmailAddress"];
				$eol = PHP_EOL;
				$message = stripslashes("Hi, $livEmployeeName. $eol $eol" . "You have successfully cancelled the Skype Interview with $applicantName (Username: $username, Email address: $applicantEmailAddress) that was scheduled for $date at $time. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
				mail($livEmployeeEmailAddress, $applicantName . " Skype Interview", $message);
				sendMailToApplicant($date, $time, $livEmployee, $applicantEmailAddress, $row["FirstName"], $livEmployeeEmailAddress, $livEmployeeName);
			}
		}
	}
	
	//Function sends an email to the applicant telling them that the LIV employee has had to cancel the Skype interview
	function sendMailToApplicant($date, $time, $livEmployee, $applicantEmailAddress, $applicantName, $livEmployeeEmailAddress, $livEmployeeName){
		$eol = PHP_EOL;
		$message = stripslashes("Hi, $applicantName. $eol $eol". "$livEmployeeName (Username: $livEmployee, Email Address: $livEmployeeEmailAddress) has had to cancel the Skype Interview with you that was scheduled for $date at $time. Please book another time slot with $livEmployeeName. We apologise for any inconvenience caused. $eol $eol". "Kind regards, $eol" . "LIV Portal");
		mail($applicantEmailAddress, "Your Skype Interview", $message);
	}

	//Function echoes the selected date's Skype interviews into a table
	function getAccomDetails($date){
		$date = date("Y-m-d", strtotime($date));
		$dbConnect = new dbConnect();
		$username = $_SESSION['LivEmployeeUsername'];
		$sql = "select u.Username, u.FirstName, u.LastName, u.EmailAddress, u.SkypeAddress, s.SkypeTime from tblSkypeTimes s inner join tblUsers u on s.ApplicantUsername = u.Username where SkypeDate  = '$date' and LivEmployee = '$username';";
        $result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			echo "<tr>";
			echo "<th>Username</th>";
			echo "<th>Applicant Name</th>";
			echo "<th>Email Address</th>";
			echo "<th>Skype Address</th>";
			echo "<th>Scheduled Skype Time</th>";
			echo "<th></th>";
			echo "</tr>";
			while($rows = $result->fetch_assoc()){
				echo "<tr>";
				echo "<td>" . $rows["Username"] . "</td>";
				echo "<td>" . $rows["FirstName"] . ' ' . $rows["LastName"]."</td>";
				echo "<td>" . $rows["EmailAddress"] . "</td>";
				echo "<td>" . $rows["SkypeAddress"] . "</td>";
				echo "<td>" . $rows["SkypeTime"] . "</td>";
				echo "<td><button onclick='deleteBooking(this)'>Delete</button></td>";
			    echo "</tr>";
			}
		}
		else{
			echo "<center>There are no Skype bookings for this date</center>";
		}
	}

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
		if(!isset($_POST["ajaxResponse"])){
			include "../loaderStyle.php";
?>
<html>
    <h1>Scheduled Skype Bookings</h1>
</html>
<?php
	include "../calendar.php";
?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
 window.onload = setListeners;
 var currentDate;
   
	//Function sets listeners that will perform a specific task when a specific event occurs
	function setListeners(){
        var cmbInterviewDate = document.getElementById("cmbInterviewDate");

        cmbInterviewDate.addEventListener("change", function() {
			var firstDate = $(cmbInterviewDate).val();
			displaySelectedDate(firstDate);
            getInterviews(firstDate);
		});                   

        var firstDate = $(cmbInterviewDate).val();
        displaySelectedDate(firstDate);
		getInterviews(firstDate);
	}
	
	//Deletes the specified booking from the database
    function deleteBooking(element){
		//Deletes booking if the LIV employee confirms their decision
		if(confirm("Are you sure you would like to delete this booking?")){
			var rowNum = element.parentNode.parentNode.rowIndex; 
			var table = document.getElementById("tblSkypeBookings");
			var applicantUsername = table.rows[rowNum].cells[0].innerHTML;
			var skypeTime = table.rows[rowNum].cells[4].innerHTML;
			var skypeDate = currentDate;
			displayLoader();				
			$.ajax({
				url: window.location.pathname,
				type: "post",
				data: {"deleteBooking" : "1", "applicantUsername": applicantUsername, "skypeDate": skypeDate, "skypeTime": skypeTime, "ajaxResponse":"1"},
				success: function(response){
					if(response == 1){
						alert("The Skype booking has been deleted successfully, and an email has been sent to the applicant requesting another time");
					}
					else{
						alert("An error occurred while trying to delete the Skype booking, please try again later...");
					}
					hideLoader();
					location.reload();
				}
			});
		}
    }
	
	//Function displays the selected date in the calendar in the date pickers
	function handleDateClick(date){
        var formattedDate = new Date(date);
        formattedDate = formatDate(formattedDate);
        var cmbInterviewDate = document.getElementById("cmbInterviewDate");
        $(cmbInterviewDate).val(formattedDate);
		                
        displaySelectedDate(formattedDate);
		getInterviews(date);
	}
        
	//Function fetches the Skype interviews for the selected date	
    function getInterviews(date){
		displayLoader();
		
		//Sends data to PHP
        $.ajax({
			url: window.location.pathname,
			type: "post",
			data: {"dateClicked" : date,"ajaxResponse":"1"},
			success:function(response){
				var booking = document.getElementById("tblSkypeBookings");
				booking.innerHTML = response;
				hideLoader();
			}
		});
    }
        
    //Function formats a date into a format suitable for a datepicker
	//https://stackoverflow.com/questions/23593052/format-javascript-date-to-yyyy-mm-dd
    function formatDate(date) {
		var month = '' + (date.getMonth() + 1),
		day = '' + date.getDate(),
		year = date.getFullYear();

		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	}
           
	//Function displays the selected date in the calendar	   
    function displaySelectedDate(firstDate){
		currentDate = firstDate;
        var table = document.getElementById("table");
		for (var i = 2, row; row = table.rows[i]; i++) {
			for (var j = 0, col; col = row.cells[j]; j++) {
				currentMonth = table.rows[0].cells[1].innerHTML;   
				var colValue = new Date(col.innerHTML + ' ' + currentMonth);
				colValue = formatDate(colValue);
				if(colValue == firstDate && col.innerHTML != ""){
					$(col).addClass('selectedDate');
					$(col).removeClass('unselectedDate');
                }
                else{
                    $(col).addClass('unselectedDate');
                    $(col).removeClass('selectedDate');
                }
            } 
        }
	}
</script>
<html>
<p><label>Interview Date</label><br/>
<input type="date" id="cmbInterviewDate" name="cmbInterviewDate" value="<?php echo date('Y-m-d'); ?>"/> </p>
	<br>
	<h2>Bookings</h2>
	  <table style = "width:100%" id = "tblSkypeBookings" class="report">
	  </table>
	<br>
</html>
<?php
		}
	}
?>