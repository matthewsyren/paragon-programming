<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
?>

<!DOCTYPE html>
	<html>
		<title>LIV Village | Home</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			.tab {display:none}
		</style>
		<style>
			body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
			.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
			.fa-anchor,.fa-coffee {font-size:200px}
		</style>
		<?php
				include "../styles.css";
				include "navbar.php";
		?>
		<body>
		<br/><br/>
			<div class="w3-row-padding w3-container">
				<div class="w3-content">
					<div class="w3-twothird">
						<h1>PURPOSE OF THIS SITE</h1>
						<hr style="width:50px;border:5px solid red" class="w3-round">
						<h5 class="w3-padding-32">This site allows you to manage the applications that you receive regarding people coming to stay at LIV Village.  
					</div>
				</div>
			</div>
			
			<div class="w3-row-padding w3-container">
				<div class="w3-content">
					<div class="w3-twothird">
							<h1>HELP</h1>
							<hr style="width:50px;border:5px solid red" class="w3-round">
							<h5 class="w3-padding-32">To manage accommodation, click on the Accommodation tab and choose the appropriate option. <br/><br/>To view applications that have been made, click on the Applications tab, and filter your results accordingly. When you decide what to do with each applicant (accept application, request Skype interview etc.), click on the appropriate button, and an email will automatically be sent to the applicant informing them of your decision. <br/><br/>To view who will be staying at LIV on a particular date, click on the Bookings tab and click on a date. You can also filter the results by applicant type and the accommodation that they are staying at. <br/><br/>
							To view outstanding payments for accommodation bookings, go to the Payments tab. This will display all accommodation bookings for which a payment hasn't yet been made. Once an applicant makes a payment, you will receive an email with a copy of the proof of payment. If it is acceptable, you can then Accept the payment on this page. If the applicant takes too long to pay, you can click on the Reject button, which will cancel their booking and send the applicant an email telling them they took too long to pay and must make another booking if they'd like to come to LIV. 
							<br/><br/>To set a Reminder for yourself, go to the Reminders tab and enter your reminder. Note: Each reminder has a time associated with it, and the times are listed in 30 minute intervals. 
							<br/><br>To create an account for a new employee, go to the Register tab and enter their details. If the new account must receive email updates, set the Job Title to Active, otherwise set it to Viewer. Once you have entered the information, the new account will be ready to use. Note: The username has to be unique. 
							<br/><br/>To set the schedule for an applicant, go to the Schedule tab and enter the appropriate details. Once again, the time slots are listed in 30 minute intervals.
							<br/><br/>To manage your Skype interviews, go to the Skype tab. From here, you can set times when you are available for a Skype interview, as well as view your upcoming Skype interviews. If need be, you can cancel an interview, and an email will be sent to the  applicant explaining to them that you have had to reschedule the interview. 
									
					</div>
				</div>
			</div>
		</body>
	</html>
	
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>