<?php
    session_start();
        
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){	
        if(!isset($_POST["ajaxResponse"])){
?>

<html>
	<body>
		<?php
            include "navbar.php";             
            include "../styles.css";
            
            }
		}
		else{
			echo "Please <a href='login.php'>Login</a> before coming to this page. ";
		}
            
			include "../dbConn.php";            

			//Ensures the user has logged in before using the page
			if(isset($_SESSION["LivEmployeeUsername"])){
				if(isset($_POST["addUnavailableDates"])){
					//markAsUnavailable($_POST["startDate"], $_POST["endDate"], $_POST["accommodationID"]);
					deleteAccommodationBookings($_POST["startDate"], $_POST["endDate"], $_POST["accommodationID"]);
					unset($_POST["startDate"]);
					unset($_POST["endDate"]);
					unset($_POST["accommodationID"]);
				}
				else if(isset($_POST["getAccommodation"])){
						getAccommodation();
						unset($_POST["getAccommodation"]);
				}
				else if($_POST["getUnavailableDates"]){
						getUnavailableDates();
						unset($_POST["getUnavailableDates"]);
				}
				else if(isset($_POST["deleteUnavailableDates"])){
						deleteUnavailableDates($_POST["accommodationID"],$_POST["startDate"], $_POST["endDate"]);
						unset($_POST["startDate"]);
					unset($_POST["endDate"]);
					unset($_POST["accommodationID"]);
				}
			}
			
			//Function deletes accommodation bookings that fall within the dates that have been marked as unavailable
			function deleteAccommodationBookings($checkInDate, $checkOutDate, $accommodationID){
				$sql = "select * from tblAccommodationBookings where ((CheckInDate >= '$checkInDate' and CheckInDate <= '$checkOutDate') or (CheckOutDate >= '$checkInDate' and CheckOutDate <= '$checkOutDate') or (CheckInDate <= '$checkInDate' and CheckOutDate >= '$checkInDate')) and AccommodationID = '$accommodationID'";
				$dbConnect = new dbConnect();
				$result = $dbConnect->executeQuery($sql);
				$sql = "delete from tblAccommodationBookings where ((CheckInDate >= '$checkInDate' and CheckInDate <= '$checkOutDate') or (CheckOutDate >= '$checkInDate' and CheckOutDate <= '$checkOutDate') or (CheckInDate <= '$checkInDate' and CheckOutDate >= '$checkInDate')) and AccommodationID = '$accommodationID'";
				$dbConnect = new dbConnect();
				$deleteResult = $dbConnect->executeQuery($sql);
				if($deleteResult == 1){
					//Sends an email to all applicants who will be affected by the cancellation
					while($row = $result->fetch_assoc()){
						//Fetches applicant's details
						$username = $row["Username"];
						$bookingID = $row["BookingID"];
						$checkInDate = $row["CheckInDate"];
						$checkOutDate = $row["CheckOutDate"];
						$accommodationID = $row["AccommodationID"];
						
						$sql = "select FirstName, EmailAddress from tblUsers where Username = '$username'";
						$dbConnect = new dbConnect();
						$userResult = $dbConnect->executeQuery($sql);
						if($userResult->num_rows > 0){
							$userRow = $userResult->fetch_assoc();
							$userFirstName = $userRow["FirstName"];
							$userEmailAddress = $userRow["EmailAddress"];
							$eol = PHP_EOL;
							
							//Sends email to applicant telling them their booking has been cancelled
							$message = stripslashes("Hi, $userFirstName. $eol $eol". "Unfortunately, your accommodation booking with us has had to be cancelled due to the accommodation being unexpectedly unavailable for your desired dates. $eol $eol". "Here are the details of the booking that has been cancelled: $eol" . "Booking ID: $bookingID $eol" . "Check-in date: $checkInDate $eol" . "Check-out date: $checkOutDate $eol" . "Accommodation: $accommodationID $eol $eol" . "If you would still like to come to LIV, please book accommodation for another time. We apologise for any inconvenience caused. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
							mail($userEmailAddress, "Your accommodation booking", $message);
						}
					}
				}
				
				markAsUnavailable($checkInDate, $checkOutDate, $accommodationID);
			}
            
			//Function adds the unavailable dates into the database
            function markAsUnavailable($checkInDate, $checkOutDate, $accommodationID){
                $currentDate = date("Y-m-d");
                $dbConnect = new DbConnect();
                $sql = "insert into tblAccommodationUnavailability (AccommodationID, StartDate, EndDate) values ('$accommodationID', '$checkInDate', '$checkOutDate');";
                $result = $dbConnect->executeQuery($sql);
                
                echo $result;
            }
            
			//Function fetches accommodation to display in the combo box
            function getAccommodation(){
                $accommodationType = $_SESSION["accommodationType"];
                $sql = "select AccommodationID from tblAccommodation";
                $dbConnect = new dbConnect();
                $result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
                        echo "<option>" . $row["AccommodationID"] . "</option>";
                    }
                }
            }
            
			//Function fetches the dates that have been marked as unavailable
            function getUnavailableDates(){
                $dbConnect = new dbConnect();
				$date = date('Y-m-d');
                $sql = "select * from tblAccommodationUnavailability where EndDate >= '$date'";
                $result = $dbConnect->executeQuery($sql);
                if($result->num_rows > 0){
                    echo "<tr><th>Accommodation ID</th><th>Start Date</th><th>End Date</th><th></th></tr>";
                    while($row = $result->fetch_assoc()){
                        echo "<tr><td>" . $row["AccommodationID"] . "</td>";
                        echo "<td>" . $row["StartDate"] . "</td>";
						echo "<td>" . $row["EndDate"] . "</td>";
                        echo "<td><button onclick='deleteUnavailableDates(this)'>Delete</button</td></tr>";
                    }
                }
				else{
					echo "<center>There are no upcoming dates when accommodation is marked as unavailable</center>";
				}
            }
            
			//Function marks dates as available again
            function deleteUnavailableDates($accommodationID, $startDate, $endDate){
                $sql = "delete from tblAccommodationUnavailability where AccommodationID = '$accommodationID' and StartDate = '$startDate' and EndDate = '$endDate'";
                $dbConnect = new dbConnect();
                $result = $dbConnect->executeQuery($sql);
                echo $result;
            }
            
			//Ensures the user has logged in before using the page
			if(isset($_SESSION["LivEmployeeUsername"])){
				if(!isset($_POST["ajaxResponse"])){
					include "../loaderStyle.php";
        ?>
		<br/><br/>
		<h1>Accommodation Unavailability</h1>
		<?php
			include "calendar.php";
		?>
                
        <p>Accommodation: <select id="cmbAccommodationID"></select>
		
		<p float='left'>Please select the dates that the accommodation will be unavailable for.</p>

		<p class='form'><label>Start Date:</label><br/> <input type="date" id="checkInDate" name="checkInDate" value="<?php echo date('Y-m-d'); ?>"/> </p>
		<p class='form'><label>End Date:</label><br/> <input type="date" id="checkOutDate" name="checkOutDate" value="<?php echo date('Y-m-d'); ?>"/> </p>
		
		<h3>Emails will be sent to applicants who have booked to stay in this accommodation between the specified dates explaining to them that they will need to reschedule</h2>
		<button onclick="markAccommodationAsUnavailable()">Mark as unavailable</button>
        <br/>
        <br/>
		<h2>Current unavailable dates</h2>
        <table id="tblUnavailableDates" class="report" width="100%"></table>
	</body> 
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>	
    <script>
        var previousClick = null, firstDate = null, secondDate = null, counter = 1, currentMonth = null;
        window.onload = setListeners;
           
		//Sets listeners that perform appropriate actions when an event occurs   
        function setListeners(){
            var cmbCheckInDate = document.getElementById("checkInDate");
            var cmbCheckOutDate = document.getElementById("checkOutDate");

			//Displays date on calendar whenever the user changes the date in the date picker
            cmbCheckInDate.addEventListener("change", function() {
				firstDate = $(cmbCheckInDate).val();
				displaySelectedDates();
			});
		
			//Displays date on calendar whenever the user changes the date in the date picker
			cmbCheckOutDate.addEventListener("change", function() {
				secondDate = $(cmbCheckOutDate).val();
				displaySelectedDates();
			});
            
			displayLoader();
			
			//Fetches accommodation 
            $.ajax({
                url : window.location.pathname,
                type : "post",
                data: {"ajaxResponse":"1", "getAccommodation":"1"},
				success: function(response){
                    var cmbAccommodationID = document.getElementById("cmbAccommodationID");
					cmbAccommodationID.innerHTML = response;
					hideLoader();
                }
            });
           
			firstDate = $(cmbCheckInDate).val();
            secondDate = $(cmbCheckOutDate).val();
            displaySelectedDates();
            getUnavailableDates();
		}
         
		//Function displays dates that have been marked as unavailable
        function getUnavailableDates(){
			displayLoader();
            $.ajax({
                url: window.location.pathname,
                type:"post",
                data: {"getUnavailableDates":"1", "ajaxResponse":"1"}, 
                success: function(response){
					var tblUnavailableDates = document.getElementById("tblUnavailableDates");
                    tblUnavailableDates.innerHTML = response;
					hideLoader();
                }
            });
		}
    
		//Function displays selected dates in the datepickers
        function handleDateClick(date){                   
            date = new Date(date);
            var formattedDate = formatDate(date);
       
            if(counter == 0){
                firstDate = formattedDate;
                counter++;
                var cmbCheckIn = document.getElementById("checkInDate");
                $(cmbCheckIn).val(formattedDate);
                secondDate = firstDate;
                var cmbCheckOut = document.getElementById("checkOutDate");
                $(cmbCheckOut).val(formattedDate);
            }
            else{
                secondDate = formattedDate;
                counter--;
                var cmb = document.getElementById("checkOutDate");
                $(cmb).val(formattedDate);
            }
            if(firstDate >= secondDate){
                secondDate = formattedDate + 1;
                firstDate = formattedDate;
                if(counter == 0){
                    counter++;
                }
                var cmb = document.getElementById("checkInDate");
                $(cmb).val(formattedDate);
            }
            if(firstDate != null && secondDate != null){
                displaySelectedDates();
            }
        }
                
		//Function displays the selected dates in the calendar		
        function displaySelectedDates(){
            var table = document.getElementById("table");

			for (var i = 2, row; row = table.rows[i]; i++) {
				for (var j = 0, col; col = row.cells[j]; j++) {
					currentMonth = table.rows[0].cells[1].innerHTML;
					//alert("First " + firstDate + "   Current " + (col.innerHTML + ' ' + currentMonth));      
					var colValue = new Date(col.innerHTML + ' ' + currentMonth);
					colValue = formatDate(colValue);
					if(colValue >= firstDate && colValue <= secondDate && col.innerHTML != ""){
						$(col).addClass('selectedDate');
						$(col).removeClass('unselectedDate');
				    }
                    else{
                        $(col).addClass('unselectedDate');
						$(col).removeClass('selectedDate');
                    }
				} 
			}
		}

		//Function clears selected dates in the calendar
		function clearSelection(date){
			var selectedDates = document.getElementsByClassName("selectedDate");
			for(var i = 0; i < selectedDates.length; i++){
				var cel = selectedDates[i];
				$(cel).removeClass('selectedDate');
				$(cel).addClass('unselectedDate');
                alert(cel.innerHTML + "  " + cel.classList);
			}
		}

		//Function formats date into yyyy-mm-dd format
		function formatDate(date) {
			var month = '' + (date.getMonth() + 1),
			day = '' + date.getDate(),
			year = date.getFullYear();

			if (month.length < 2) month = '0' + month;
			if (day.length < 2) day = '0' + day;

			return [year, month, day].join('-');
		}
                
		//Function marks accommodation as unavailable	
        function markAccommodationAsUnavailable(){
            var cmbStartDate = document.getElementById("checkInDate");
            var cmbEndDate = document.getElementById("checkOutDate");
            var cmbAccommodationID = document.getElementById("cmbAccommodationID");
			
			//Marks accommodation as unavailable if accommodation has been selected (in other words, the combo box isn't empty)
			if($(cmbAccommodationID).has('option').length == 0){
				alert('Please select accommodation');
			}	
            else{
				var startDate = $(cmbStartDate).val();
				var endDate = $(cmbEndDate).val();
				var accommodationID = $(cmbAccommodationID).val();
						   
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"startDate" : startDate, "endDate": endDate, "accommodationID": accommodationID, "addUnavailableDates": "1", "ajaxResponse": "1" },
					success: function(response){
						if(response == 1){
							alert("Accommodation marked as unavailable");
							location.reload();
						}
						else{
							alert("An error occurred while marking the accommodation as unavailable, please try again...");
						}
					}                        
				});
			}				
        }
        
		//Function deletes unavailable dates for the accommodation
        function deleteUnavailableDates(element){
			//Asks user to confirm that they want to delete the dates
			if(confirm("Are you sure you want to delete these unavailable dates?")){
				displayLoader();
				var tblUnavailableDates = document.getElementById("tblUnavailableDates");
                var rowNum = element.parentNode.parentNode.rowIndex; 
                var accommodationID = tblUnavailableDates.rows[rowNum].cells[0].innerHTML;
                var startDate = tblUnavailableDates.rows[rowNum].cells[1].innerHTML;
                var endDate = tblUnavailableDates.rows[rowNum].cells[2].innerHTML;
                
                $.ajax({
                        url: window.location.pathname,
                        type: "post",
                        data: {"deleteUnavailableDates":"1", "accommodationID": accommodationID, "startDate": startDate, "endDate":endDate, "ajaxResponse": "1"},
                        success: function(response){
                                if(response == 1){
                                        alert("Dates successfully marked as available.");
										location.reload();
                                }
                                else{
                                        alert("An error occurred while deleting the unavailable dates, please try again...");
                                }
								hideLoader();
                        }
                });
			}
        }
    </script>
</html>
<?php
        }
	}
?>