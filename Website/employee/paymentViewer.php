<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["LivEmployeeUsername"])){
        if(!isset($_POST["ajaxResponse"])){
            include "navbar.php";
            include "styles.css";
        }
        
        include "../dbConn.php";
		
		//Calls appropriate method based on posted values
        if(isset($_POST["getOutstandingPayments"])){
            getOutstandingPayments();
            unset($_POST["getOutstandingPayments"]);
        }
        else if(isset($_POST["acceptPayment"])){
            acceptPayment($_POST["bookingID"]);
            unset($_POST["acceptPayment"]);
            unset($_POST["bookingID"]);
        }
		else if(isset($_POST["rejectPayment"])){
            rejectPayment($_POST["bookingID"]);
            unset($_POST["rejectPayment"]);
            unset($_POST["bookingID"]);
        }
    }
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
		//Function updates the payment status to accepted (1) in the database and sends an email to the applicant
        function acceptPayment($bookingID){
            $sql = "update tblAccommodationBookings set PaymentMade = 1 where BookingID = '$bookingID'";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
			if($result == 1){
				sendMailToApplicant($bookingID);
			}
            echo $result;
        }
		
		//Function deletes an accommodation booking from the database and sends an email to the applicant
		function rejectPayment($bookingID){
			$sql = "select * from tblAccommodationBookings where BookingID = $bookingID";
			$dbConnect = new dbConnect();
			$bookingDetails = $dbConnect->executeQuery($sql);
			
            $sql = "delete from tblAccommodationBookings where BookingID = '$bookingID'";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
			if($result == 1){
				sendCancellationMailToApplicant($bookingID, $bookingDetails);
			}
            echo $result;
        }
		
		//Function sends an email to the user informing them of the acceptance of their payment
		function sendMailToApplicant($bookingID){
			$sql = "select * from tblAccommodationBookings where BookingID = $bookingID";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				//Gets booking details
				$row = $result->fetch_assoc();
				$checkInDate = $row["CheckInDate"];
				$checkOutDate = $row["CheckOutDate"];
				$accommodationID = $row["AccommodationID"];
				$username = $row["Username"];
				
				//Gets applicant's details
				$sql = "select * from tblUsers where Username = '$username'";
				$dbConnect = new dbConnect();
				$result = $dbConnect->executeQuery($sql);
				if($result->num_rows > 0){
					//Sends email to the applicant
					$eol = PHP_EOL;
					$row = $result->fetch_assoc();
					$userFirstName = $row["FirstName"];
					$userEmailAddress = $row["EmailAddress"];
					$message = stripslashes("Hi, $userFirstName. $eol $eol". "Your payment for your stay at LIV has been accepted and your booking has now been confirmed. $eol $eol" . "Here are the booking details: $eol" . "Booking ID: $bookingID $eol" . "Check-in date: $checkInDate $eol" . "Check-out date: $checkOutDate $eol" . "Accommodation: $accommodationID $eol $eol" . "We look forward to seeing you! $eol $eol" . "Kind regards, $eol" . "LIV Portal");
					mail($userEmailAddress, "Your accommodation booking", $message);
				}
			}
		}
		
		//Function sends an email to the user informing them of the rejection of their payment and cancellation of their booking
		function sendCancellationMailToApplicant($bookingID, $bookingDetails){
			//Gets booking details
			$booking = $bookingDetails->fetch_assoc();
			$checkInDate = $booking["CheckInDate"];
			$checkOutDate = $booking["CheckOutDate"];
			$accommodationID = $booking["AccommodationID"];
			$username = $booking["Username"];
			
			//Gets applicant's details
			$sql = "select * from tblUsers where Username = '$username'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				//Sends email to applicant
				$eol = PHP_EOL;
				$row = $result->fetch_assoc();
				$userFirstName = $row["FirstName"];
				$userEmailAddress = $row["EmailAddress"];
				$message = stripslashes("Hi, $userFirstName. $eol $eol". "Your provisional booking for accommodation at LIV has been cancelled. This is due to the amount of time you have taken to finalise the booking by making your payment. $eol $eol" . "Here are the booking details of the booking that was cancelled: $eol" . "Booking ID: $bookingID $eol" . "Check-in date: $checkInDate $eol" . "Check-out date: $checkOutDate $eol" . "Accommodation: $accommodationID $eol $eol" . "If you would still like to come to LIV, please make another accommodation booking. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
				mail($userEmailAddress, "Your accommodation booking", $message);
			}
		}
        
		//Function echoes accommodation booking details where no payment has been made into a table
        function getOutstandingPayments(){
            $sql = "select ta.*, tu.FirstName, tu.LastName from tblAccommodationBookings ta inner join tblUsers tu on ta.Username = tu.Username where ta.PaymentMade = 0";
            $dbConnect = new dbConnect();
            $result = $dbConnect->executeQuery($sql);
            if($result->num_rows > 0){
                echo "<tr><th>Booking ID</th><th>Username</th><th>Name</th><th>Check-in date</th><th>Check-out date</th><th>Accommodation</th><th>Date booked</th><th>Amount Due</th><th><th></tr>";
                while($row = $result->fetch_assoc()){
                    echo "<tr>";
                    echo "<td>".$row["BookingID"]."</td>";
                    echo "<td>".$row["Username"]."</td>";
                    echo "<td>".$row["FirstName"].' '. $row["LastName"]."</td>";
                    echo "<td>".$row["CheckInDate"]."</td>";
                    echo "<td>".$row["CheckOutDate"]."</td>";
                    echo "<td>".$row["AccommodationID"]."</td>";
                    echo "<td>".$row["DateBooked"]."</td>";
					echo "<td>".'R'.$row["AmountDue"]."</td>";
                    echo "<td><button onclick='acceptPayment(this)'>Accept</button></td>";
                    echo "<td><button onclick='rejectPayment(this)'>Reject</button></td>";
                    echo "</tr>";
                }
            }
			else{
				echo "<center>There are no outstnding payments for accommodation bookings</center>";
			}
        }
		
		//Ensures the user has logged in before using the page
		if(isset($_SESSION["LivEmployeeUsername"])){
			if(!isset($_POST["ajaxResponse"])){
				include "../loaderStyle.php";
?>
<html>
        <h1>Outstanding payments</h1>
        <table id="tblOutstandingPayments" class="report" width="100%"></table>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
        <script>
				//Calls getOutstandingPayments function when the window loads
                window.onload = getOutstandingPayments;
                
				//Function fetches details on accommodation bookings with outstanding payments
                function getOutstandingPayments(){
                    displayLoader();
                    $.ajax({
                        url: window.location.pathname,
                        type: "post",
                        data: {"getOutstandingPayments" : "1", "ajaxResponse": "1"},
                        success: function(response){
                            var tblOutstandingPayments = document.getElementById("tblOutstandingPayments");
                            tblOutstandingPayments.innerHTML = response;
							hideLoader();
                        }
                    });
                }
                
				//Function accepts the payment for a booking and sends the data to the PHP side for processing
                function acceptPayment(element){
					//Accepts payment if user confirms their decision
					if(confirm("Are you sure you would like to accept the payment for this booking?")){
						displayLoader();
						var table = document.getElementById("tblOutstandingPayments");
                        var rowNumber = element.parentNode.parentNode.rowIndex; 
                        var bookingID = table.rows[rowNumber].cells[0].innerHTML;
                        
						//Sends data to PHP
                        $.ajax({
                            url: window.location.pathname,
                            type: "post",
                            data: {"acceptPayment" : "1", "bookingID" : bookingID, "ajaxResponse": "1"},
                            success: function(response){
                                if(response == 1){
                                    alert("Payment accepted, an email has been sent to the applicant telling them that their accommodation booking has been confirmed");
									location.reload();
								}
                                else{
                                    alert("A problem occured while accepting the payment, please try again later...");
                                }
								hideLoader();
                            }
                        });
					}
                }
                
				//Function rejects the payment for a booking and sends the data to the PHP side for processing
                function rejectPayment(element){
					//Rejects the payment if the user confirms their decision
					if(confirm("Are you sure you would like to delete this applicant's provisional booking?")){
						var table = document.getElementById("tblOutstandingPayments");
                        var rowNumber = element.parentNode.parentNode.rowIndex; 
                        var bookingID = table.rows[rowNumber].cells[0].innerHTML;
						displayLoader();
                        
						//Sends data to the PHP side
                        $.ajax({
                            url: window.location.pathname,
                            type: "post",
                            data: {"rejectPayment" : "1", "bookingID" : bookingID, "ajaxResponse": "1"},
                            success: function(response){
                                if(response == 1){
                                    alert("Accommodation booking cancelled, an email has been sent to the applicant telling them that their accommodation booking has been cancelled and they must make another booking.");
									location.reload();
								}
                                else{
                                    alert("A problem occured while accepting the payment, please try again later...");
                                }
								hideLoader();
                            }
                        });
					}
                }
        </script>
        <?php
			}
		}
        ?>