<?php
    session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		include "navbar.php";
		include "styles.css";
		include "dbConn.php";
			
		if(isset($_POST["submit"])){
			$username = $_SESSION["Username"];
			$sql = "delete from tblApplications where Username = '$username'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result == 1){
				$date = date('Y/m/d');
				$sql = "insert into tblApplications values ('$username', '$date', 0, 0, 0, 'Guest');";
				$dbConnect = new dbConnect();
				$result = $dbConnect->executeQuery($sql);
				if($result == 1){
					sendEmail();
				}
			}
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
	
	//Function sends an email to the applicant and LIV employees about the guest application
	function sendEmail(){
		//Gets files that have been uploaded
        $file_name = $_FILES["attachment"]["name"];
        $temp_name = $_FILES["attachment"]["tmp_name"];
        $file_type = $_FILES["attachment"]["type"];
                        
        $file_name1 = $_FILES["attachment1"]["name"];
        $temp_name1 = $_FILES["attachment1"]["tmp_name"];
        $file_type1 = $_FILES["attachment1"]["type"];

        $username = $_SESSION["Username"];
        $sql = "select * from tblUsers where Username = '$username';";
        $dbConnect = new dbConnect();
        $result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$firstName = $row["FirstName"];
			$lastName = $row["LastName"];
			$emailAddress = $row["EmailAddress"];
			$fullName = $firstName . ' ' . $lastName;
			$eol = PHP_EOL;
			mail($emailAddress, "Your guest application", "Hi, $firstName. $eol $eol" . "Your application to be a guest at LIV has been received. Please wait for a LIV employee to contact you. $eol $eol" . "Kind regards, $eol" . "LIV Portal");
				
			$sql = "select * from tblLivEmployees where JobTitle = 'Active'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				while($row = $result->fetch_assoc()){
					//Attachment code learnt and adapted from https://www.youtube.com/watch?v=zYocypr0Xig
					$from = "noreply@a15008377.dx.am";
                    $to = $row["EmailAddress"];
					$livName = $row["FirstName"];
						
					$from = "noreply@a15008377.dx.am";
					$to = $row["EmailAddress"];
					$subject = "$fullName Guest Application";
					$uid = md5(uniqid(time()));
					$eol = PHP_EOL;
					$message = "Hi, $livName. $eol" . "$fullName has applied to visit LIV as a guest. A copy of their police check and ID/passport has been attached. If you would like to contact $firstName, their email address is: $emailAddress. $eol $eol" . "Kind regards, $eol" . "LIV Portal";

					//Gets file contents
					$file = $temp_name;
					$content = chunk_split(base64_encode(file_get_contents($file)));
											   
					$header = "From: " . $from . $eol;
					$header .= "Reply-To: " . $replyto . $eol;
					$header .= "MIME-Version: 1.0".$eol;
					$header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"" . $eol;
					$header .= "This is a multipart message in MIME format. " . $eol;
											   
					//Content
					$header .= "--" . $uid . $eol;
					$header .= "Content-type:text/plain; charset=iso-8859-1".$eol;
					$header .= "Content-Transfer-Encoding: 7bit".$eol;
					$header .= $message . $eol;
											 
					//Attachment 1 (learnt how to add attachment from https://www.youtube.com/watch?v=zYocypr0Xig)
					$header .= "--" . $uid . $eol;
					$header .= "Content-type: " .$file_type . "; name=\"" . $file_name . "\"". $eol;
					$header .= "Content-Transfer-Encoding: base64" . $eol;
					$header .= "Content-Disposition: attachment; filename=\"".$file_name."\"" .$eol;
					$header .= $content;
												 
					//Attachment 2
					$file = $temp_name1;
					$content = chunk_split(base64_encode(file_get_contents($file)));
					$header .= "--" . $uid . $eol;
					$header .= "Content-type: " .$file_type1 . "; name=\"" . $file_name1 . "\"". $eol;
					$header .= "Content-Transfer-Encoding: base64" . $eol;
					$header .= "Content-Disposition: attachment; filename=\"".$file_name1."\"" .$eol;
					$header .= $content . $eol;

					//Sends email
					mail($to, $subject, "", $header);
				}
				echo "<script>alert('Your application has been submitted, please wait for a LIV employee to contact you...');</script>";
			}
        }                   
	}
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
?>

<h1>Guest Application</h1>
<form name = "registration"  method = "POST" enctype="multipart/form-data">
<p>Copy of ID/Passport: <input type="file" name="attachment" required="true"/></p>
<p>Police Check: <input type="file" name="attachment1" required="true"/></p>
<button name="submit" type="submit">Submit</button>
</form>
<?php
	}
?>