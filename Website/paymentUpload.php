<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		if(!isset($_POST["ajaxResponse"])){
			include "styles.css";
			include "navbar.php";
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
        
    include "dbConn.php";

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		//Calls the appropriate function based on the posted data's value
		if(isset($_POST["getBookingIDs"])){
			getBookingIDs();
		}
	}
        
	//Function fetches the booking IDs for the signed in user for which they haven't paid yet	
    function getBookingIDs(){
		//Fetches the appropriate booking IDs
        $username = $_SESSION["Username"];
        $sql = "select BookingID from tblAccommodationBookings where Username = '$username' and PaymentMade = 0";
        $dbConnect = new dbConnect();
        $result = $dbConnect->executeQuery($sql);
		
		//Outputs the booking IDs in a format that can be displayed in a <select> element
        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
                echo "<option>" . $row["BookingID"] ."</option>";
            }
        }
    }
    //Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){    
		if(!isset($_POST["ajaxResponse"])){
			include "loaderStyle.php";
?>
<style>
	#div1{
		background-color: #f99f18;
		min-height:200px;
		min-width: 48%;
        width: auto;
		display: inline-block;
		margin: auto;
        border-radius: 20px;
        height: auto;
	}
	
	#div2{
		background-color: #0fb5fc;
		min-height:200px;
        height: auto;
		min-width: 48%;
        width: auto;
		display: inline-block;
		margin: auto;
		float: right;
        border-radius: 20px;
	}
</style>
<html>
<br/><br/>
<h1>Payment for accommodation</h1>
<div id="div1">
	<h1>South African Banking Details</h1>
	<p>Account Name: Lungisisa Indlela Village<br/>
	Bank: First National Bank<br/>
	Branch Name: Umhlanga Crescent<br/>
	Branch Code: 220629<br/>
	Account Number: 6242 0878 946<br/>
	Swift Code: FIRNZAJJ</p>
	</div>
	<div id="div2">
	<h1>LIV UK Banking Details</h1>
	<p>Account Name: LIV UK Donations<br/>
	Bank: CAF Bank Ltd<br/>
	Branch: 25 Kings Hill Avenue, Kings Hill West Malling, Kent ME194JQ<br/>
	Account Number: 00021073 <br/>
	Sort Code: 40-52-40<br/>
	</div>
    <form name = "proofOfPayment" id="proofOfPaymentForm" method = "POST" enctype="multipart/form-data">
        <p><label>Booking ID</label><br/>
        <select id="cmbBookingID"></select></p>
        <p><label>Proof of payment</label><br/>
        <input type="file" id="proofOfPayment" name="proofOfPayment" required="true"/></p>
		<br/>
		<br/>
        <button onclick="checkForCompleteness()">Submit</button>
    </form>
</html>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>
	//Calls getBookingIDs function when the page loads
    window.onload = getBookingIDs;
        
	//Function ensures that all required information has been entered
	function checkForCompleteness(){
		var form = document.getElementById("proofOfPaymentForm");
        if($(form)[0].checkValidity()) {
			//Inserts data if all required information has been inserted
            uploadProofOfPayment();
        }
	}	
		
	//Function tells the PHP side of the code to get the user's booking IDs	
    function getBookingIDs(){
		displayLoader();
        $.ajax({
            url: window.location.pathname, 
            type: "post",
            data: {"getBookingIDs" : "1", "ajaxResponse": "1"},
            success: function(response){
                var cmbBookingID = document.getElementById("cmbBookingID");
                cmbBookingID.innerHTML = response;
				hideLoader();
			}
        });
    }
        
	//Function passes the file that the user uploaded to proofOfPaymentUpload.php, which will send an email to the LIV employees with the file attached
    function uploadProofOfPayment(){
		//Gets values and file
        var cmbBookingID = document.getElementById("cmbBookingID");
        var bookingID = $(cmbBookingID).val();
		
		if($(cmbBookingID).has('option').length == 0){
			alert('Please select a booking ID');
		}
		else{
			displayLoader();
			//Uploads file
			var file = $('#proofOfPayment').prop('files')[0];   
			var formData = new FormData();                  
			formData.append('file', file);  
			formData.append('bookingID', bookingID);
			
			//Passes file to the proofOfPaymentUpload.php file
			$.ajax({
				url: 'proofOfPaymentUpload.php', 
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				type: 'post',
				success: function(response){
					alert(response); 
					hideLoader();
				}, 
				error: function(response){
					alert("Your proof of payment has been received, please wait for a LIV employee to contact you");
					hideLoader();
				}
			});
		} 
    }
</script>

<?php
		}
	}
?>