<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
?>
<html>
<style>
	#div1{
		background-color: #f99f18;
		cursor:pointer;
		min-height:200px;
		width: 33%;
        height: auto;
		display: inline-block;
		vertical-align: top;
        border-radius: 20px;
	}
	
	#div2{
		background-color: #0fb5fc;
		cursor:pointer;
		min-height:200px;
		width: 33%;
        height: auto;
		display: inline-block;
		vertical-align: top;
        border-radius: 20px;
	}
	
	#div3{
		background-color: #8cdd3b;
		min-height:200px;
		width: 33%;
        height: auto;
		display: inline-block;
		vertical-align: top;
		cursor:pointer;
        border-radius: 20px;
	}
</style>
	
	<body>
	<?php
        include "styles.css";
        include "navbar.php";
    ?>	
	<br>

	<div>
		<h1>What would you like to apply for?</h1>
		<div onclick="location.href='volunteerApplication.php'; " id='div1'>
			<h1>Volunteer</h1>
			<p>Volunteer your skills


				Medical, Ministry, Teaching, Sports coaching, you name it, we need it. Volunteer for long or short term opportunities.</p>
		</div>
	</a>
	<div onclick="location.href='guestApplication.php'; " id='div2'>
		<h1>Guest</h1>
		<p>As a Guest, you will be able to stay at one of our lodges.</p>
	</div>
	<div onclick="location.href='gapYearApplication.php';" id='div3'>
		<h1>Gap-year Student</h1>
		<p>LIV 4 Change gap year. Young people, spend a year at LIV and journey with Christ by revealing and restoring identity, intentional discipleship, active internship work, whilst giving the opportunity to serve the vision and community of LIV Village.


		LIV provides an excellent social investment opportunity as a corporate group on the Village as well as into the local community</p>
		</div>
	</div>
</body>
</html>
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>