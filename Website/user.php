<?php
	class User{
		//Attributes
		public $username;
		public $firstName;
		public $lastName;
		public $emailAddress;
		public $password;
		public $skypeAddress;
		
		//Constructor		
		function __construct($username, $firstName, $lastName, $emailAddress, $password, $skypeAddress){
			$this->username = $username;
			$this->firstName = $firstName;
			$this->lastName= $lastName;
			$this->emailAddress = $emailAddress;
			$this->password= $password;
			$this->skypeAddress = $skypeAddress;
		}
	}
?>