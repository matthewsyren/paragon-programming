<?php
	session_start();
	
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		include "dbConn.php";
		include "skype.php";

		//Calls appropriate method based on the posted data's value
		if(isset($_POST["getBookings"])){
			getSkypeDetails();	
		}
		if(isset($_POST["oldDate"])&& isset($_POST["oldTime"]) && isset($_POST["oldLivEmployee"]) && isset($_POST["newDate"])&& isset($_POST["newTime"]) && isset($_POST["newLivEmployee"])){
			updateBooking($_POST["oldDate"], $_POST["oldTime"], $_POST["oldLivEmployee"], $_POST["newDate"], $_POST["newTime"], $_POST["newLivEmployee"]);
			unset($_POST["bookingDate"]);
		}
		if(isset($_POST["populateLivEmployees"])){
			getLivEmployees();
			unset($_POST["populateLivEmployees"]);
		}
		if(isset($_POST["getTimesForDate"]) && isset($_POST["getTimesForLivEmployee"])){
			getAvailableTimes($_POST["getTimesForDate"], $_POST["getTimesForLivEmployee"]);
			unset($_POST["time"]);
		}
		if(isset($_POST["deleteDate"])&& isset($_POST["deleteLivEmp"]) && isset($_POST["deleteTime"])){
			deleteBook($_POST["deleteDate"],$_POST["deleteLivEmp"],$_POST["deleteTime"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}

	//Function echoes the contents of a <select> element that contain the times that a LIV employee is available for an interview
	function getAvailableTimes($date, $livEmployee){
		//Fetches the available times
		$dbConnect = new dbConnect();
		$sql = "select SkypeTime from tblSkypeTimes where SkypeDate = '$date' AND LivEmployee = '$livEmployee' AND ApplicantUsername = ''";
		$result = $dbConnect->executeQuery($sql);
		
		//Outputs the available times
		if($result->num_rows > 0){
			while($rows = $result->fetch_assoc()){
				echo "<option>" . $rows["SkypeTime"] . "</option>";
			}
		}
	}

	//Function fetches the usernames of the LIV employees
	function getLivEmployees(){
		//Fetches the LIV employees' usernames
		$dbConnect = new dbConnect();
		$sql = "select distinct LivEmployee from tblSkypeTimes ;";
		$result = $dbConnect->executeQuery($sql);
		   
		//Outputs the usernames in a format that can be used by a <select> tag   
		if($result->num_rows > 0){
			while($row = $result->fetch_assoc()){
				echo "<option>" . $row["LivEmployee"] . "</option>";
			}
		}
	}
	
	//Function updates the date and time of a Skype interview
	function updateBooking($oldDate, $oldTime, $oldLivEmployee, $newDate, $newTime, $newLivEmployee){
		//Marks the previous booking's time as available
		$username = $_SESSION["Username"];
		$dbConnect = new dbConnect();
		$skype = new Skype($oldDate, $oldTime, $oldLivEmployee, $username);
		$sql = "update tblSkypeTimes set ApplicantUsername = '' where SkypeDate = '$skype->date' AND SkypeTime = '$skype->time' AND LivEmployee = '$skype->livEmployee';";
		$res = $dbConnect->executeQuery($sql);
		
		//Updates the booking details if the previous booking has been reset
		if($res == 1){
			$dbConnect = new dbConnect();
			$skype = new Skype($newDate, $newTime, $newLivEmployee, $username);
			$sql = "update tblSkypeTimes set ApplicantUsername = '$skype->applicantUsername' where SkypeDate ='$skype->date' AND LivEmployee = '$skype->livEmployee' AND SkypeTime ='$skype->time'";
			$result = $dbConnect->executeQuery($sql);
		}
		echo $result;
	}

	//Deletes a Skype booking
	function deleteBook($tblDate,$livEmp,$skypeTme){
		//Removes the Skype booking from the database, and marks the time slot as available again
		$dbConnect = new dbConnect();
		$sql = "Update tblSkypeTimes set ApplicantUsername = '' where SkypeDate = '$tblDate' and LivEmployee = '$livEmp' and  SkypeTime = '$skypeTme' ;";
		$res = $dbConnect->executeQuery($sql);
		if($res == 1){
			//Sends an email to the LIV employee telling the booking has been cancelled
			sendMailToLivEmployee($tblDate, $skypeTme, $livEmp);
		}
		echo $res;
	}
	
	//Sends an email to the LIV employee telling thm that the Skype interview has been cancelled
	function sendMailToLivEmployee($date, $time, $livEmployee){
		$username = $_SESSION["Username"];
		$sql = "select FirstName, EmailAddress from tblLivEmployees where Username = '$livEmployee'";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			//Gets LIV employee's details
			$row = $result->fetch_assoc();
			$livEmployeeName = $row["FirstName"];
			$livEmployeeEmailAddress = $row["EmailAddress"];
			
			//Gets user's details
			$sql = "select FirstName, LastName, EmailAddress from tblUsers where Username = '$username'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				$row = $result->fetch_assoc();
				$applicantName = $row["FirstName"] . ' ' . $row["LastName"];
				$applicantEmailAddress = $row["EmailAddress"];
				
				//Sends email
				$eol = PHP_EOL;
				$message = stripslashes("Hi, $livEmployeeName. $eol $eol" . "$applicantName (Username: $username, Email address: $applicantEmailAddress) has cancelled the Skype interview with you that was set for $date at $time. This time slot has now been marked as available for you.$eol $eol" . "Kind regards, $eol" . "LIV Portal");
				mail($livEmployeeEmailAddress, $applicantName . " Skype Interview", $message);
				sendMailToApplicant($date, $time, $livEmployee, $applicantEmailAddress, $row["FirstName"], $livEmployeeEmailAddress, $livEmployeeName);
			}
		}
	}
	
	//Sends an email to the applicant confirming that the Skype interview has been cancelled
	function sendMailToApplicant($date, $time, $livEmployee, $applicantEmailAddress, $applicantName, $livEmployeeEmailAddress, $livEmployeeName){
		$eol = PHP_EOL;
		$message = stripslashes("Hi, $applicantName. $eol $eol". "You have successfully cancelled your Skype Interview with $livEmployeeName (Username: $livEmployee, Email Address: $livEmployeeEmailAddress) that was set for $date at $time. $eol $eol". "Kind regards, $eol" . "LIV Portal");
		mail($applicantEmailAddress, "Your Skype Interview", $message);
	}

	//Displays the upcoming Skype bookings for the user that is signed in
	function getSkypeDetails(){
		$dbConnect = new dbConnect();
		$username = $_SESSION['Username'];
		$sql = "select LivEmployee, SkypeDate, SkypeTime from tblSkypeTimes where ApplicantUsername = '$username';";
		$result = $dbConnect->executeQuery($sql);

		//Echoes out the upcoming bookings into a table
		if($result->num_rows > 0){
			echo "<tr>";
			echo "<th>LIV Employee</th>";
			echo "<th>Date</th>";
			echo "<th>Time</th>";
			echo "<th></th>";
			echo "</tr>";

			while($rows = $result->fetch_assoc()){
				echo "<tr>";
				echo "<td>" . $rows["LivEmployee"] . "</td>";
				echo "<td>" . $rows["SkypeDate"] . "</td>";
				echo "<td>" . $rows["SkypeTime"] . "</td>";
				echo "<td><button type='button' onclick='modalDelete(this)'>Delete</button></td>";
				echo "</tr>";
			}
		}
		else{
			echo "<center>You have no scheduled Skype interviews coming up</center>";
		}
	}

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "loaderStyle.php";
			echo "<br>";
?>
<!DOCTYPE html>
	<html>
		<head>

		<?php
			include "styles.css";
			include "modalStyle.css";
		?>
		</head>
		<body>
			<h1>Upcoming Skype Bookings</h1>

			<!-- The Modal -->
			<div id="modalWindow" class="modal">
				<!-- Modal content -->
				<div class="modal-content">
					<div class="modal-header">
					<span id="close">&times;</span>
					<h1>Update Skype Booking</h1>
				</div>
				<div class="modal-body">
					<h2>LIV Employee</h2>
					<select id="cmbLivEmployees"></select>
					<br>
					<h2>New Date</h2>
					<center><input type = "date" id="newDate"></center>
					<br>
					<h2>Available times</h2>
					<select id="cmbTimes"></select>
					<br>
				</div>
				<div class="modal-footer">
				  <button onclick="updateBooking(this)">Update booking</button>
				</div>
			  </div>
			</div>

		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
		<script>
			window.onload = getSkypeBookings;
            
			//Declarations
            var oldDate, oldLivEmployee, oldTime;

			//Fucntion tells the PHP side of the code to get the upcoming Skype bookings for the user
			function getSkypeBookings(){
				displayLoader();
				var table = document.getElementById("table");
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"getBookings" : "1","ajaxResponse":"1"},
					success:function(response){
						table.innerHTML = response;
						hideLoader();
						setListeners();
					}
				});
			}
			 
			//Function sets the on change listeners for the appropriate elements
            function setListeners(){
				//Declarations
                var cmbSkypeDate = document.getElementById("newDate");
				var cmbTimes = document.getElementById("cmbTimes");
				var cmbLivEmployees = document.getElementById("cmbLivEmployees");

				//Updates the available times based on the date that the user has selected
                cmbSkypeDate.addEventListener("change", function() {
					displayLoader();
					var date = $(cmbSkypeDate).val();
					var livEmp = cmbLivEmployees.options[cmbLivEmployees.selectedIndex].value;
					$.ajax({
						url: window.location.pathname,
						type: "post",
						data: {"getTimesForDate" : date, "getTimesForLivEmployee": livEmp}, 
						success: function(response){
							cmbTimes.innerHTML = response;
							hideLoader();
						}
					});
				});
				
				//Updates the available times based on the LIV employee that the user has selected
				cmbLivEmployees.addEventListener("change", function() {
					displayLoader();
					var date = $(cmbSkypeDate).val();
					var livEmp = cmbLivEmployees.options[cmbLivEmployees.selectedIndex].value;
					$.ajax({
						url: window.location.pathname,
						type: "post",
						data: {"getTimesForDate" : date, "getTimesForLivEmployee": livEmp}, 
						success: function(response){
							cmbTimes.innerHTML = response;
							hideLoader();
						}
					});
				});
            }

			//Assigns the modal to a variable
			var modal = document.getElementById('modalWindow');

			//Assigns the close button for the modal to a variable
			var closeButton = document.getElementById("close");

			//Opens the modal window
			function openModal(element) {
				modal.style.display = "block";
				var cmbLivEmployees = document.getElementById("cmbLivEmployees");
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"populateLivEmployees" : "1"}, 
					success: function(response){
						cmbLivEmployees.innerHTML = response;
					}
				});               
			  
				//Gets the appropriate values and displays them in the modal window
				var cmbTimes = document.getElementById("cmbTimes");			
				var dtePicker = document.getElementById("newDate");              
				var rowNum = element.parentNode.parentNode.rowIndex; 
				var table = document.getElementById("table");
				var date = table.rows[rowNum].cells[1].innerHTML;
				var livEmployee = table.rows[rowNum].cells[0].innerHTML;
				var time = table.rows[rowNum].cells[2].innerHTML;
				date = new Date(date);
				date = formatDate(date);
				oldDate = date;
				oldTime = time;
				oldLivEmployee = livEmployee;
				$(dtePicker).val(date);
			}


			//Closes the modal when the user clicks on the close button
			closeButton.onclick = function() {
				modal.style.display = "none";
			}

			//Closes the modal when the user clicks outside the modal window
			window.onclick = function(event) {
				if (event.target == modal) {
					modal.style.display = "none";
				}
			}

			//Function passes in the data required to update the booking to the PHP side of the code
			function updateBooking(element){
				//Declarations
				var cmbLivEmployees = document.getElementById("cmbLivEmployees");
				var cmbTimes = document.getElementById("cmbTimes");
				var datePicker = document.getElementById("newDate");
				var date = $(datePicker).val();
				
				//Assigning variables
				var time = cmbTimes.options[cmbTimes.selectedIndex].value;
				var livEmp = cmbLivEmployees.options[cmbLivEmployees.selectedIndex].value;
				
				if(cmbLivEmployees.options.length == 0 || cmbTimes.options.length == 0){
					alert("Please ensure that you have selected a time for the Skype meeting, as well as a LIV employee to speak with...");
				}
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"oldDate" : oldDate,"oldTime": oldTime ,"oldLivEmployee": oldLivEmployee, "newDate" : date, "newTime": time, "newLivEmployee": livEmp, "ajaxResponse": "1"},
					success: function(response){
						if(response == 1){
							alert("Update successful");
							location.reload();
						}
						else{
							alert("An error occured while updating your booking, please try again");
						}
					}	
				});
			}
                         
			//Function formats the data in a y-m-d format			 
            function formatDate(date) {
				var month = '' + (date.getMonth() + 1),
				day = '' + date.getDate(),
				year = date.getFullYear();

				if (month.length < 2) month = '0' + month;
				if (day.length < 2) day = '0' + day;

				return [year, month, day].join('-');
			}
			
			//Function passed in the data required to delete a Skype booking to the PHP side
			function modalDelete(element){
				if(confirm("Are you sure you would like to cancel this Skype interview?")){
					displayLoader();
					//Declarations and assignments
					var rwNum = element.parentNode.parentNode.rowIndex; 
					var table = document.getElementById("table");
					var getLivEmp = table.rows[rwNum].cells[0].innerHTML;
					var getDate = table.rows[rwNum].cells[1].innerHTML;
					var getSkypeTme = table.rows[rwNum].cells[2].innerHTML;
					
					$.ajax({
						url: window.location.pathname,
						type: "post",
						data: {"deleteDate":getDate,"deleteLivEmp":getLivEmp,"deleteTime":getSkypeTme, "ajaxResponse": "1"},
						success: function(response){
							if(response == 1){
								alert("Interview cancelled successfully");
								location.reload();
							}
							else{
								alert("There was an error while trying to cancel your interview, please try again...");
							}
							hideLoader();
						}
					});
				}
			}
			</script>
			<br>
			<table style="width:100%" id="table" class="report">
			</table>
			<br>
		</body>
	</html>
<?php
		}
	}
?>