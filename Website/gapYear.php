<?php
	class GapYearStudent{
		//Attributes
		public $username;
		public $title;
		public $address;
		public $homeTelephoneNumber;
		public $businessTelephoneNumber;
		public $cellNumber;
		public $dateOfBirth;
		public $age;
		public $gender;
		public $nationality;
		public $passportNumber;
		public $dateIssued;
		public $validUntil;
		public $maritalStatus;
		public $emergencyContactName;
		public $emergencyContactTelephone;
		public $emergencyContactAddress;
		public $relationship;
		public $currentLocalChurch;
		public $cAddress;
		public $pastor;
		public $cEmail;
		public $timeSpentAtChurch;
		public $reasonsForBecomingAChristian;
		public $churchLifeInvolvement;
		public $missionWork;
		public $highestLevelOfEducationCompleted;
		public $currentOccupation;
		public $languagesSpoken;
		public $interculturalExperience;
		public $otherSkillsOrWorkExperience;
		public $hobbiesAndInterests;
		public $futurePlans;
		public $howLIVWasDiscovered;
		public $previousExperienceWithLIV;
		public $reasonsForApplying;
		public $expectationsForProgram;
		public $areasToFocusOn;
		public $greatestStrengths;
		public $greatestWeaknessess;
		public $lifeCircumstancesAndChallenges;
		public $familyOpinionOfLIV;
		public $adaptability;
		public $teamSkills;
		public $communicationSkills;
		public $relationshipWithUnlikedPeople;
		public $substanceAbuseHistory;
		public $smoker;
		public $inebriationFrequency;
		public $occultActivities;
		public $pastPsychologicalTreatment;
		public $medicallssues;
		public $criminalConvictions;
		public $additionalInformation;
		
		//Constructor
		function __construct($username, $title, $address, $homeTelephoneNumber, $businessTelephoneNumber, $cellNumber, $dateOfBirth,
		$age, $gender, $nationality, $passportNumber, $dateIssued, $validUntil, $maritalStatus, $emergencyContactName, $emergencyContactTelephone, $emergencyContactAddress,
		$relationship, $currentLocalChurch, $cAddress, $pastor, $cEmail, $timeSpentAtChurch, $reasonsForBecomingAChristian, $churchLifeInvolvement, $missionWork,
		$highestLevelOfEducationCompleted, $currentOccupation, $languagesSpoken, $interculturalExperience, $otherSkillsOrWorkExperience, $hobbiesAndInterests, $futurePlans, $howLIVWasDiscovered,
		$previousExperienceWithLIV, $reasonsForApplying, $expectationsForProgram, $areasToFocusOn, $greatestStrengths, $greatestWeaknessess, $lifeCircumstancesAndChallenges,
		$familyOpinionOfLIV, $adaptability, $teamSkills, $communicationSkills, $relationshipWithUnlikedPeople, $substanceAbuseHistory, $smoker, $inebriationFrequency,
		$occultActivities, $pastPsychologicalTreatment, $medicallssues, $criminalConvictions, $additionalInformation){
			$this->username = $username;
			$this->title = $title;
			$this->address = $address;
			$this->homeTelephoneNumber = $homeTelephoneNumber;
			$this->businessTelephoneNumber = $businessTelephoneNumber;
			$this->cellNumber = $cellNumber;
			$this->dateOfBirth = $dateOfBirth;
			$this->age = $age;
			$this->gender = $gender;
			$this->nationality = $nationality;
			$this->passportNumber = $passportNumber;
			$this->dateIssued = $dateIssued;
			$this->validUntil = $validUntil;
			$this->maritalStatus = $maritalStatus;
			$this->emergencyContactName = $emergencyContactName;
			$this->emergencyContactTelephone = $emergencyContactTelephone;
			$this->emergencyContactAddress = $emergencyContactAddress;
			$this->relationship = $relationship;
			$this->currentLocalChurch = $currentLocalChurch;
			$this->cAddress = $cAddress;
			$this->pastor = $pastor;
			$this->cEmail = $cEmail;
			$this->timeSpentAtChurch = $timeSpentAtChurch;
			$this->reasonsForBecomingAChristian = $reasonsForBecomingAChristian;
			$this->churchLifeInvolvement = $churchLifeInvolvement;
			$this->missionWork = $missionWork;
			$this->highestLevelOfEducationCompleted = $highestLevelOfEducationCompleted;
			$this->currentOccupation = $currentOccupation;
			$this->languagesSpoken = $languagesSpoken;
			$this->interculturalExperience = $interculturalExperience;
			$this->otherSkillsOrWorkExperience = $otherSkillsOrWorkExperience;
			$this->hobbiesAndInterests = $hobbiesAndInterests;
			$this->futurePlans = $futurePlans;
			$this->howLivWasDiscovered = $howLIVWasDiscovered;
			$this->previousExperienceWithLIV = $previousExperienceWithLIV;
			$this->reasonsForApplying = $reasonsForApplying;
			$this->expectationsForProgram = $expectationsForProgram;
			$this->areasToFocusOn = $areasToFocusOn;
			$this->greatestStrengths = $greatestStrengths;
			$this->greatestWeaknessess = $greatestWeaknessess;
			$this->lifeCircumstancesAndChallenges = $lifeCircumstancesAndChallenges;
			$this->familyOpinionOfLIV = $familyOpinionOfLIV;
			$this->adaptability = $adaptability;
			$this->teamSkills = $teamSkills;
			$this->communicationSkills = $communicationSkills;
			$this->relationshipWithUnlikedPeople = $relationshipWithUnlikedPeople;
			$this->substanceAbuseHistory = $substanceAbuseHistory;
			$this->smoker = $smoker;
			$this->inebriationFrequency = $inebriationFrequency;
			$this->occultActivities = $occultActivities;
			$this->pastPsychologicalTreatment = $pastPsychologicalTreatment;
			$this->medicallssues = $medicallssues;
			$this->criminalConvictions = $criminalConvictions;
			$this->additionalInformation = $additionalInformation;
		}
	}
?>