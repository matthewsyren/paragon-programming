<?php
    session_start();
	
    include "dbConn.php";

	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		//Calls the appropriate method based on the POST variables that have been set	
		if(isset($_POST["getBookings"])){
			getAccommodationBookings();
			unset($_POST["getBookings"]);
		}
		else if(isset($_POST["bookingID"]) && isset($_POST["newCheckInDate"]) && isset($_POST["newCheckOutDate"])){
			updateBooking($_POST["bookingID"], $_POST["newCheckInDate"], $_POST["newCheckOutDate"]); 
			unset($_POST["newCheckInDate"]);
			unset($_POST["newCheckOutDate"]);
		}
		else if(isset($_POST["bookingID"]) && isset($_POST["deleteBooking"])){
			deleteBooking($_POST["bookingID"]);
			unset($_POST["bookingID"]);
		}
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
        
	//Function updates the accommodation booking for the user
    function updateBooking($bookingID, $newCheckInDate, $newCheckOutDate){
        $dbConnect = new dbConnect();
        $username = $_SESSION["Username"];
        $sql = "update tblAccommodationBookings set CheckInDate = '$newCheckInDate', CheckOutDate = '$newCheckOutDate' where BookingID = '$bookingID' and Username = '$username'";
        $result = $dbConnect->executeQuery($sql);
        echo $result;
    }
        
	//Function deletes a booking that the user has made	
    function deleteBooking($bookingID){
        $dbConnect = new dbConnect();
        $username = $_SESSION["Username"];
		$sql = "select * from tblAccommodationBookings where BookingID = $bookingID";
		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$sql = "delete from tblAccommodationBookings where BookingID = '$bookingID';";
			$result = $dbConnect->executeQuery($sql);
			if($result == 1){
				//Allows the user to book another stay by cancelling their application's previous booking
				$sql = "update tblApplications set ApplicationExpired = 0 where Username = '$username' and ApplicationExpired = 1";
				$result = $dbConnect->executeQuery($sql);
				echo $result;
				sendMailToEmployees($row, $bookingID);
			}
		}
    }
	
	//Function sends an email to the appropriate LIV employees telling them about the cancelled booking
	function sendMailToEmployees($row, $bookingID){
		$username = $_SESSION["Username"];
		$checkInDate = $row["CheckInDate"];
		$checkOutDate = $row["CheckOutDate"];
		$numberOfPeople = $row["NumberOfPeople"];
		$accommodationID = $row["AccommodationID"];
		$totalCost = $row["AmountDue"];
		$sql = "select FirstName, LastName, EmailAddress from tblUsers where Username = '$username'";

		$dbConnect = new dbConnect();
		$result = $dbConnect->executeQuery($sql);
		if($result->num_rows > 0){
			$row = $result->fetch_assoc();
			$applicantName = $row["FirstName"] . " " . $row["LastName"];
			$applicantEmailAddress = $row["EmailAddress"];
				
			$sql = "select FirstName, EmailAddress from tblLivEmployees where JobTitle = 'Active'";
			$dbConnect = new dbConnect();
			$result = $dbConnect->executeQuery($sql);
			if($result->num_rows > 0){
				while($row = $result->fetch_assoc()){
					$name = $row["FirstName"];
					$emailAddress = $row["EmailAddress"];
					$eol = PHP_EOL;
					$message = stripslashes("Hi, $name. $eol $eol" . "$applicantName (Username: $username, Email Address: $applicantEmailAddress) has cancelled their provisional booking to stay in $accommodationID. $eol $eol" . "Here are the details of the cancelled booking: $eol" . "Booking ID: $bookingID $eol" . "Check-in date:  $checkInDate $eol" . "Check-out date: $checkOutDate $eol" . "Number of people booked for: $numberOfPeople $eol" . "Total amount due: R" . "$totalCost. " . "$eol $eol" . "Kind regards, $eol" . "LIV portal");
					mail($emailAddress, $applicantName . " accommodation booking cancellation", $message);
				}
			}
			sendMailToUser($checkInDate, $checkOutDate, $accommodationID, $totalCost, $bookingID, $numberOfPeople, $applicantName, $emailAddress);
		}
	}
	
	//Function sends an email to the applicant confirming their booking has been deleted
	function sendMailToUser($checkInDate, $checkOutDate, $accommodationID, $totalCost, $bookingID, $numberOfPeople, $applicantName, $emailAddress){
		$eol = PHP_EOL;
		$message = stripslashes("Hi, $applicantName. $eol $eol" . "Your accommodation booking at LIV has been successfully cancelled. $eol $eol" . "Here are the details for the cancelled booking: $eol" . "Booking ID: $bookingID $eol" . "Check-in date:  $checkInDate $eol" . "Check-out date: $checkOutDate $eol" . "Number of people booked for: $numberOfPeople $eol" . "Total amount due: R" . "$totalCost. " . "$eol $eol" . "Kind regards, $eol" . "LIV Portal");
		mail($emailAddress, "Your accommodation booking cancellation", $message);
	}
      
	//Function returns echoes a table containing the logged in user's upcoming accommodation bookings  
    function getAccommodationBookings(){
        $username = $_SESSION["Username"];
        $dbConnect = new dbConnect();
        $sql = "select * from tblAccommodationBookings where Username = '$username'";
        $result = $dbConnect->executeQuery($sql);
        if($result->num_rows > 0){
            echo "<tr><th>Booking ID</th><th>Accommodation ID</th><th>Check In Date</th><th>Check Out Date</th><th>Payment Made</th><th></th></tr>";
            while($rows = $result->fetch_assoc()){
                $paymentMade = $rows["PaymentMade"]  == 0 ? "No" : "Yes";
                echo "<tr>";
                echo "<td>" . $rows["BookingID"] . "</td>";
				echo "<td>" . $rows["AccommodationID"] . "</td>";
                echo "<td>" . $rows["CheckInDate"] . "</td>";
                echo "<td>" . $rows["CheckOutDate"] . "</td>";
                echo "<td>" . $paymentMade . "</td>";
                echo "<td><button onclick='deleteBooking(this)'>Delete</button></td>";
                echo "</tr>";
            }
        }
		else{
			echo "<center>You have no accommodation bookings</center>";
		}
    }
    
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
		//Includes appropriate files
		if(!isset($_POST["ajaxResponse"])){
			include "navbar.php";
			include "styles.css";
			include "modalStyle.css";    
			include "loaderStyle.php";
    ?>
  
  <html>
        <h1>Accommodation Bookings</h1>
        <table id='tblAccommodationBookings' class='report'></table>
</html>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>	
<script>
        window.onload = getBookings;
        
		//Function sets the content of the table to whatever is returned from the Ajax request (the upcoming accommodation bookings)
        function getBookings(){ 
			displayLoader();
            $.ajax({
                url: window.location.pathname,
                type: "post",
                data: {"getBookings": "1", "ajaxResponse": "1"},
                success: function(response){
                    var tblBookings = document.getElementById("tblAccommodationBookings");
                    tblBookings.innerHTML = response;
					hideLoader();
                }
            });
        }
        
		var oldCheckInDate, oldCheckOutDate, bookingID;
        

        
		//Function sends the information of the booking that must be deleted to the PHP part of the page
        function deleteBooking(element){
            if(confirm("Are you sure you would like to cancel this booking?")){
				displayLoader();
				setOldValues(element);
				$.ajax({
					url: window.location.pathname,
					type: "post",
					data: {"deleteBooking" : "1", "bookingID": bookingID, "ajaxResponse": "1"},
					success: function(response){
						if(response == 1){
							alert("Booking deleted successfully");
							location.reload();
						}
						else{
							alert("An error occured while deleting your booking, please try again...");
						}
						hideLoader();
					}
				});
			}
        }

		//Sets the values for the global variables
        function setOldValues(element){
            var table = document.getElementById("tblAccommodationBookings");
            var rowNumber = element.parentNode.parentNode.rowIndex; 
            bookingID = table.rows[rowNumber].cells[0].innerHTML;
            oldCheckInDate = table.rows[rowNumber].cells[1].innerHTML;
            oldCheckOutDate = table.rows[rowNumber].cells[2].innerHTML;
        }

</script>

<?php
		}
	}
?>