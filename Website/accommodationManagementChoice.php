﻿<?php
        session_start();
        
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
?>

<html>
	<style>
		#div1{
			background-color: #f99f18;
			cursor:pointer;
			min-height:200px;
			width: 33%;
			height: auto;
			display: inline-block;
			vertical-align: top;
			border-radius: 20px;
		}
		
		#div2{
			background-color: #0fb5fc;
			cursor:pointer;
			min-height:200px;
			width: 33%;
			height: auto;
			display: inline-block;
			vertical-align: top;
			border-radius: 20px;
		}
		
		#div3{
			background-color: #8cdd3b;
			min-height:200px;
			width: 33%;
			height: auto;
			display: inline-block;
			vertical-align: top;
			cursor:pointer;
			border-radius: 20px;
		}
	</style>

	<body>
        <?php
            include "styles.css";
            include "navbar.php";
		?>
        
		<br>
		<div>
			<h1>What would you like to do?</h1>
			<div onclick="location.href='accommodationTypeChoice.php';" id='div1'>
				<h1>Make a booking</h1>
				<p>Select dates for your stay at LIV</p>
			</div>
			<div onclick="location.href='accommodationBookingManagement.php'; "id='div2'>
				<h1>Manage your booking</h1>
				<p>View your booking's payment status or cancel your booking</p>
			</div>
			<div onclick="location.href='paymentUpload.php';" id='div3'>
				<h1>Upload proof of payment for your booking</h1>
				<p>Once you have booked and paid for your accommodation, please upload your proof of payment to confirm your booking</p>
			</div>
		</div>
		</div>
		</body>
</html>
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>