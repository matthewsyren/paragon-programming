<?php
        session_start();
        
	//Ensures the user has logged in before using the page
	if(isset($_SESSION["Username"])){
?>
<!DOCTYPE html>
	<html>
		<title>LIV Village | Home</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<style>
			.tab {display:none}
		</style>
		<style>
			body,h1,h2,h3,h4,h5,h6 {font-family: "Lato", sans-serif}
			.w3-bar,h1,button {font-family: "Montserrat", sans-serif}
			.fa-anchor,.fa-coffee {font-size:200px}
		</style>
		<?php
				include "styles.css";
				include "navbar.php";
		?>
		<body>
		<br/><br/>
			<div class="w3-row-padding w3-container">
				<div class="w3-content">
					<div class="w3-twothird">
						<h1>PURPOSE OF THIS SITE</h1>
						<hr style="width:50px;border:5px solid red" class="w3-round">
						<h5 class="w3-padding-32">This site allows you to apply to be either a Gap-year Student, Guest or Volunteer at LIV. Each of these roles has different requirements and responsibilities, which are explained in more detail when you apply for one of these 
													roles. 
					</div>
				</div>
			</div>
			
			<div class="w3-row-padding w3-container">
				<div class="w3-content">
					<div class="w3-twothird">
							<h1>PROCESS TO APPLY</h1>
							<hr style="width:50px;border:5px solid red" class="w3-round">
							<h5 class="w3-padding-32">To apply to come to LIV, go to the Applications tab and choose the appropriate application type. Once you have chosen your application type, fill in the 
							required details for the application and submit your application. Your application will then be reviewed by a LIV team member, and once it has been reviewed, you might be asked to schedule 
							a Skype interview with one of the employees. <br/><br/>To book a Skype interview, go to the Skype tab and click on Book a Skype interview. Once on the booking page, choose which LIV employee you 
							would like to schedule the interview with, and select one of the available times for that employee. The employee will then be notified of the date and time for the interview. Note: You or 
							the LIV team member can reschedule the interview if necessary.          

							<br/><br/>If your application is accepted, you will receive an email informing you that your application has been accepted. The next step would be to book your dates to stay at LIV. To do this, 
										go to the Accommodation tab and select Make a booking. You will then be prompted to choose which accommodation you'd like to stay in (the options are either the Lodge or Barracks, 
										and the rules regarding who can stay in which accommodation are explained on that page). <br/><br/>Once you have chosen your accommodation type, you will be able to choose the dates you 
										would like to stay, as well as view pictures of the accommodation. Note: If the maximum capacity for specific accommodation is reached for a day, you will not be able to book for that date.
										Either choose different accommodation or different dates if this happens. 
					</div>
				</div>
			</div>
			 
			<div class="w3-container w3-black w3-center w3-opacity w3-margin-left w3-margin-right w3-padding-50" >
				<h1 class="w3-margin w3-xxlarge ">Find Us Here &#9759;</h1>
			</div>

			<div class = "w3-center">
				<iframe
				  width="1200"
				  height="800"
				  frameborder="0" style="border:0"
				  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAc-7f8zqpO2AR8TVGreah-3tpTi6O-KIg
					&q=LIV" allowfullscreen>
				</iframe>
			</div>
		</body>
	</html>
<?php
	}
	else{
		echo "Please <a href='login.php'>Login</a> before coming to this page. ";
	}
?>