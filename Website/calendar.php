<?php
    //Gets the correct month and passes it into the displayCalendar function
    if(isset($_POST["dateClicked"])){
        dateClicked($_POST["dateClicked"]);
    }
    else if(isset($_POST["incrementMonth"])){
        $date = strtotime($_POST["currentMonth"] . " next month");
        displayCalendar(date("M Y", $date));
    }
    else if(isset($_POST["decrementMonth"])){
        $date = strtotime($_POST["currentMonth"] . " previous month");
        displayCalendar(date("M Y", $date));
    }
    else{
        displayCalendar(date("M Y"));
    }
?>

<html>
	<?php
		//Function displays a calendar for the appropriate month
		function displayCalendar($date){
			//Echoes the table and its column headings
            echo "<br/>";
			echo "<div id='placeHolder'>";
			echo "<table cellspacing=3 cellpadding=3 id='table' width='100%'> ";
			echo "<tr id='month'>";
			echo "<td id='decrementMonth' onclick='dateClick(this)' align='center'>&#10094;</td>";
			echo "<td id='monthAndYear' onclick='dateClick(this)' COLSPAN='5' align=center >". $date . "</td> ";
			echo "<td id='incrementMonth' onclick='dateClick(this)' align='center'>&#10095;</td>";
			echo "</tr>";
			echo "<tr>";
			echo "<td onclick='dateClick(this)' align=center><strong>Mon</strong></td>";
			echo "<td onclick='dateClick(this)' align=center><strong>Tue</strong></td>";
			echo "<td onclick='dateClick(this)' align=center><strong>Wed</strong></td>";
			echo "<td onclick='dateClick(this)' align=center><strong>Thu</strong></td>";
			echo "<td onclick='dateClick(this)' align=center><strong>Fri</strong></td>";
			echo "<td onclick='dateClick(this)' align=center><strong>Sat</strong></td>";
			echo "<td onclick='dateClick(this)' align=center><strong>Sun</strong></td>";
			echo "</tr>";
			
			//Creates array to hold dates, calculates the first day of the week of the month and determines the number of calendar cells that wil be displayed
			$arr = array();
			$dayOfWeek = date("w", strtotime($date)) == 0 ? 7 : date("w", strtotime($date))  ;
			$size = $dayOfWeek + cal_days_in_month(CAL_GREGORIAN, date("m", strtotime($date)), date("Y", strtotime($date)));
			
			//Leaves the cells before the 1st of the month blank
			for($i = 1; $i < $dayOfWeek; $i++){
				$arr[$i] = "";
			}
			$count = 1;
			
			//Adds the dates of the month to the array
			for($i = $dayOfWeek; $i < $size; $i++){
				$arr[$i] = $count;
				$count++;
			}	
			
			//Leaves the cells after the last day of the month blank
			for($i = 0; $i < $size % 7; $i++){
				$arr[$i + $size] = "";
			}
                        
			//Echoes the contents of the array to the table
			for($i = 1; $i < $size; $i += 7){
				echo "<tr>";
				for($j = 0; $j < 7; $j++){
                    echo "<td onclick='dateClick(this)' align=center class='unselectedDate'>" . $arr[$i + $j] . "</td>";          
				}
				echo "</tr>";
			}
			echo "</table>";
			echo "</div>";
		}
              
        include "styles.css";  
	?>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>	
	<script>
		var previousElement = null;
		
		//Functon displays the description and selling price of the item whose Add to cart button was clicked on
		function dateClick(element) {
			//Assigns values to the appropriate variables
			var rowNum = element.parentNode.rowIndex;
			var colNum = element.cellIndex;
			var table = document.getElementById("table");
			var date = table.rows[rowNum].cells[colNum].innerHTML; 
			var currentMonth = table.rows[0].cells[1].innerHTML;
			var placeHolder = document.getElementById("placeHolder");

			//Executes appropriate action based on the element that was clicked
			if(date == '❮'){
				$.ajax({
					url: 'calendar.php',
					type: 'post',
					data: { "decrementMonth": "1", "currentMonth" : currentMonth},
					success: function(response) { 
						placeHolder.innerHTML =  "";
						placeHolder.innerHTML = response;
                        setListeners();
					}
				}); 
			}
			else if( date == '❯'){
				$.ajax({
					url: 'calendar.php',
					type: 'post',
					data: {"incrementMonth" : "1", "currentMonth" : currentMonth},
					success: function(response) { 
						placeHolder.innerHTML =  "";
						placeHolder.innerHTML = response;
                        setListeners();}
				}); 
			}
			else if(date != "" && rowNum > 1){
				//Sets the background colour of the clicked element to green, and clears the background of the previously clicked element
				if(previousElement != null){
                    $(previousElement).toggleClass('selectedDate');
                    $(previousElement).toggleClass('unselectedDate');
                }
				$(element).toggleClass('selectedDate');        
				$(element).toggleClass('unselectedDate');
				previousElement = element;
				
				//Passes the selected date to the handleDate function (which is implemented by a class that includes/requires this class)
                date = date + " " + currentMonth;
                handleDateClick(date);
			}
		}
	</script>        
</html>